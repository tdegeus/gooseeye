
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open a new figure
gplot.figure(figsize=(15,10),fontsize=24)

# dummy image
# -----------

# create a dummy image
I = gimage.dummy_circles([200,200],N=(10,10),random=True,periodic=True)
# calculate volume fraction
phi = np.mean(I)

# plot
gplot.subplot(2,3,1)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image: $\mathcal{I}$')


# 2-point probability
# -------------------

# calculate the 2-point probability
S2 = gimage.correlate(I,I,[61,61],periodic=True)

# plot
gplot.subplot(2,3,4)
gplot.imshow(S2,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$S_2$ (image)')

# insert artifact and calculate 2-point probability
# -------------------------------------------------

# create artifact
I          = np.array(I,copy=True)
I[:50,:50] = True

# plot
gplot.subplot(2,3,2)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image with artifact')

# calculate the 2-point probability
S2 = gimage.correlate(I,I,[61,61],periodic=True)

# plot
gplot.subplot(2,3,5)
gplot.imshow(S2,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$S_2$ (image with artifact)')

# create masked and calculate 2-point probability with masked artifact
# --------------------------------------------------------------------

# create mask
Imask          = np.zeros(I.shape,dtype='bool')
Imask[:50,:50] = True

# plot mask
gplot.subplot(2,3,3)
gplot.imshow(Imask,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'mask')

# calculate the 2-point probability
S2 = gimage.correlate(I,I,[61,61],periodic=True,fmask=Imask)

# plot
gplot.subplot(2,3,6)
gplot.imshow(S2,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$S_2$ (masked artifact)')

# save result
# -----------

# show and save the figure
gplot.render(render=True,save=True,name='correlate_masked.svg')


