
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# calculate
# ---------

# create a dummy image
I = gimage.dummy_circles([200,200],N=(10,10),random=True,periodic=True)
# calculate the volume fraction of black
phi = np.mean(I)

# calculate the lineal path function
L = gimage.linealpath(I,[61,61],periodic=True)

# plot
# ----

# open figure
gplot.figure(figsize=(15,5),fontsize=24)

# plot
gplot.subplot(1,3,1)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image: $\mathcal{I}$')

# plot
gplot.subplot(1,3,2)
gplot.imshow(L,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'lineal path: $L$')

# plot cross-section
gplot.subplot(1,3,3)
gplot.plot(np.arange(-30,31),L[30,:])
gplot.ylim([  0,phi])
gplot.xlim([-30, 30])
gplot.axistick(
  axis       = 'yl',
  tick       = (0,phi),
  ticklabels = ('',r'$\varphi$'),
)
gplot.axistick(
  axis       = 'xl',
  tick       = (-30,0,+30),
  ticklabels = ('-30','0','30'),
)
gplot.title(r'cross section ($x$-axis)')

# render/save
gplot.render(render=True,save=True,name='linealpath.svg')
