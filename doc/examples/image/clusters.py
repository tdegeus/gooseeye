
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# dummy image and clusters
# ------------------------

# create a dummy image
I = gimage.dummy_circles([200,200],N=(10,10),random=True,periodic=True)

# determine the clusters, and the centers of gravity
label,num_features,centers,size_features = gimage.clusters(I,periodic=True,return_centers=True)

# plot
# ----

# open a new figure
gplot.figure(figsize=(15,8),fontsize=24)

# plot image
gplot.subplot(1,3,1)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image')

# plot cluster labels
gplot.subplot(1,3,2)
gplot.imshow(label,
  cmap = 'jet',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'clusters')

# plot cluster centers (as markers projected on image)
# select coordinates
i,j = np.where(centers.T>0)
# cluster centers
gplot.subplot(1,3,3)
gplot.imshow(label,
  cmap = 'jet',
)
gplot.plot(i,j,marker='o',color='r',linestyle='none')
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'cluster centers (projected)')

gplot.render(render=True,save=True,name='clusters.svg')