
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open a new figure
gplot.figure(figsize=(15,8),fontsize=24)

# create a dummy image
I        = np.zeros((21,21),dtype='bool')
I[ 4, 4] = True
I[14,15] = True
I[15,15] = True
I[16,15] = True
I[15,14] = True
I[15,16] = True

# determine the clusters, and the centers of gravity
labels,num_features,centers,size_features = gimage.clusters(I,periodic=True,return_centers=True)

# dilation settings:
# cluster 1 -> 1 iteration
# cluster 2 -> 2 iterations
iterations = np.arange(num_features+1,dtype='int32')

# backup the image (for plotting below)
I0 = np.array(I.astype(np.int32),copy=True)
# plot: 0 x dilation
gplot.subplot(1,3,1)
gplot.imshow(I,
  clim = [0,3],
  cmap = 'afmhot',
)
gplot.title(r'image')

# dilate
labels = gimage.clusters_dilate(labels,num_features,iterations,periodic=True)

# backup/create the image for plotting
I       = np.array(labels,copy=True).astype(np.bool).astype(np.int32)*2
I[I0>0] = 1
I1      = np.array(I,copy=True)
# plot: 1 x dilation
gplot.subplot(1,3,2)
gplot.imshow(I,
  clim = [0,3],
  cmap = 'afmhot',
)
gplot.title(r'image + 1 dilation step')

# dilate
labels = gimage.clusters_dilate(labels,num_features,iterations,periodic=True)

# backup/create the image for plotting
I       = np.array(labels,copy=True).astype(np.bool).astype(np.int32)*3
I[I1>0] = 2
I[I0>0] = 1
I2      = np.array(I,copy=True)
# plot: 2 x dilation
gplot.subplot(1,3,3)
gplot.imshow(I,
  clim = [0,3],
  cmap = 'afmhot',
)
gplot.title(r'image + 2 dilation steps')

# save image
gplot.render(render=True,save=True,name='clusters_dilate.svg')