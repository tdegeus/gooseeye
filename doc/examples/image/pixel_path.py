
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open new figure
gplot.figure(figsize=(15,10),fontsize=24.)

# set data to plot grid
grid  = (
  np.hstack(( 0.*np.ones((20,1))              , 1.*np.ones((20,1))              )).T,
  np.hstack(( np.arange(20).reshape(-1,1)/19. , np.arange(20).reshape(-1,1)/19. )).T,
)


for (subplot,algorithm) in enumerate(['bresenham','actual','full']):

  # calculate a few pixel paths
  paths = (
    gimage.path([0,0],[ 9, 2],algorithm=algorithm),
    gimage.path([0,0],[-3, 9],algorithm=algorithm),
    gimage.path([0,0],[-8, 9],algorithm=algorithm),
    gimage.path([0,0],[-9, 0],algorithm=algorithm),
    gimage.path([0,0],[-9,-3],algorithm=algorithm),
    gimage.path([0,0],[-2,-9],algorithm=algorithm),
    gimage.path([0,0],[+9,-2],algorithm=algorithm),
  )

  # store the paths as image, for plotting
  img = np.zeros((19,19),dtype='int32')
  for i,path in enumerate(paths):
    img[path[:,0]+9,path[:,1]+9] = i+1

  # plot the paths
  ax = gplot.subplot(1,3,subplot+1)
  gplot.imshow(img,cmap='afmhot_r',extent=(0,.9999,0,.9999))
  gplot.plot(grid[0],grid[1],linewidth=1.,color='k')
  gplot.plot(grid[1],grid[0],linewidth=1.,color='k')
  gplot.axis(visible=False,xlim=(-0.1,1.1),ylim=(-0.1,1.1))
  gplot.title(r"\textit{algorithm = '%s'}"%algorithm)
  ax.set_aspect('equal')

# save figure
gplot.render(render=True,save=True,name='pixel_path.svg')


