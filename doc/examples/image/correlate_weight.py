
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open a new figure
gplot.figure(figsize=(15,15),fontsize=24)

# create image and damage
# -----------------------

# create a dummy image (boolean)
(I,shapes) = gimage.dummy_circles([200,200],N=(10,10),random=True,periodic=True,return_shapes=True)
# calculate volume fraction
phi = np.mean(I)

# create 'damage':
# shift and scale the islands in the image
shapes[:,0] += 1.1*shapes[:,2]
shapes[:,2] *= 0.4
# create 'damage' image
W = gimage.dummy_circles([200,200],shapes=shapes,periodic=True)
W = W.astype(np.bool)

# create image to plot
Ip          = np.zeros(I.shape,dtype='float')
Ip[I==True] = 0.99
Ip[W==True] = 1.5

# plot image + 'damage'
gplot.subplot(3,3,1)
gplot.imshow(Ip,
  clim = [0,2],
  cmap = gplot.colormap(cmap=['gray_r','Greens'],N=512),
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{I}$ (black) + $\mathcal{W}$ (green)')

# correlations
# ------------

# calculate correlations: without and with a mask over damage
WI = gimage.correlate_weight(W,I,[61,61],periodic=True,mask=W)

# plot image
gplot.subplot(3,3,2)
gplot.imshow(WI-phi,
  extent = [-30,30,-30,30],
  clim   = [0,1],
  cmap   = 'gray_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{W} \star (\mathcal{M} \mathcal{I})$')

# plot image
gplot.subplot(3,3,3)
gplot.imshow(WI-phi,
  extent = [-30,30,-30,30],
  clim   = [-phi,phi],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{W} \star (\mathcal{M} \mathcal{I}) - \langle \mathcal{I} \rangle$')

# convert to gray-scale image
# ---------------------------

Ib = np.array(I,copy=True)

I  = I.astype(np.float)
I *= 0.2
I += 0.4
I += 0.1*np.random.randn(I.size).reshape(I.shape)
I /= np.max(I)

# create image to plot
Ip          = np.zeros(I.shape,dtype='float')
Ip         += I*0.99
Ip[W==True] = 1.5

# plot image + 'damage'
gplot.subplot(3,3,4)
gplot.imshow(Ip,
  clim = [0,2],
  cmap = gplot.colormap(cmap=['gray_r','Reds'],N=512),
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{I}$ (gray) + $\mathcal{W}$ (green)')

# correlations
# ------------

# calculate correlations: without and with a mask over damage
WI = gimage.correlate_weight(W,I,[61,61],periodic=True,mask=W)

# plot image
gplot.subplot(3,3,5)
gplot.imshow(WI,
  extent = [-30,30,-30,30],
  clim   = [0,1],
  cmap   = 'gray_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{W} \star (\mathcal{M} \mathcal{I})$')

# plot image
gplot.subplot(3,3,6)
gplot.imshow(WI-np.mean(I),
  extent = [-30,30,-30,30],
  clim   = [-0.03,0.03],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{W} \star (\mathcal{M} \mathcal{I}) - \langle \mathcal{I} \rangle$')

# histogram of the float-point image
# ----------------------------------

gplot.subplot(3,1,3)
hist,bin_edges = gimage.histogram(I,W,bins=32,range=(0,1),density=True)
gplot.pdf(hist=hist,bin_edges=bin_edges,bar_plot=True)
gplot.axes(
  xlim   = (0,1),
  ylim   = (0,5),
  ntick  = (6,6),
  fmt    = ('%1.1f','%d'),
  xlabel = r'$\mathcal{I}$',
)

# save result
# -----------

# show and save the figure
gplot.render(render=True,save=True,name='correlate_weight.svg')