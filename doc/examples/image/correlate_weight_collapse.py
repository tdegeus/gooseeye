
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open a new figure
gplot.figure(figsize=(15,10),fontsize=24)

# create image and damage
# -----------------------

# create a dummy image (boolean)
(I,shapes) = gimage.dummy_circles([300,300],N=(15,15),random=True,periodic=True,return_shapes=True)
# calculate volume fraction
phi = np.mean(I)

# create 'damage':
# shift and scale the islands in the image
shapes[:,0] += 1.1*shapes[:,2]
shapes[:,2] *= 0.5
# create 'damage' image
W = gimage.dummy_circles([300,300],shapes=shapes,periodic=True)
W = W.astype(np.bool)

# convert damage to clusters
label,_,center,_ = gimage.clusters(W,return_centers=True,periodic=True)

# correlation boolean image
# -------------------------

# calculate correlation
WI  = gimage.correlate_weight(W,I,[61,61],periodic=True,mask=W)
WIc = gimage.correlate_weight_collapse(I.astype(np.float),label,center,[61,61],mask=W,periodic=True)

# create image to plot
Ip          = np.zeros(I.shape,dtype='float')
Ip[I==True] = 0.99
Ip[W==True] = 1.5

# plot image + 'damage'
gplot.subplot(2,3,1)
gplot.imshow(Ip,
  clim = [0,2],
  cmap = gplot.colormap(cmap=['gray_r','Greens'],N=512),
)
gplot.axes(
  xlim  = (0,300),
  ylim  = (0,300),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{I}$ (black) + $\mathcal{W}$ (green)')

# plot image
gplot.subplot(2,3,2)
gplot.imshow(WI-phi,
  extent = [-30,30,-30,30],
  clim   = [-phi,phi],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'correlation')

# plot image
gplot.subplot(2,3,3)
gplot.imshow(WIc-phi,
  extent = [-30,30,-30,30],
  clim   = [-phi,phi],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'correlation: collapse')

# convert to gray-scale image
# ---------------------------

Ib = np.array(I,copy=True)

I  = I.astype(np.float)
I *= 0.2
I += 0.4
I += 0.1*np.random.randn(I.size).reshape(I.shape)
I /= np.max(I)

# correlation floating-points image
# ---------------------------------

# calculate correlation
WI  = gimage.correlate_weight(W,I,[61,61],periodic=True,mask=W)
WIc = gimage.correlate_weight_collapse(I,label,center,[61,61],mask=W,periodic=True)

# create image to plot
Ip          = np.zeros(I.shape,dtype='float')
Ip         += I*0.99
Ip[W==True] = 1.5

# plot image + 'damage'
gplot.subplot(2,3,4)
gplot.imshow(Ip,
  clim = [0,2],
  cmap = gplot.colormap(cmap=['gray_r','Greens'],N=512),
)
gplot.axes(
  xlim  = (0,300),
  ylim  = (0,300),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'$\mathcal{I}$ (gray) + $\mathcal{W}$ (green)')

# plot image
gplot.subplot(2,3,5)
gplot.imshow(WI-np.mean(I),
  extent = [-30,30,-30,30],
  clim   = [-0.03,0.03],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'correlation')

# plot image
gplot.subplot(2,3,6)
gplot.imshow(WIc-np.mean(I),
  extent = [-30,30,-30,30],
  clim   = [-0.03,0.03],
  cmap   = 'RdBu_r',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'correlation: collapse')

# save result
# -----------

# show and save the figure
gplot.render(render=True,save=True,name='correlate_weight_collapse.svg')