
import gooseeye.plot  as gplot
import gooseeye.image as gimage
import numpy          as np

# open a new figure
gplot.figure(figsize=(15,10),fontsize=24)

# dummy image
# -----------

# create a dummy image
I = gimage.dummy_circles([200,200],N=(10,10),random=True,periodic=True)
# calculate volume fraction
phi = np.mean(I)

# plot
gplot.subplot(2,3,1)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image: $\mathcal{I}$')

# 2-point probability
# -------------------

# calculate the 2-point probability
S2 = gimage.correlate(I,I,[61,61],periodic=True)

# plot
gplot.subplot(2,3,2)
gplot.imshow(S2,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet',
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'2-point probability: $S_2$')

# plot cross-section
gplot.subplot(2,3,3)
gplot.plot(np.arange(-30,31),S2[30,:])
gplot.plot([-30,30],[phi**2.0,phi**2.0],linestyle='--',linewidth=1,color='k')
gplot.ylim([  0,phi])
gplot.xlim([-30, 30])
gplot.axistick(
  axis       = 'yl',
  tick       = (0,phi**2.0,phi),
  ticklabels = ('',r'$\varphi^2$',r'$\varphi$'),
)
gplot.axistick(
  axis       = 'xl',
  tick       = (-30,0,+30),
  ticklabels = ('-30','0','30'),
)
gplot.title(r'cross section ($x$-axis)')

# add noise to the image
# ----------------------

# add noise to the image (defined above)
I  = I.astype(np.float)
I += 0.1*(2.0*np.random.random(I.size)-1.0).reshape(I.shape)+0.1
I /= 1.2
# calculate characteristic values
Imean   = np.mean(I)
Isqmean = np.mean(I**2.)

# plot
gplot.subplot(2,3,4)
gplot.imshow(I,
  clim = [0,1],
  cmap = 'gray_r',
)
gplot.axes(
  xlim  = (0,200),
  ylim  = (0,200),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'image with noise: $\mathcal{I}$')

# auto-correlation with noise
# ---------------------------

# calculate the auto-correlation
auto = gimage.correlate(I,I,[61,61],periodic=True)

# plot
gplot.subplot(2,3,5)
gplot.imshow(auto,
  extent = [-30,30,-30,30],
  clim   = [0,phi],
  cmap   = 'jet'
)
gplot.axes(
  xlim  = (-30,30),
  ylim  = (-30,30),
  ntick = (3,3),
  fmt   = ('%d','%d'),
)
gplot.title(r'auto-correlation: $\mathcal{I} \star \mathcal{I}$')

# plot cross-section
gplot.subplot(2,3,6)
gplot.plot(np.arange(-30,31),auto[30,:])
gplot.plot([-30,30],[Imean**2.,Imean**2.],linestyle='--',linewidth=1,color='k')
gplot.plot([-30,30],[Isqmean  ,Isqmean  ],linestyle='--',linewidth=1,color='k')
gplot.ylim([0,phi])
gplot.xlim([-30,30])
gplot.axistick(
  axis       = 'yl',
  tick       = (0,phi**2.0,Imean**2.0,Isqmean,phi),
  ticklabels = ('',r'$\varphi^2$',r'$\langle \mathcal{I} \rangle^2$',r'$\langle \mathcal{I}^2 \rangle$',r'$\varphi$'),
)
gplot.axistick(
  axis       = 'xl',
  tick       = (-30,0,+30),
  ticklabels = ('-30','0','30'),
)
gplot.title(r'cross section ($x$-axis)')

# save the image
# --------------

# show and save the figure
gplot.render(render=True,save=True,name='correlate.svg')

