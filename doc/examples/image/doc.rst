
:tocdepth: 3

gooseeye.image
==============

.. contents::
  :local:
  :depth: 2
  :backlinks: top

2-point probability / auto-correlation
--------------------------------------

Theory
""""""

The most basic statistic determines the typical region in which two points (pixels, voxels, ...) are related. This is best understood by considering a binary 2D-image. In such an image a pixel is either white or black. This is given by the following indicator

.. math::

  \mathcal{I}(\vec{x}_i)
  =
  \begin{cases}
    1 \quad & \mathrm{if} \vec{x}_i\; \in \mathrm{black} \\
    0 \quad & \mathrm{if} \vec{x}_i\; \in \mathrm{white} \\
  \end{cases}

The 2-point probability, :math:`S_2`, is the probability that two points, at a certain distance :math:`\Delta \vec{x}` are both in black. I.e.

.. math::

  S_2 (\Delta \vec{x})
  =
  P
  \big\{
    \mathcal{I}( \vec{x}               ) = 1 ,
    \mathcal{I}( \vec{x}+\Delta\vec{x} ) = 1
  \big\}

Two limits can directly be identified. If the two points coincide, the probability that both points are in black is the same a the probability that one point is in black: the (volume) fraction of black, :math:`\varphi`. I.e.

.. math::

  S_2 ( || \Delta \vec{x} || = 0) = \varphi

If the two points are completely uncorrelated, when the point are far apart, each point has a probability :math:`\varphi` to be in black, and thus

.. math::

  S_2 ( || \Delta \vec{x} || \rightarrow \infty) = \varphi^2

In between these extremes, :math:`S_2` decays from :math:`\varphi` towards the asymptotic value of :math:`\varphi^2`.

The gray-scale generalization is the auto-correlation function

.. math::

  f ( \delta \vec{x} )
  &=
  \int \mathcal{I} ( \vec{x} ) \;
  \mathcal{I} ( \vec{x} - \delta \vec{x} ) \; \mathrm{d} \delta \vec{x} \\
  &= \mathcal{I} ( \vec{x} ) \star \mathcal{I} ( \vec{x} )

Along the same arguments, limit values can be obtained. In this case:

.. math::

  f( \delta \vec{x} = 0 )                &= \langle \mathcal{I}^2 \rangle   \\
  f( \delta \vec{x} \rightarrow \infty ) &= \langle \mathcal{I}   \rangle^2

where the brackets :math:`\langle \ldots \rangle` denotes the average.

Example
"""""""

This result is based on a simple, periodic, image comprising circular black circles embedded in a white background. The top row shows the image and the results for the boolean image: from left to right: the image, :math:`S_2` in two-dimensions, and cross-section of this result along the horizontal axis. On the bottom row the same image and results are show for a gray scale image, for which noise is added and the background and the islands are made gray. From left to right the same results are shown.

.. image:: correlate.svg
  :width: 700px

To generate this example and obtain the result the following steps are taken. First include the relevant modules:

.. literalinclude:: correlate.py
   :language: python
   :lines: 2-4

Then, from the top-left to the bottom-right image:

1.  The simple black-white image, created by:

    .. literalinclude:: correlate.py
       :language: python
       :lines: 13

    which has the data-type ``numpy.bool``.

2.  The two-point probability, for this boolean image:

    .. literalinclude:: correlate.py
       :language: python
       :lines: 35

3.  A cross-section of the two-point probability, along the ``x``-axis.

    .. literalinclude:: correlate.py
       :language: python
       :lines: 54

4.  The image, with add added random noise.

    .. literalinclude:: correlate.py
       :language: python
       :lines: 74-76

5.  The auto-correlation function for the floating-point image (with noise):

    .. literalinclude:: correlate.py
       :language: python
       :lines: 99

6.  A cross-section of the auto-correlation function, along the ``x``-axis.

    .. literalinclude:: correlate.py
       :language: python
       :lines: 118

[:download:`source: correlate.py <correlate.py>`]

Masked correlation
""""""""""""""""""

This function also has the possibility to mask certain pixels. The image's mask is a boolean-matrix of exactly the same shape as the image. Everywhere where the mask is ``True``, the pixel in the image is ignored. The normalization is corrected for the reduced number of data-points.

.. image:: correlate_masked.svg
  :width: 700px

To apply a mask (as in the example above) to the example:

.. literalinclude:: correlate_masked.py
   :language: python
   :lines: 114

[:download:`source: correlate_masked.py <correlate_masked.py>`]

2-point cluster function
------------------------

Theory
""""""

If an image consists of isolated clusters, the 2-point cluster function can be used to quantify the probability that two points are in the same cluster. It is defined as follows:

.. math::

  C_2 (\Delta x) =
  P \big\{ \mathcal{C}(\vec{x}) = \mathcal{C}(\vec{x}+\Delta\vec{x}) \neq 0 \big\}

whereby :math:`\mathcal{C}` is an indicator with a unique non-zero index for each cluster.

Example
"""""""

.. image:: correlate_cluster.svg
  :width: 700px

The steps needed to calculate the 2-point cluster function for the example above, are:

1.  Label the clusters using a unique index per cluster:

    .. literalinclude:: correlate_cluster.py
       :language: python
       :lines: 74

2.  Calculate the 2-point cluster function:

    .. literalinclude:: correlate_cluster.py
       :language: python
       :lines: 91

[:download:`source: correlate_cluster.py <correlate_cluster.py>`]

Lineal path function
--------------------

Theory
""""""

The 2-point cluster function has a first order notion of connectedness. To quantify the true connectedness along a path, the lineal path function is used.

The first thing that is needed is to create a path:

.. image:: pixel_path.svg
  :width: 700px

[:download:`source: pixel_path.py <pixel_path.py>`]

The lineal path function than signal the probability that the entire path between two points in is the same phase.

Example
"""""""

.. image:: linealpath.svg
  :width: 700px

The steps needed to calculate the lineal function for the example above, are:

.. literalinclude:: linealpath.py
   :language: python
   :lines: 15

[:download:`source: linealpath.py <linealpath.py>`]

Weighted correlation
--------------------

Theory
""""""

The weighted correlation characterized the average indicator :math:`\mathcal{I}` around high weight factor :math:`\mathcal{W}`. In the example above, the indicator is zero for matrix and one for inclusion. The weight factor are the green sites.

Mathematically the weighted correlation reads

.. math::

  \mathcal{P} ( \Delta \vec{x} ) = \frac{
    \sum_i
    \mathcal{W}( \vec{x}_i ) \;
    \mathcal{I}( \vec{x}_i + \Delta x )
  }{
    \sum_i
    \mathcal{W}( \vec{x}_i ) \;
    \hfill
  }

Additionally pixels can be masked, for instance to ignore :math:`\mathcal{I}` everywhere where :math:`\mathcal{W}` is non-zeros. The masked correlation reads

.. math::

  \mathcal{P} (\Delta \vec{x}) =
  \frac{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    [ \mathcal{I} \mathcal{M} ] (\vec{x}_i + \Delta \vec{x}) \;
  }{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    \hfill
    \mathcal{M}\, (\vec{x}_i + \Delta \vec{x}) \;
  }

where all pixels where :math:`\mathcal{M}(\vec{x}_i) = 0` are ignored.

.. note::

  Throughout GooseEYE the definition of a mask is such that if ``M[i,j] == True`` the pixel is ignored. If ``M[i,j] == False`` the pixel is not masked. This is the inverse of the above definition.

Example
"""""""

.. image:: correlate_weight.svg
  :width: 700px

[:download:`source: correlate_weight.py <correlate_weight.py>`]

The command to calculate the correlation:

.. literalinclude:: correlate_weight.py
   :language: python
   :lines: 48

Collapse to single point
""""""""""""""""""""""""

To calculate the probability of the inclusion directly next to a weight site (i.e. the green circles in the example above and below) the collapse correlation is calculated. The distance to the edge of the site, :math:`\vec{\delta}_i` is therefore corrected for as follows:

.. math::

  \mathcal{P} (\Delta \vec{x}) =
  \frac{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    \mathcal{I} (\vec{x}_i + \Delta \vec{x} + \vec{\delta}_i) \;
  }{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    \hfill
  }

Similarly to the above, a mask may be introduced as follows:

.. math::

  \mathcal{P} (\Delta \vec{x}) =
  \frac{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    [ \mathcal{I} \mathcal{M} ] (\vec{x}_i + \Delta \vec{x} + \vec{\delta}_i) \;
  }{
    \sum_{i}\;
    \mathcal{W} (\vec{x}_i) \;
    \hfill
    \mathcal{M}\, (\vec{x}_i + \Delta \vec{x} + \vec{\delta}_i) \;
  }

Example
"""""""

.. image:: correlate_weight_collapse.svg
  :width: 700px

[:download:`source: correlate_weight_collapse.py <correlate_weight_collapse.py>`]

The command to calculate the correlation:

.. literalinclude:: correlate_weight_collapse.py
   :language: python
   :lines: 33

Obtain clusters
---------------

Calculate clusters
""""""""""""""""""

.. image:: clusters.svg
  :width: 700px

[:download:`source: clusters.py <clusters.py>`]

To obtain the clusters:

.. literalinclude:: clusters.py
   :language: python
   :lines: 13


Dilate clusters (differently)
"""""""""""""""""""""""""""""

.. image:: clusters_dilate.svg
  :width: 700px

[:download:`source: clusters_dilate.py <clusters_dilate.py>`]

To obtain the dilated clusters:

.. literalinclude:: clusters_dilate.py
   :language: python
   :lines: 19,52