
import gooseeye.plot as gplot
import numpy         as np

gplot.figure(figsize=(15,10))
gplot.subplots_adjust(wspace=0.3)

gplot.subplot(1,2,1)
gplot.plot(np.linspace(0,1,100),np.linspace(0,1,100))
gplot.axes(
  xlim        = (0,1),
  ylim        = (0,1),
  ntick       = (6,6),
  fmt         = ('%1.1f','%1.1f'),
  xlabel      = r'$x$',
  ylabel      = r'$y$',
  label_delta = [0.15,0.20],
)

gplot.subplot(1,2,2)
gplot.plot(np.linspace(0,1,100),1.-np.linspace(0,1,100))
gplot.axes(
  xlim        = (0,1),
  ylim        = (0,1),
  ntick       = (6,6),
  fmt         = ('%1.1f','%1.1f'),
  xlabel      = r'$x$',
  ylabel      = r'$y$',
  label_delta = [0.15,0.20],
)


gplot.render(name='subplot.svg',save=True)