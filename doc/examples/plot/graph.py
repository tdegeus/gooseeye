
import gooseeye.plot as gplot
import numpy         as np

# set example data
x = np.linspace(-1.0,1.0,200)
y = x**3.0

# open new figure: this sets custom defaults
gplot.figure()

# plot curve
gplot.plot(x,y)

# overwrite axes with custom positions/labels
gplot.axes(
  xlim   = (-1, 1),
  ylim   = (-1, 1),
  ntick  = (11,11),
  fmt    = ('%1.1f','%1.1f'),
  xlabel = r'$x$',
  ylabel = r'$y$',
)

# show and save the figure
gplot.render(render=True,save=True,name='graph.svg')