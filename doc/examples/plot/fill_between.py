

import gooseeye.plot as gplot
import numpy         as np

# create example data
x  = np.linspace(-1,1,200)
y0 = x**3.0
y1 = -x**2.0+1.0

gplot.figure()
gplot.fill_between(x,y0,y1)
gplot.axes(
  xlim    = (     -1,      1),
  ylim    = (     -1,      1),
  ntick   = (      5,      5),
  fmt     = ('%1.1f','%1.1f'),
  xlabel  = r'$x$',
  ylabel  = r'$y$',
)
gplot.render(render=True,save=True,name='fill_between.svg')