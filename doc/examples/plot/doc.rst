
:tocdepth: 3

gooseeye.plot
=============

.. contents::
  :local:
  :depth: 2
  :backlinks: top

A simple graph
--------------

Basic example
"""""""""""""

This example shows the plot for the function :math:`y = x^3`. The result:

.. image:: graph.svg
  :width: 400 px

The corresponding code:

.. literalinclude:: graph.py
   :language: python

[:download:`source: graph.py <graph.py>`]

Highly customized example
"""""""""""""""""""""""""

This example shows the plot for the function :math:`y = x^3`. The result:

.. image:: graph_annotate.svg
  :width: 400 px

The corresponding code:

.. literalinclude:: graph_annotate.py
   :language: python

[:download:`source: graph_annotate.py <graph_annotate.py>`]

Specify color per line
""""""""""""""""""""""

.. image:: color_cycle.svg
  :width: 400 px

The corresponding code:

.. literalinclude:: color_cycle.py
   :language: python

[:download:`source: color_cycle.py <color_cycle.py>`]

Subplots
--------

.. image:: subplot.svg
  :width: 400 px

The corresponding code:

.. literalinclude:: subplot.py
   :language: python

[:download:`source: subplot.py <subplot.py>`]

Hatch curve
-----------

Area between curves
"""""""""""""""""""

.. image:: fill_between.svg
  :width: 400 px

The corresponding code:

.. literalinclude:: fill_between.py
   :language: python

[:download:`source: fill_between.py <fill_between.py>`]

Colormaps
---------

.. contents::
  :local:
  :depth: 2
  :backlinks: top

colorid
"""""""

:tue:

  .. image:: colorid_tue.svg
     :width: 150 px

:GrBu:

  .. image:: colorid_GrBu.svg
     :width: 150 px


matplotlib
""""""""""

:sequential:

  .. image:: cmap_matplotlib_sequential.png
     :width: 400 px

  .. image:: cmap_matplotlib_sequential-2.png
     :width: 400 px

:diverging:

  .. image:: cmap_matplotlib_diverging.png
     :width: 400 px

:qualitative:

  .. image:: cmap_matplotlib_qualitative.png
     :width: 400 px

:miscellaneous:

  .. image:: cmap_matplotlib_miscellaneous.png
     :width: 400 px
