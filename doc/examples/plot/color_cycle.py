
import numpy as np
import gooseeye.plot as gplot

# create example data
# -------------------

x = np.linspace(0,1,1001)
y = np.zeros((x.size,10))

for i in range(y.shape[1]):
  y[:,i] = np.exp(-1.*float(i+1)*x)

# plot
# ----

# new figure
gplot.figure()

# set colors of the lines
gplot.gca().set_color_cycle(
  gplot.colormap(cmap='rainbow',N=y.shape[1],dtype='numpy')
)

# plot
gplot.plot(x,y)

# save
gplot.render(render=True,save=True,name='color_cycle.svg')