
import gooseeye.plot as gplot
import numpy         as np

# set example data
x = np.linspace(-1.0,1.0,200)
y = x**3.0

# open new figure: this sets custom defaults
gplot.figure()

# plot the curves: add labels for the legend
gplot.plot(x,y               ,label=r'$f(x) = x^3$',color='b')
gplot.plot(x,x-(0.7-0.7**3.0),label=r'$f(x) = x+c$',color='g')

# set annotation lines: annotating the intersection
gplot.plot([ 0.7    ,0.7    ],[-1.0    ,0.7**3.],color='k',linestyle='--',linewidth=1)
gplot.plot([-1.0    ,0.7    ],[ 0.7**3.,0.7**3.],color='k',linestyle='--',linewidth=1)

# annotate the intersection with the word 'intersection'
gplot.annotate(0.7,0.7**3.0,'intersection',delta=(0.3,0.7))

# set the limits (needed only for 'axistick' below)
gplot.xlim(-1.0,+1.0)
gplot.ylim(-1.0,+1.0)
# add annotation on the axes
gplot.axistick('yl',[0.0,0.7**3.0],['',r'$y_\mathrm{c}$'])
gplot.axistick('xl',[0.0,0.7     ],['',r'$x_\mathrm{c}$'])

# custom numbers at the axes
gplot.axes(
  xlim   = (-1, 1),
  ylim   = (-1, 1),
  ntick  = ( 5, 5),
  fmt    = ('%1.1f','%1.1f'),
  xlabel = r'$x$',
  ylabel = r'$y$',
)

# add legend
gplot.legend(loc='upper left')

# render to the screen and save
gplot.render(render=True,save=True,name='graph_annotate.svg')