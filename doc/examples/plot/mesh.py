import gooseeye.plot as gplot
import goosefem      as gf

# create a mesh using the GooseFEM module
# this module also provides methods to create patches (see below)
mesh = gf.meshgrid.square(nelem=(20,20))

# plot the mesh
gplot.figure()
gplot.patch(mesh.aspatch())
gplot.axes(
  equal   = True,
  xlim    = (-0.1,1.1),
  ylim    = (-0.1,1.1),
  visible = False,
)
gplot.render(render=True,save=True,name='.svg')