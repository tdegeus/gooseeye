
import numpy as np
import gooseeye.hexagon as ghex
import gooseeye.plot as gplot

I = np.arange(4).reshape(2,2)

gplot.figure()
gplot.subplot(1,2,1)
gplot.imshow(I,cmap='afmhot')

gplot.subplot(1,2,2)
ghex.imshow(I,cmap='afmhot')
gplot.render()

# ghex.imshow(I)