
:tocdepth: 3

image.h
-------

.. contents::
  :local:
  :depth: 2
  :backlinks: top

.. .............................................................................

.. _src/core/image_dummy_circles:

``image_dummy_circles``
"""""""""""""""""""""""

.. doxygenfunction:: image_dummy_circles
   :project: core

.. .............................................................................

.. _src/core/image_mid:

``image_mid``
"""""""""""""

.. doxygenfunction:: image_mid
   :project: core

.. .............................................................................

.. _src/core/image_path:

``image_path``
""""""""""""""

.. doxygenfunction:: image_path
   :project: core

.. .............................................................................

.. _src/core/image_clusters:

``image_clusters``
""""""""""""""""""

.. doxygenfunction:: image_clusters
   :project: core

.. .............................................................................

.. _src/core/image_clusters_center:

``image_clusters_center``
"""""""""""""""""""""""""

.. doxygenfunction:: image_clusters_center
   :project: core

.. .............................................................................

.. _src/core/image_clusters_dilate:

``image_clusters_dilate``
"""""""""""""""""""""""""

.. doxygenfunction:: image_clusters_dilate
   :project: core

.. .............................................................................

.. _src/core/image_correlate:

``image_correlate``
"""""""""""""""""""

.. doxygenfunction:: image_correlate
   :project: core

.. .............................................................................

.. _src/core/image_correlate_mask:

``image_correlate_mask``
""""""""""""""""""""""""

.. doxygenfunction:: image_correlate_mask
   :project: core

.. .............................................................................

.. _src/core/image_correlate_weight:

``image_correlate_weight``
""""""""""""""""""""""""""

.. doxygenfunction:: image_correlate_weight
   :project: core

.. .............................................................................

.. _src/core/image_correlate_weight_mask:

``image_correlate_weight_mask``
"""""""""""""""""""""""""""""""

.. doxygenfunction:: image_correlate_weight_mask
   :project: core

.. .............................................................................

.. _src/core/image_correlate_weight_collapse:

``image_correlate_weight_collapse``
"""""""""""""""""""""""""""""""""""

.. doxygenfunction:: image_correlate_weight_collapse
   :project: core

.. .............................................................................

.. _src/core/Stamp:

``Stamp``
"""""""""

.. doxygenstruct:: Stamp
   :project: core

.. .............................................................................

.. _src/core/image_linealpath:

``image_linealpath``
""""""""""""""""""""

.. doxygenfunction:: image_linealpath
   :project: core

.. .............................................................................

.. _src/core/image_linealpath_stamp_points:

``image_linealpath_stamp_points``
"""""""""""""""""""""""""""""""""

.. doxygenfunction:: image_linealpath_stamp_points
   :project: core

.. .............................................................................

.. _src/core/image_linealpath_stamp_paths:

``image_linealpath_stamp_paths``
""""""""""""""""""""""""""""""""

.. doxygenfunction:: image_linealpath_stamp_paths
   :project: core

.. .............................................................................

