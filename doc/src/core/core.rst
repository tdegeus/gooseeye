
:tocdepth: 3

core.h
------

.. contents::
  :local:
  :depth: 2
  :backlinks: top

.. .............................................................................

.. _src/core/Matrix:

``Matrix``
""""""""""

.. doxygenstruct:: Matrix
   :project: core

.. .............................................................................

.. _src/core/matrix_dtype:

``matrix_dtype``
""""""""""""""""

.. doxygenfunction:: matrix_dtype
   :project: core

.. .............................................................................

.. _src/core/matrix_shape:

``matrix_shape``
""""""""""""""""

.. doxygenfunction:: matrix_shape
   :project: core

.. .............................................................................

.. _src/core/matrix_consistent:

``matrix_consistent``
"""""""""""""""""""""

.. doxygenfunction:: matrix_consistent
   :project: core

.. .............................................................................

.. _src/core/get_error:

``get_error``
"""""""""""""

.. doxygenfunction:: get_error
   :project: core

.. .............................................................................
