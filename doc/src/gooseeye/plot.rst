
:tocdepth: 3

gooseeye.plot
=============

.. contents::
  :local:
  :depth: 2
  :backlinks: top

Overview
--------

Figure settings
"""""""""""""""

.. autosummary::

   gooseeye.plot.figure
   gooseeye.plot.subplot
   gooseeye.plot.title
   gooseeye.plot.show
   gooseeye.plot.render
   gooseeye.plot.savefig
   gooseeye.plot.filename
   gooseeye.plot.html

Different types of plots
""""""""""""""""""""""""

.. autosummary::

   gooseeye.plot.plot
   gooseeye.plot.fill
   gooseeye.plot.fill_between
   gooseeye.plot.scatter
   gooseeye.plot.boxplot
   gooseeye.plot.errorbar
   gooseeye.plot.square
   gooseeye.plot.pdf
   gooseeye.plot.cdf
   gooseeye.plot.cdf_threshold

Plot matrix or mesh
"""""""""""""""""""

.. autosummary::

   gooseeye.plot.imread
   gooseeye.plot.imshow
   gooseeye.plot.patch
   gooseeye.plot.mesh2polygon
   gooseeye.plot.mesh2patch

Colormap and colorbar
"""""""""""""""""""""

.. autosummary::

   gooseeye.plot.clim
   gooseeye.plot.colorbar
   gooseeye.plot.coloraxis
   gooseeye.plot.colormap
   gooseeye.plot.colorid
   gooseeye.plot.ascolormap
   gooseeye.plot.colormap_show

Axis settings
"""""""""""""

.. autosummary::

   gooseeye.plot.axes
   gooseeye.plot.axis
   gooseeye.plot.axislabel
   gooseeye.plot.xlim
   gooseeye.plot.ylim
   gooseeye.plot.axistick
   gooseeye.plot.axispart

Annotation
""""""""""

.. autosummary::

   gooseeye.plot.legend
   gooseeye.plot.annotate

Function reference
------------------

.. automodule:: gooseeye.plot
   :members:
   :imported-members:
