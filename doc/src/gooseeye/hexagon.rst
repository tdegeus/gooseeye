
################
gooseeye.hexagon
################

Module reference
================

.. contents::
  :local:
  :depth: 2
  :backlinks: top

Plotting
--------

.. autosummary::

  gooseeye.hexagon.imshow

Function reference
------------------

.. automodule:: gooseeye.hexagon
  :members:
  :imported-members: