
:tocdepth: 3

gooseeye.image
==============

.. contents::
  :local:
  :depth: 2
  :backlinks: top

Correlation measures
--------------------

.. autosummary::

  gooseeye.image.correlate
  gooseeye.image.correlate_weight
  gooseeye.image.correlate_weight_collapse
  gooseeye.image.linealpath

Image processing
----------------

.. autosummary::

  gooseeye.image.dilate
  gooseeye.image.clusters
  gooseeye.image.clusters_dilate

Miscellaneous
-------------

.. autosummary::

  gooseeye.image.histogram
  gooseeye.image.dummy_circles
  gooseeye.image.path

Function reference
------------------

.. automodule:: gooseeye.image
  :members:
  :imported-members:

