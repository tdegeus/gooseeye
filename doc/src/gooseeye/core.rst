
:tocdepth: 3

gooseeye.core
=============

.. contents::
  :local:
  :depth: 2
  :backlinks: top

core.h
------

.. autosummary::

  gooseeye.core.Matrix
  gooseeye.core.error

image.h
-------

.. autosummary::

  gooseeye.core.Stamp
  gooseeye.core.image_path
  gooseeye.core.image_dummy_circles
  gooseeye.core.image_clusters
  gooseeye.core.image_clusters_center
  gooseeye.core.image_clusters_dilate
  gooseeye.core.image_correlate
  gooseeye.core.image_correlate_mask
  gooseeye.core.image_correlate_weight
  gooseeye.core.image_correlate_weight_mask
  gooseeye.core.image_correlate_weight_collapse
  gooseeye.core.image_linealpath
  gooseeye.core.image_linealpath_stamp

Support functions
-----------------

.. autosummary::

  gooseeye.core.asnumpy
  gooseeye.core.np_ptr

Function reference
------------------

.. automodule:: gooseeye.core
   :members:
   :imported-members:
