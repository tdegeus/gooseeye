
GooseEYE - Python interface
===========================

Documentation

.. toctree::
   :maxdepth: 1

   src/gooseeye/plot.rst
   src/gooseeye/image.rst
   src/gooseeye/hexagon.rst
   src/gooseeye/core.rst

Examples:

.. toctree::
   :maxdepth: 2

   examples/plot/doc.rst
   examples/image/doc.rst

GooseEYE - Matlab interface
===========================

Examples:

GooseEYE - core
===============

Documentation

.. toctree::
   :maxdepth: 1

   src/core/core.rst
   src/core/image.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

