
clear all; close all;

im  = gooseeye.image.dummy_circles([500,500],[10,10],true,true);
phi = mean(mean(double(im)));

% plot image
figure();
set(gcf,'position',[0,0,1200,600]);
subplot(2,3,1);
imshow(im);
colormap(gca,'gray');
caxis([0,1]);


% calculate 2-point cluster function
label = gooseeye.image.clusters(im,true);
C     = gooseeye.image.correlate(im,im,[101,101],true,true);


% plot image
subplot(2,3,2);
imshow(C);
colormap(gca,'jet');
caxis([0,phi]);



% 
% C = gooseeye.image.linealpath(im,[51,51],true);

