#!/bin/bash

# ==============================================================================
# OSx
# ==============================================================================

if [ "$(uname)" == "Darwin" ]; then

  # get the path of the GooseFEM root
  DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

  # set environment variables
  export DYLD_LIBRARY_PATH=$DIR/src:$DYLD_LIBRARY_PATH
  export LD_LIBRARY_PATH=$DIR/src:$LD_LIBRARY_PATH
  export LIBRARY_PATH=$DIR/src:$LIBRARY_PATH
  export PYTHONPATH=$DIR/src:$PYTHONPATH

# ==============================================================================
# Linux
# ==============================================================================

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then

  # get the path of the GooseFEM root
  DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

  # set environment variables
  export LD_LIBRARY_PATH=$DIR/src:$LD_LIBRARY_PATH
  export PYTHONPATH=$DIR/src:$PYTHONPATH

# ==============================================================================
# Windows
# ==============================================================================

elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then

  echo "This platform is not supported at this moment"

# ==============================================================================
# other platforms
# ==============================================================================

else

  echo "This platform is not supported at this moment"

fi

