'''
This module is a wrapper around the "matplotlib" library providing the
opportunity generated customized plots in limited code.

:references:

  * `Colormaps <http://matplotlib.org/examples/color/colormaps_reference.html>`_

:dependencies:

  * numpy
  * matplotlib

:copyright:

  | Tom de Geus
  | tom@geus.me
  | http://www.geus.me
'''

import matplotlib as mpl
import numpy      as np

import warnings
warnings.filterwarnings("ignore")

mpl.use('Agg')

import matplotlib.pyplot as plt

# ==============================================================================

def html(name=None,mode='a',path='.',relpath=True,extension=['.svg','.png'],comment=False,title=False):
  '''
Create a HTML-file to view all figures in a path. If the HTML-file exists
this function only inserts the figures that are not already in the HTML-file.

:options:

  **name** ([``None``] | ``<str>``)
    Filename of the HTML-file. If set ``None`` the file is returned as string.

  **mode** (``'w'`` | ``'a'``)
    Write mode for the output file. If the file does not exist the write mode
    is set to 'w'. Note: this option is ignored if ``name==None``.

  **path** ([``'.'``] | ``<str>`` | ``<list>``)
    Path to search figures.

  **relpath** ([``True``] | ``False``)
    Convert the file-path to a relative path.

  **extension** ([``('.svg','.png')``] | ``<str>`` | ``<list>``)
    Extensions of the figures.

  **comment** ([``False``] | ``True``)
    Insert new figure(s) as comments. This option is ignored if a new file is
    created (or a string is returned).

  **title** ([``False``] | ``True``)
    Include figure's filename as title.
  '''

  import os

  # check/convert input options
  # ---------------------------

  # set type of input arguments
  types = {}
  types[ 'name'      ] = [ 'str','None' ]
  types[ 'mode'      ] = [ 'str','None' ]
  types[ 'path'      ] = [ 'str','list' ]
  types[ 'relpath'   ] = [ 'bool'       ]
  types[ 'extension' ] = [ 'str','list' ]
  types[ 'comment'   ] = [ 'bool'       ]
  # check using list
  for var in types:
    check  = []
    check += [eval('type(%s)==%s'%(var,i)) for i in types[var] if i!='None']
    check += [eval('%s is None'%var)       for i in types[var] if i=='None']
    if not any(check):
      raise IOError('Unknown input type %s'%var)
  # check write mode
  if name is not None:
    if mode not in ['w','a']:
      raise IOError('Unknown write mode')
  # convert mode if the file does not exist
  if mode=='a' and name is not None:
    if not os.path.exists(name):
      mode = 'w'

  # convert path(s)/extension(s) to list for convenient programming
  if type(path)!=list:
    path = list(path)
  if type(extension)!=list:
    extension = list(extension)

  # read figures
  # ------------

  # convert paths to relative path
  if relpath:
    path = [os.path.relpath(p) for p in path]
  # read all files in all paths
  files = []
  for p in path:
    files += [os.path.join(p,f) for f in os.listdir(p) if os.path.isfile(os.path.join(p,f))]
  # extract files with desired extension(s)
  files = [f for f in files if os.path.splitext(f)[1] in extension]
  # remove leading "./" from path
  for (i,f) in enumerate(files):
    try:
      if f[:2]=="./":
        files[i] = f[2:]
    except:
      pass
  # store
  files = sorted(files)

  # create stand-alone file (and exit)
  # ----------------------------------

  if name is None or mode=='w':
    lines  = ['<!DOCTYPE html>']
    lines += ['<html>']
    lines += ['<body>']
    if title:
      lines += ['%s<br>\n<img height="300" src="%s">\n<br>'%(f,f) for f in files]
    else:
      lines += ['<img height="300" src="%s">\n<br>'%f for f in files]
    lines += ['</body>']
    lines += ['</html>']
    lines += ['']
    # no file specified: return stand-alone file as string
    if name is None:
      return '\n'.join(lines)
    if mode=='w':
      open(name,'w').write('\n'.join(lines))
      return None

  # update existing file
  # --------------------

  # check presence of filename
  if name is None:
    raise IOError('Filename must be specfied in append mode')

  # read file, and add figures that are not yet present
  text = open(name,'r').read()
  # skip files that are already in the html file
  files = [f for f in files if len(text.split(f))<2]
  # if no files remain, break function
  if len(files)==0:
    return None

  # try adding the files between "<body>" or "<html>"
  if len(text.split('<body>'))>1 and len(text.split('</body>'))>1:
    (head,post) = text.split('</body>')
    post        = '</body>'+post
  elif len(text.split('<html>'))>1 and len(text.split('</html>'))>1:
    (head,post) = text.split('</html>')
    post        = '</html>'+post
  else:
    head    = text
    post    = ''
    comment = True

  # add the files
  if comment:
    if title:
      body = '\n'.join(['<!--%s<br>-->\n<!--<img height="300" src="%s"><br>-->'%(f,f) for f in files])
    else:
      body = '\n'.join(['<!--<img height="300" src="%s"><br>-->'%f for f in files])
  else:
    if title:
      body = '\n'.join(['%s<br>\n<img height="300" src="%s">\n<br>'%(f,f) for f in files])
    else:
      body = '\n'.join(['<img height="300" src="%s">\n<br>'%f for f in files])

  # write file
  open(name,'w').write(head+body+'\n'+post)
  return None

# ==============================================================================

def figure(**kwargs):
  '''
New figure with custom settings.

* use LaTeX. including "amsmath",
* custom font and print settings,
* custom figure size.

:options:

  **figsize** ([``(13,10)``] | ``(width,height)``)
    Customize the size of the figure.

  **delta** ([``(0.13,0.11)``] | ``(dx,dy)``)
    Custom move the axes in x- and y-direction

  **fontsize** ([``36.0``] | ``<float>``)
    Set a custom font-size. The other ``gooseeye.plot`` function derive their
    font-size from this setting. Alternatively they can be overwritten.

  **axissize** ([``False``] | ``(x0,y0,wx,hy)``)
    Specify the axis size. For example, to span the whole figure size specify::

      axissize = (0.0,0.0,1.0,1.0)

    Notice that the ``delta`` option will be ignored.

:returns:

  **gcf** (``<matplotlib>``)
    Figure handle.

.. seealso::

  `matplotlib.pyplot.figure
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.figure>`_
  '''

  # check exclusivity of options
  if 'delta' in kwargs and 'axissize' in kwargs:
    raise IOError('The option "delta" and "axissize" are mutually exclusive')

  # extract local options / defaults
  fontsize = kwargs.pop( 'fontsize' , 36.0        )
  delta    = kwargs.pop( 'delta'    , (0.13,0.11) )
  axissize = kwargs.pop( 'axissize' , False       )

  # set "figure" defaults
  kwargs.setdefault( 'figsize'   , (13,10) )
  kwargs.setdefault( 'facecolor' , 'w'     )

  # open figure, and empty
  gcf = plt.figure(**kwargs)
  plt.clf()

  # customly move the axis
  if not axissize:
    plt.axes([delta[0],delta[1],0.97-delta[0],0.97-delta[1]])
  else:
    plt.axes(axissize)

  # font/print/latex settings
  mpl.rc('text',usetex=True)
  mpl.rc('font',family='serif',serif='Computer Modern Roman',size=fontsize,weight='light')
  mpl.rcParams['lines.linewidth'    ] = 3.0
  mpl.rcParams['lines.color'        ] = 'r'
  mpl.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'
  mpl.rcParams['text.latex.preamble'] = r'\usepackage{amsfonts}'
  mpl.rcParams['text.latex.preamble'] = r'\usepackage{amssymb}'
  mpl.rcParams['text.latex.preamble'] = r'\usepackage{bm}'
  mpl.rcParams['svg.fonttype'       ] = 'none'

  # return the figure handle
  return gcf

# ==============================================================================

def gca(*args,**kwargs):
  '''
Get the current axis.

.. seealso::

  `matplotlib.pyplot.gca
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.gca>`_
  '''

  return plt.gca(*args,**kwargs)

# ==============================================================================

def rc(*args,**kwargs):
  '''
Settings of the module.
  '''

  return plt.rc(*args,**kwargs)

# ==============================================================================
# alias matplotlib.pylab functions (with defaults set)
# ==============================================================================

# ------------------------------------------------------------------------------

def subplot(*args,**kwargs):
  '''
Open a sub-plot.

:arguments:

  **nrows** (``<int>``)
    Number of rows.

  **ncols** (``<int>``)
    Number of columns.

  **plot_number** (``<int>``)
    Identify the particular subplot in which to plot.

.. seealso::

  * `matplotlib.pyplot.subplot
    <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.subplot>`_

  * :py:meth:`gooseeye.plot.subplots`

  * :py:meth:`gooseeye.plot.subplot_adjust`

  '''
  return plt.subplot(*args,**kwargs)

# ------------------------------------------------------------------------------

def subplots_adjust(*args,**kwargs):
  '''
Modify sub-plot settings.

:options:

  **wspace**  ([``0.2``] | ``<float>``)
    Amount of width reserved for blank space between subplots.

  **hspace**  ([``0.5``] | ``<float>``)
    Amount of height reserved for white space between subplots.

  **left**  ([``0.125``] | ``<float>``)
    Left side of the subplots of the figure.

  **right**  ([``0.9``] | ``<float>``)
    Right side of the subplots of the figure.

  **bottom**  ([``0.1``] | ``<float>``)
    Bottom of the subplots of the figure.

  **top**  ([``0.9``] | ``<float>``)
    Top of the subplots of the figure.

.. seealso::

  * :py:meth:`gooseeye.plot.subplot`

  * :py:meth:`gooseeye.plot.subplots`

  * `StackOverflow subplot size
    <http://stackoverflow.com/questions/6541123/
    improve-subplot-size-spacing-with-many-subplots-in-matplotlib>`_
  '''
  return plt.subplots_adjust(*args,**kwargs)

# ------------------------------------------------------------------------------
# get handle properties
# ------------------------------------------------------------------------------

def getp(*args,**kwargs):
  '''
Get the properties of a handle.
  '''
  return plt.getp(*args,**kwargs)

# ------------------------------------------------------------------------------
# title
# ------------------------------------------------------------------------------

def title(*args,**kwargs):
  '''
Set the figure's title.

:argument:

  **title** (``<str>``)
    The title.

.. seealso::

  `matplotlib.pyplot.title
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.title>`_
  '''

  kwargs.setdefault( 'fontsize' , mpl.rcParams['font.size']*32./36. )

  return plt.title(*args,**kwargs)

# ------------------------------------------------------------------------------
# limits
# ------------------------------------------------------------------------------

def xlim(*args,**kwargs):
  '''
Set the limits of the horizontal axis.

.. seealso::

  `matplotlib.pyplot.xlim
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.xlim>`_
  '''
  return plt.xlim(*args,**kwargs)


def ylim(*args,**kwargs):
  '''
Set the limits of the horizontal axis.

.. seealso::

  `matplotlib.pyplot.ylim
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.ylim>`_
  '''
  return plt.ylim(*args,**kwargs)


def clim(*args,**kwargs):
  '''
Set the color limits.

.. seealso::

  `matplotlib.pyplot.clim
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.clim>`_
  '''

  return plt.clim(*args,**kwargs)

# ------------------------------------------------------------------------------
# plot with boxplot
# ------------------------------------------------------------------------------

def boxplot(*args,**kwargs):
  '''
Make boxplot.

.. seealso::

  `matplotlib.pyplot.boxplot
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.boxplot>`_

  '''

  return plt.boxplot(*args,**kwargs)


# ------------------------------------------------------------------------------
# plot with errorbar
# ------------------------------------------------------------------------------

def errorbar(*args,**kwargs):
  '''
Plot with errorbar.

.. seealso::

  * `matplotlib.pyplot.errorbar
    <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.errorbar>`_


  * `StackOverflow errorbar
    <http://stackoverflow.com/questions/18498742/
    how-do-you-make-an-errorbar-plot-in-matplotlib-using-linestyle-none-in-rcparams>`_

  '''

  return plt.errorbar(*args,**kwargs)

# ------------------------------------------------------------------------------
# fill_between
# ------------------------------------------------------------------------------

def fill_between(*args,**kwargs):
  r'''
Plot two curves, with the area between the two curves filled. This function has
the custom behavior that by default the lines are plotted as thick lines.

:options:

  **linestyle** ([``'-'``] | ``'--'`` | ``'-.'`` | ``None``)
    The style of the lines which are plotted. If set to ``None``, the behavior
    of this function is the same as the default behavior of the matplotlib
    function.

  **alpha** ([``0.3``] | ``<float>``)
    The alpha of the area highlighted, between the two curves.

:returns:

  **fill_between** (``<matplotlib.handle>``)
    Handle of the area between the two curves.

  **plot1,plot2** (``<matplotlib.handle>``), *optional*
    Handles of the lines at the boundary of the area (only if ``linestyle`` if
    not ``None``). The order coincides with the order of the input arguments.

.. seealso::

  `matplotlib.pyplot.fill_between
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.fill_between>`_

  '''

  # set default "fill_between" options
  kwargs.setdefault('alpha',0.3)

  # extract "linestyle": if not None, additional lines are plotted
  linestyle = kwargs.pop('linestyle','-')

  # only "fill_between"
  if linestyle in ['none','None',None]:
    return plt.fill_between(*args,**kwargs)

  # modify default "plot" and "fill_between" options
  # - copy all options from the input options
  plotopt = {key:value for key,value in kwargs.iteritems()}
  # - set non-zero linewidth only for "plot"
  plotopt['linewidth'] = kwargs.pop('linewidth',mpl.rcParams['lines.linewidth'])
  kwargs ['linewidth'] = 0.0
  # - remove alpha from "plot"
  plotopt.pop('alpha',None)

  # plot "fill_between"
  hf = plt.fill_between(*args,**kwargs)

  # copy the color from "fill_between"
  plotopt.setdefault('color',plt.getp(hf,'facecolors')[0,:-1])

  # plot lines
  hp1 = plt.plot(args[0],args[1],**plotopt)

  if 'label' in plotopt:
    plotopt.pop('label')

  hp2 = plt.plot(args[0],args[2],**plotopt)

  # return all handles
  return (hf,hp1,hp2)

# ------------------------------------------------------------------------------
# text
# ------------------------------------------------------------------------------

def text(*args,**kwargs):
  '''
Plot text.

.. seealso::

  `matplotlib.pyplot.text
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.text>`_
  '''
  return plt.text(*args,**kwargs)

# ------------------------------------------------------------------------------
# plot
# ------------------------------------------------------------------------------

def plot(*args,**kwargs):
  '''
Plot curve.

:options:

  **imarker** (``(<slice>,<str>)`` | ``(<slice>,r'$...$')``)
    Plot a marker on specific data-points, for example ``imarker=(-1,'o')``.

:other options:

  **linestyle** ([``'-'``] | ``'--'`` | ``'-.'`` | ``':'``)
    Set the linestyle.

  **dashes** (``(<int>,<int>) = (width,spacing)``)
    If the linestyle is dashed (``'--'``) the size of the dashed and their
    spacing can be modified using this options.

  **marker** (``<str>`` | ``r'$...$'``)
    Plot markers at the data-points. Notice the nice feature that LaTeX-text
    can be used as marker.

  **markersize, ms** ([``6``] | ``<int>``)
    Size of the marker.

  **markeredgecolor, mec** (``<str>`` | ``<tuple>`` | ... )
    Color of the marker's edges.

  **markeredgewidth, mew** ([``0.5``] | ``<float>``)
    Width of the marker's edge.

  **markerfacecolor, mfc**  (``<str>`` | ``<tuple>`` | ... | ``'none'``)
    Color of the marker's face.

  **markevery** ([``None``] | ``<int>``)
    Subsample the plot when using markers.

.. seealso::

  * `matplotlib.pyplot.plot
    <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot>`_

  * `matplotlib.markers
    <http://matplotlib.org/api/markers_api.html>`_

  * `matplotlib.markers: more information
    <http://matplotlib.org/api/
    lines_api.html#matplotlib.lines.Line2D.set_markevery>`_
  '''

  imarker = kwargs.pop('imarker',None)

  handle = plt.plot(*args,**kwargs)

  if imarker:
    kwargs.pop('marker',None)
    imarker = list(imarker)
    args    = tuple([arg[imarker[0]] for arg in args])
    plt.plot(*args,marker=imarker[1],**kwargs)

  return handle

# ------------------------------------------------------------------------------
# scatter
# ------------------------------------------------------------------------------

def scatter(*args,**kwargs):
  '''
Scatter plot.

.. seealso::

  `matplotlib.pyplot.scatter
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.scatter>`_
  '''
  return plt.scatter(*args,**kwargs)

# ------------------------------------------------------------------------------
# fill
# ------------------------------------------------------------------------------

def fill(x,y,**kwargs):
  '''
Plot the area below enclosed by the polygon. Using the ``zeropad`` option this
corresponds to the area below the curve.

:options:

  **zeropad** ([``True``] | ``False``)
    Zero-pad the function left and right. This effectively causes the function
    to plot the area below the curve.

  **linestyle** ([``'-'``] | ``'--'`` | ``'-.'`` | ``':'``)
    Plot the original line.

  **linestyle** ([``'solid'``] | ``'dashed'`` | ``'dashdot`` | ``'dotted'``)
    Plot a line around the highlighted area.

.. seealso::

  `matplotlib.pyplot.fill
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.fill>`_
  '''

  # extract custom options
  zeropad   = kwargs.pop('zeropad'  ,True)
  linestyle = kwargs.pop('linestyle','-' )

  if linestyle in ['solid','dashed','dashdot','dotted']:
    kwargs[linestyle] = linestyle
    line              = False
  elif linestyle in ['-','--','-.',':']:
    line              = True

  # store data
  if line:
    x0  = np.array(x,copy=True)
    y0  = np.array(y,copy=True)
    opt = {key:val for key,val in kwargs.iteritems() if key in ['color','linewidth']}

  # apply zero-padding (optional)
  if zeropad:
    x = np.hstack(( x[0]*np.ones ((1)) , x , x[-1]*np.ones ((1)) ))
    y = np.hstack((      np.zeros((1)) , y ,       np.zeros((1)) ))

  # plot filled plot
  hfill = plt.fill(x,y,**kwargs)

  if line:
    hline = plt.plot(x0,y0,**opt)
    return (hfill,hline)
  else:
    return hfill

# ------------------------------------------------------------------------------
# imshow
# ------------------------------------------------------------------------------

def imshow(*args,**kwargs):
  '''
Show a two-dimensional matrix.

:argument:

  im (``<numpy.ndarray>``)
    The image (2D matrix).

:options:

  interpolation ([``'nearest'``] | ``<str>``)
    Set the interpolation. See the matplotlib documentation for more values.

  extent (``(left,right,bottom,top) = (<float>,<float>,<float>,<float>)``)
    The location, in data-coordinates, of the lower-left and upper-right
    corners. If None, the image is positioned such that the pixel centers fall
    on zero-based (row, column) indices.

.. note::

  To extend the colormap with a color that indicates values that are out of
  range:

  .. code-block:: python

    cs = gplot.imshow()
    cs.cmap.set_under('k')
    cs.cmap.set_over ('r')

.. seealso::

  `matplotlib.pyplot.imshow
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.imshow>`_
  '''

  # remove custom options
  clim = kwargs.pop('clim',None)

  # set default options
  kwargs.setdefault('interpolation','nearest')

  # plot
  h = plt.imshow(*args,**kwargs)

  # apply color limit
  if clim is not None:
    plt.clim(clim)

  return h


# ------------------------------------------------------------------------------
# legend
# ------------------------------------------------------------------------------

def legend(**kwargs):
  '''
Add legend to the plot.

:options:

  **loc** ([``'upper right'``] | ``<str>``)
    The location of the legend.

  **fancybox** ([``True``] | ``<bool>``)
    Show a box with rounded edges.

  **shadow** ([``True``] | ``<bool>``)
    Show a shadow behind the box, to create an optical effect of the legend
    being placed on top of the plot.

  **columnspacing** ([``1.0``] | ``<float>``)
    Spacing between columns. Using ``ncol`` to obtain more than one column.

  **handletextpad** ([``0.2``] | ``<float>``)
    The length between the legend handle and text. Measured in font-size units.

  **fontsize** (``<float>``), *automatic*
    The font-size. If not specified the font-size is automatically calculated
    from the font-size set in ``gooseeye.plot.figure``.

  **numpoints** (``<int>``), *optional*
    Specify the number of markers to include.

:labels match colors:

  .. code-block:: python

    # define colors
    colors = [ 'r' , 'b' ]

    # plot
    gplot.figure()
    gplot.plot(x,ya,color=colors[0],label='...')
    gplot.plot(x,yb,color=colors[1],label='...')

    # create a legend from plot
    handle = gplot.legend(...)

    # convert the colors
    for color,text in zip(colors,leg.get_texts()):
      text.set_color(color)

.. seealso::

  `matplotlib.pyplot.legend
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend>`_
  '''

  # set defaults
  kwargs.setdefault('fontsize'     ,mpl.rcParams['font.size']*32./36. )
  kwargs.setdefault('loc'          ,'upper right'                     )
  kwargs.setdefault('fancybox'     ,True                              )
  kwargs.setdefault('shadow'       ,True                              )
  kwargs.setdefault('columnspacing',1.0                               )
  kwargs.setdefault('handletextpad',0.2                               )

  # add legend
  return plt.legend(**kwargs)

# ==============================================================================
# plot a square area
# ==============================================================================

def square(x0,y0,dx,dy,**kwargs):
  '''
Plot a square box. The default plot options can be used.

:arguments:

  **x0,y0,dx,dy** (``<float>``)
    The coordinates of the lower-left corner, and the width and height.

.. seealso::

  `matplotlib.pyplot.plot
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot>`_
  '''

  x = np.array([
    [ x0    , x0+dx ],
    [ x0+dx , x0+dx ],
    [ x0    , x0+dx ],
    [ x0    , x0    ],
  ])

  y = np.array([
    [ y0    , y0    ],
    [ y0    , y0+dy ],
    [ y0+dy , y0+dy ],
    [ y0    , y0+dy ],
  ])

  return plt.plot(x.T,y.T,**kwargs)


# ==============================================================================
# convert figure's axes
# ==============================================================================

# ------------------------------------------------------------------------------
# apply the ticks to the axes
# ------------------------------------------------------------------------------

def axistick(axis,tick,ticklabels,position=False,fontsize=None,shift_first=True):
  r'''
Apply labels at specified positions along the specified axis.

:arguments:

  **axis** (``'xl'`` | ``'yl'`` | ``'xu'`` | ``'yu'``)
    Specify the axis to which the label is applied:

    * horizontal (``x``) or vertical (``y``)
    * lower limit (``l``) or upper limit (``u``)

  **tick** (``<list>`` | ``<numpy.ndarray>``)
    Positions at which to apply the ``ticklabels``.

  **ticklabels** (``<list>``)
    Labels that are assigned to the positions of ``tick``.

:options:

  **position** ([``False``] | ``<float>``)
    The fixed position in the x- or y-direction (the direction opposite to that
    of ``tick``, selected automatically depending on ``axis``). By default it
    is calculated automatically based on the size of the axes.

  **fontsize** ([``None``] | ``<float>``)
    Font-size. If set to ``None`` the font-size is automatically calculated from
    the font-size set in ``gooseeye.plot.figure``.

  **shift_first** ([``True``] | ``False``)
    Shift the first label on the y-axis upwards.

:returns:

  **handle** (``<matplotlib>``)
    Handle of the tick-labels (``matplotlib.pylab.text``).
  '''

  # set default
  if fontsize is None:
    fontsize = mpl.rcParams['font.size']*32./36.

  # check input
  if axis not in ['xl','xu','yl','yu']:
    raise IOError('Unknown "axis" input')

  # correct text "-0.0" to "0.0"
  for (icol,ilab) in enumerate(ticklabels):
    if ilab=='$-0.0$':
      ticklabels[icol] = '$0.0$'

  # if no position is specified: calculate using the axis
  if not position:
    # determine the relevant axis length
    if axis[0]=='x':
      l0 = plt.getp(plt.gca(),'ylim')
    elif axis[0]=='y':
      l0 = plt.getp(plt.gca(),'xlim')
    # calculate a shift length
    dl = (l0[1]-l0[0])/100.
    # store the position
    if axis[1]=='l':
      position = l0[0]-dl
    elif axis[1]=='u':
      position = l0[1]+dl

  # remove the current labels, and set the tick markers
  if axis=='xl':
    plt.xticks(tick,['']*len(tick))
  elif axis=='yl':
    plt.yticks(tick,['']*len(tick))

  # combine data
  if axis[0]=='x':
    dat = zip(tick,position*np.ones(len(tick)),ticklabels)
  elif axis[0]=='y':
    dat = zip(position*np.ones(len(tick)),tick,ticklabels)

  # plot the labels and store the handles
  handle = []
  for (ix,iy,it) in dat:
    handle.append(plt.text(ix,iy,it))

  # apply the alignment
  if axis[0]=='x':
    plt.setp(handle,horizontalalignment='center')
    if axis[1]=='l':
      plt.setp(handle,verticalalignment='top')
    elif axis[1]=='u':
      plt.setp(handle,verticalalignment='bottom')
  elif axis[0]=='y' and len(handle)>0:
    plt.setp(handle,verticalalignment='center')
    if shift_first:
      plt.setp(handle[0],verticalalignment='bottom')
    if axis[1]=='l':
      plt.setp(handle,horizontalalignment='right')
    elif axis[1]=='u':
      plt.setp(handle,horizontalalignment='left')

  # apply the font-size
  plt.setp(handle,fontsize=fontsize)

  return handle

# ------------------------------------------------------------------------------
# define ticks and labels and apply to axis
# ------------------------------------------------------------------------------

def axis(*args,**kwargs):
  '''
Apply custom limits to the axis.

:arguments:

  **xlim** (``(min,max)``)
    The lower and upper limits in x-direction.

  **ylim** (``(min,max)``)
    The lower and upper limits in y-direction.

:options:

  **ntick** ([``(11,11)``] | ``(<int>,<int>)``)
    Number of tick labels along each axis.

  **niter** ([``(1,1)``] | ``(<int>,<int>)``)
    The the number of labels to skip along each axis. The function does show a
    marking on the axis for the label that is skipped.

  **fmt** ([``('$%f$','$%f$')``] | ``(<str>,<str>)``)
    Print format for the tick labels.

  **xdelta,ydelta** ([``False``] | ``<float>``)
    Offset in x- and y-direction. If set to ``False`` default values are
    calculated from the axis limits.

  **visible** ([``True``] | ``False``)
    Handle visibility of the axis.

  **equal** ([``False``] | ``True``)
    Set the aspect-ratio to equal.

  **fontsize** (``<float>``), *automatic*
    Font-size. If not specified the font-size is automatically calculated from
    the font-size set in ``gooseeye.plot.figure``.

:returns:

  **handle** (``<list>``)
    Handles of the x- and the y-axis.
  '''

  # input arguments / options / defaults
  # ------------------------------------

  # convert argument to keyword-arguments
  if len(args)==2:
    kwargs['xlim'] = args[0]
    kwargs['ylim'] = args[1]
  elif len(args)==1:
    if len(args)!=4:
      raise IOError('Ambiguous input, use keyword arguments')
    xlim = [ args[0] , args[1] ]
    ylim = [ args[0] , args[1] ]
  elif len(args)!=0:
    raise IOError('Ambiguous input, use keyword arguments')

  # convert combined arguments to single argument
  for key in ['lim','ntick','niter','fmt','tick']:
    if key in kwargs:
      (kwargs['x'+key],kwargs['y'+key]) = kwargs.pop(key)

  # initiate dictionaries (simplifies implementation)
  lim   = {}
  tick  = {}
  niter = {}
  ntick = {}
  fmt   = {}
  delta = {}
  dim   = ['x','y']
  # extract / set defaults
  lim  ['x'] = kwargs.pop( 'xlim'      , plt.getp(plt.gca(),'xlim')        )
  lim  ['y'] = kwargs.pop( 'ylim'      , plt.getp(plt.gca(),'ylim')        )
  tick ['x'] = kwargs.pop( 'xtick'     , False                             )
  tick ['y'] = kwargs.pop( 'ytick'     , False                             )
  niter['x'] = kwargs.pop( 'xniter'    , 1                                 )
  niter['y'] = kwargs.pop( 'yniter'    , 1                                 )
  ntick['x'] = kwargs.pop( 'xntick'    , 11                                )
  ntick['y'] = kwargs.pop( 'yntick'    , 11                                )
  fmt  ['x'] = kwargs.pop( 'xfmt'      , '%1.2f'                           )
  fmt  ['y'] = kwargs.pop( 'yfmt'      , '%1.2f'                           )
  delta['x'] = kwargs.pop( 'xdelta'    , False                             )
  delta['y'] = kwargs.pop( 'ydelta'    , False                             )
  fontsize   = kwargs.pop( 'fontsize'  , mpl.rcParams['font.size']*32./36. )
  visible    = kwargs.pop( 'visible'   , True                              )

  # function to select label
  # ------------------------

  def label_select(lab,nlab,niter):
    # initiate list of labels to hide
    hide = np.ones((nlab),dtype='bool')
    # loop over labels that are shown
    i = 0
    while i<nlab:
      hide[i] = False
      i += niter
    # hide labels
    for i in np.where(hide)[0]:
      lab[i] = ''
    # close
    return lab

  # define labels
  # -------------

  # tick positions
  tp = {i:np.linspace(lim[i][0],lim[i][1],ntick[i]) for i in dim}

  # shift factors
  for (i,j) in zip(dim,dim[::-1]):
    if not delta[i]:
      delta[i] = (lim[j][1]-lim[j][0])/100.

  # convert tick positions to text
  for i in dim:
    if not tick[i]:
      tick[i] = [fmt[i]%j for j in tp[i]]

  # empty labels
  for i in dim:
    if niter[i]>1:
      tick[i] = label_select(tick[i],ntick[i],niter[i])

  # apply limits
  plt.xlim(lim['x'])
  plt.ylim(lim['y'])
  # set visibility
  if not visible:
    plt.xticks([])
    plt.yticks([])
    plt.gca().spines['left'  ].set_color('none')
    plt.gca().spines['right' ].set_color('none')
    plt.gca().spines['bottom'].set_color('none')
    plt.gca().spines['top'   ].set_color('none')
    return
  # plot labels
  hx = axistick('xl',tp['x'],tick['x'],position=lim['y'][0]-delta['x'],fontsize=fontsize)
  hy = axistick('yl',tp['y'],tick['y'],position=lim['x'][0]-delta['y'],fontsize=fontsize)

  # set axis equal
  if kwargs.pop('equal',False):
    plt.gca().set_aspect('equal')

  return (hx,hy)

# ------------------------------------------------------------------------------
# set the axis labels
# ------------------------------------------------------------------------------

def axislabel(*args,**kwargs):
  '''
Apply labels to the x- and y-axis.

:options:

  **xlabel,ylabel** (``False`` | ``<str>``)
    Label on the x- and/or y-axis.

  **delta** ([``False``] | ``(<float>,<float>)``)
    Offset in x- and y-direction. If set to ``False``, default values are
    calculated from the axis limits.

  **rotation** ([``(0,90)``] | ``(<float>,<float>)``)
    Rotation -- in degrees -- of the x- and y-label.

  **fontsize** (``<float>``), *automatic*
    Font-size. If not specified the font-size is automatically calculated from
    the font-size set in ``gooseeye.plot.figure``.

:returns:

  **handle** (``(<matplotlib>,<matplotlib>)``)
    Handle(s) of the axis.
  '''

  # input arguments / options / defaults
  # ------------------------------------

  # convert argument to keyword-arguments
  if len(args)==2:
    kwargs['xlabel'] = args[0]
    kwargs['ylabel'] = args[1]
  elif len(args)!=0:
    raise IOError('Ambiguous input, use keyword arguments')

  # convert combined arguments to single argument
  for key in ['label','rotation']:
    if key in kwargs:
      (kwargs['x'+key],kwargs['y'+key]) = kwargs.pop(key)

  # extract / set defaults
  xlab      = kwargs.pop( 'xlabel'    , False                     )
  ylab      = kwargs.pop( 'ylabel'    , False                     )
  fontsize  = kwargs.pop( 'fontsize'  , mpl.rcParams['font.size'] )
  delta     = kwargs.pop( 'delta'     , False                     )
  xrotation = kwargs.pop( 'xrotation' , 0                         )
  yrotation = kwargs.pop( 'yrotation' , 90                        )

  # read data to set position
  # -------------------------

  # get the axis limits
  xlim = plt.getp(plt.gca(),'xlim')
  ylim = plt.getp(plt.gca(),'ylim')
  # set shift factors
  if not delta:
    delta = [ (ylim[1]-ylim[0])/9.*1.2 , (xlim[1]-xlim[0])/7.5 ]

  # apply labels
  # ------------

  # initiate handle
  h = []

  # add labels
  if xlab:
    hx = plt.text((xlim[0]+(xlim[1]-xlim[0])/2.),ylim[0]-delta[0],xlab)
    plt.setp(hx,size=fontsize,rotation=xrotation)
    plt.setp(hx,horizontalalignment='center',verticalalignment='bottom')
    h += [hx]
  if ylab:
    hy = plt.text(xlim[0]-delta[1],(ylim[0]+(ylim[1]-ylim[0])/2.),ylab)
    plt.setp(hy,size=fontsize,rotation=yrotation)
    plt.setp(hy,horizontalalignment='left',verticalalignment='center')
    h += [hy]

  if len(h)==1:
    return h[0]
  else:
    return tuple(h)

# ------------------------------------------------------------------------------
# alias for "axis" and "axislabel"
# ------------------------------------------------------------------------------

def axes(**kwargs):
  '''
Custom axes layout. This function serves as alias for the functions ``axis`` and
``axislabel``, accepting most of their defaults.

:options:

  **xlim** (``(min,max)``)
    The lower and upper limits in x-direction.

  **ylim** (``(min,max)``)
    The lower and upper limits in y-direction.

  **ntick** ([``(11,11)``] | ``(<int>,<int>)``)
    Number of tick labels along each axis.

  **fmt** ([``('$%f$','$%f$')``] | ``(<str>,<str>)``)
    Print format for the tick labels.

  **xlabel,ylabel** (``<str>``)
    Label on the x- and/or y-axis.

  **label_delta** (``<list>``)
    The relative distance at which to set the x- and y-label respectively. If
    not specified, this is set automatically.

  **visible** ([``True``] | ``False``)
    Handle visibility of the axis.

  **equal** ([``False``] | ``True``)
    Set the aspect-ratio to equal.

:returns:

  **handle** (``(<matplotlib>,<matplotlib>,<matplotlib>,<matplotlib>)``)
    Handle(s) of the axes and the labels.
  '''

  # extract label commands
  lb = []
  for name in ['label','rotation']:
    lb += ['x'+name , 'y'+name , name ]
  lb = { i:kwargs[i] for i in kwargs if i in lb }
  if 'label_delta' in kwargs:
    lb['delta'] = kwargs.pop('label_delta')

  # extract axis commands
  ax = []
  for name in ['lim','ntick','fmt','niter','visible','equal']:
    ax += ['x'+name , 'y'+name , name ]
  ax = { i:kwargs[i] for i in kwargs if i in ax }

  # run local functions
  hlim = axis(**ax)
  hlab = axislabel(**lb)

  return (hlim,hlab)

# ------------------------------------------------------------------------------
# remove top and right part of the axis
# ------------------------------------------------------------------------------

def axispart():
  '''
Remove the top and right part of the axis.
  '''

  # get the current axis
  ax = plt.gca()
  # turn off right and top axis
  ax.spines['right'].set_color('none')
  ax.spines['top'].set_color('none')
  ax.xaxis.set_ticks_position('bottom')
  ax.yaxis.set_ticks_position('left')
  #ax.spines['bottom'].set_position(('data',0))
  #ax.spines['left'].set_position(('data',0))

# ==============================================================================
# colormaps
# ==============================================================================

# ------------------------------------------------------------------------------
# simple line identification
# ------------------------------------------------------------------------------

def colorid(cmap='tue',dtype='numpy'):
  '''
Create a colormap to identify curves.

:options:

  **cmap** (``<str>``)
    * "tue": warmred, red, pink, darkpink, darkblue, blue, lightblue,
      orange, yellow, lemon, lime, green, cornflower blue
    * "tuewarmred"
    * "tuedarkblue"
    * "tueblue"
    * "tuelightblue"
    * "GrBu": gray and blue

  **dtype** ([``'cm'``] | ``'numpy'`` | ``'xml'``)
    Specify the output type.

:returns:

  **cmap** (``<matplotlib.color>`` | ``<numpy.ndarray[N,3]>`` | ``<str>``)
    The colormap in the selected "dtype".
  '''

  out = None

  if cmap=='tue':
    out = np.array([
      [ 247,  49,  49 ],  #  0: warm red
      [ 214,   0,  74 ],  #  1: red
      [ 214,   0, 123 ],  #  2: pink
      [ 173,  32, 173 ],  #  3: dark pink
      [  16,  16, 115 ],  #  4: dark blue
      [   0, 102, 204 ],  #  5: blue
      [   0, 162, 222 ],  #  6: light blue
      [ 255, 154,   0 ],  #  7: orange
      [ 255, 221,   0 ],  #  8: yellow
      [ 206, 223,   0 ],  #  9: lemon
      [ 132, 210,   0 ],  # 10: lime
      [   0, 172, 130 ],  # 11: green
      [   0, 146, 181 ],  # 12: cornflower blue
    ],dtype='float')/255.
  elif cmap=='white':
    out = np.array([[ 255, 255, 255 ]],dtype='float')/255.
  elif cmap=='tuedarkblue':
    out = np.array([[  16,  16, 115 ]],dtype='float')/255.
  elif cmap=='tueblue':
    out = np.array([[   0, 102, 204 ]],dtype='float')/255.
  elif cmap=='tuelightblue':
    out = np.array([[   0, 162, 222 ]],dtype='float')/255.
  elif cmap=='tuewarmred':
    out = np.array([[ 247,  49,  49 ]],dtype='float')/255.
  elif cmap=='GrBu':
    out = np.array([
      [ 240, 240, 240 ],  # gray
      [  50,  50, 120 ],  # blue
    ],dtype='float')/255.
  elif cmap=='GrBu_r':
    out = np.array([
      [  50,  50, 120 ],  # blue
      [ 240, 240, 240 ],  # gray
    ],dtype='float')/255.

  if out is None:
    raise IOError('''
Unknown colormap, the following are available:

* tue
* tuedarkblue
* tueblue
* tuelightblue
* tuewarmred
* GrBu
* GrBu_r
* white
    ''')

  return ascolormap(out,dtype=dtype,name='gooseeye.plot.%s'%cmap)

# ------------------------------------------------------------------------------
# convert to colormap
# ------------------------------------------------------------------------------

def ascolormap(cmap,dtype='cm',N=256,name='gooseeye.plot.ascolormap'):
  '''
Convert input to colormap (as matplotlib object of NumPy-array).

:options:

  **cmap** (``<list>`` | ``<str>`` | ``<numpy.ndarray[N,4]>``)
    Depending on the type of the entry/entries:

    * string: the name of a "matplotlib.cm" colormap, the colormap is read
      internally;
    * matrix: completely custom colormap.

  **N** ([``256``] | ``<int>``)
    Number of colors in the returned colormap. If a NumPy array is inputted
    ``N`` is modified according to it.

  **name** ([``"gooseeye.plot.ascolormap"``] | ``<str>``)
    Specify the name of the colormap.

  **dtype** ([``'cm'``] | ``'numpy'`` | ``'xml'``)
    Specify the output type.

:returns:

  **cmap** (``<matplotlib.color>`` | ``<numpy.ndarray[N,4]>`` | ``<str>``)
    The colormap in the selected "dtype".
  '''

  import matplotlib

  # convert to NumPy-array
  if type(cmap)==str:
    cmap = eval('matplotlib.cm.%s(np.arange(%d))'%(cmap,N))

  # check RGBa format
  if cmap.shape[1]==3:
    cmap = np.hstack((
      cmap ,
      np.ones((cmap.shape[0],1),dtype=cmap.dtype)
    ))

  # update the number of colors in the colormap
  N = cmap.shape[0]

  # return as matrix
  if dtype.lower() in ['numpy','matrix','np']:
    if cmap.shape[0]==1:
      return cmap[0,:]
    else:
      return cmap

  # return as "matplotlib" colormap
  if dtype.lower() in ['cm','cmap']:
    return matplotlib.colors.ListedColormap(cmap,name=name,N=N)

  # return as "xml" file
  if dtype.lower()=='xml':

    cmap = cmap[:,:-1]
    out  = ['<ColorMap name="%s" space="RGB">'%name]
    for i,c in zip(np.linspace(0.0,1.0,N),cmap):
      out += ['<Point x="%10.8f" o="1" r="%10.8f" g="%10.8f" b="%10.8f"/>' % tuple([i]+list(c))]
    out += ['</ColorMap>']

    return '\n'.join(out)

# ------------------------------------------------------------------------------
# custom (re-interpolated) colormap
# ------------------------------------------------------------------------------

def colormap(*args,**kwargs):
  '''
Create custom colormap. The colormap is based on one or more existing colormaps.
The following operations are implemented:

* combine existing colormaps,
* exponential (in-stead of linear) color-interpolation,
* inhomogeneous color-interpolation.

:options:

  **cmap** (``<list>`` | ``<str>`` | ``<numpy.ndarray[N,4]>``)
    Depending on the type of the entry/entries:

    * string: the name of a "matplotlib.cm" colormap, the colormap is read
      internally
    * matrix: completely custom colormap.

  **N** ([``256``] | ``<int>``)
    Number of colors in the returned colormap.

  **ncol** (``<list>``)
    Number of colors to include in each part of the inhomogeneous interpolation.
    If the input is a float it is interpreted as the fraction of the colors
    (i.e. (i.e. 0.0 <= fraction <= 1.0).

  **interp** ([``"linear"``] | ``"exponential"``)
    Different interpolation modes.

  **exp** (``<list>``) *only for "exponential interpolation"*
    The exponent used in the interpolation of each of the colormaps.

  **direction** (``<list>``) *only for "exponential interpolation"*
    The direction in which to define the interpolation.

  **name** ([``"gplot.colormap"``] | ``<str>``)
    Specify the name of the colormap.

  **dtype** ([``'cm'``] | ``'numpy'`` | ``'xml'``)
    Specify the output type.

:returns:

  **cmap** (``<matplotlib.color>`` | ``<numpy.ndarray[N,4]>`` | ``<str>``)
    The colormap in the selected "dtype".

:example:

  A colormap with two colors which increase exponentially from the neutral
  color in the center::

    gplot.colormap(
      cmap      = ['Greys_r','Reds'],
      interp    = 'exp',
      direction = [   1,  -1],
      ncol      = [0.25,0.75],
      exp       = [   2,   6],
    )
  '''

  import matplotlib

  # convert argument to named arguments
  if len(args)>0 and 'cmap' not in kwargs: kwargs['cmap'] = list(*args         )
  elif               'cmap'     in kwargs: kwargs['cmap'] = list(kwargs['cmap'])

  # read/convert input
  # ------------------

  # read options
  N         = kwargs.pop( 'N'         , 256             )
  cmap      = kwargs.pop( 'cmap'      , 'Reds'          )
  interp    = kwargs.pop( 'interp'    , 'lin'           )
  exp       = kwargs.pop( 'exp'       , 5.0             )
  ncol      = kwargs.pop( 'ncol'      , None            )
  direction = kwargs.pop( 'direction' , None            )
  dtype     = kwargs.pop( 'dtype'     , 'cm'            )
  name      = kwargs.pop( 'name'      , 'gplot.colormap')

  # read the number of colormaps
  if type(cmap)==str:
    cmap = [cmap]
    nmap = 1
  elif type(cmap)==list:
    nmap = len(cmap)
  else:
    cmap = [cmap]
    nmap = 1

  # convert type of other options
  tolist = lambda x,y: [x for i in range(y)] if type(x)!=list else x
  interp = tolist(interp,nmap)
  exp    = tolist(exp   ,nmap)

  # read colormap from library
  for imap in range(nmap):
    if type(cmap[imap])==str:
      cmap[imap] = eval('matplotlib.cm.%s(np.arange(256))'%cmap[imap])
    elif len(cmap[imap].shape)==1:
      cmap[imap] = np.tile(cmap[imap],(2)).reshape(2,-1)
  # check
  for imap in range(nmap):
    if cmap[imap].shape[1]==3:
      cmap[imap] = np.hstack((
        cmap[imap] ,
        np.ones((cmap[imap].shape[0],1),dtype=cmap[imap].dtype)
      ))

  # calculate number of colors
  if ncol is None:
    ncol = np.ones((nmap),dtype='float')/float(nmap)
  else:
    ncol = np.array(ncol)
  # convert to integer
  if ncol.dtype=='float':
    ncol = (ncol*float(N)).astype(np.int)
  # check
  ncol[-1] += N-np.sum(ncol)

  # initiate direction
  if direction is None:
    direction = [1 for i in range(nmap)]

  # create colormap
  # ---------------

  # initiate new colormap
  out  = np.zeros((N,4),dtype='float')
  iout = 0

  # combine colormaps to new colormap
  for color,interpolation,exponent,n,d in zip(cmap,interp,exp,ncol,direction):

    if ( color.shape[0]==n ) and ( interpolation in ['lin','linear'] ):

      out[iout:iout+n,:] = color

    else:

      # define current axis
      xc = np.linspace(0.0,1.0,color.shape[0])
      # define interpolation axis
      if interpolation in ['lin','linear']:
        xn = np.linspace(0.0,1.0,n)
      elif interpolation in ['exp','exponential']:
        if d>=0:
          xn = np.hstack((0.0,np.logspace(0.1,exponent,n-1)/10.**exponent))
        else:
          xn = 1.0-np.hstack((0.0,np.logspace(0.1,exponent,n-1)/10.**exponent))[::-1]
      else:
        raise IOError('Unknown interpolation')
      # interpolate
      for i in range(out.shape[1]):
        out[iout:iout+n,i] = np.interp(xn,xc,color[:,i])

    # update current counter
    iout += n

  # return output
  # -------------

  return ascolormap(out,dtype=dtype,name=name)

# ------------------------------------------------------------------------------
# show matplotlib-colormaps
# ------------------------------------------------------------------------------

def colormap_show(cmap=None):
  '''
Show colormap(s) to the screen. If no colormap is specified, all available
colormaps in "matplotlib.cm" are shown.

:options:

  **cmap** ([``None``] | ``<list>`` | ``<matplotlib>``)
    The colormaps to plot.

:standard:

  ======== ====================================
  name     description
  ======== ====================================
  Greys    white        -> black
  binary   white        -> black
  gray     black        -> white
  Blues    white        -> blue
  PuBu     white        -> blue
  BuPu     white/blue   -> purple
  YlOrBr   yellow       -> brown
  YlOrRd   yellow       -> red
  PuRd     white/purple -> red
  RdPu     white/red    -> purple
  YlGn     white/yellow -> green
  Purples  white        -> purple
  Greens   white        -> green
  Oranges  white/orange -> orange
  Reds     white        -> red
  OrRd     white/orange -> red
  RdGy     red          -> white    -> grey
  RdBu     red          -> white    -> blue
  RdYlBu   red          -> yellow   -> blue
  RdYlGn   red          -> yellow   -> green
  BrBG     brown        -> white    -> green
  PuOr     brown        -> white    -> purple
  PRGn     purple       -> white    -> green
  PiYG     pink         -> white    -> green
  ======== ====================================
  '''

  if cmap is not None:

    if not (type(cmap)==list or type(cmap)==tuple):
      cmap = [cmap]

    # set data
    a = np.outer(np.arange(0,1,0.01),np.ones(10))
    # new figure
    plt.figure(figsize=(10,5))
    plt.subplots_adjust(top=0.8,bottom=0.05,left=0.01,right=0.99)
    # read colormaps
    l=len(cmap)+1
    # plot all the colormaps
    for i,c in enumerate(cmap):
      plt.subplot(1,l,i+1)
      plt.axis("off")
      plt.imshow(a,aspect='auto',cmap=c,origin="lower")

    plt.show()

  else:

    import matplotlib.cm as cm

    plt.rc('text', usetex=False)

    # set data
    a = np.outer(np.arange(0,1,0.01),np.ones(10))
    # new figure
    plt.figure(figsize=(10,5))
    plt.subplots_adjust(top=0.8,bottom=0.05,left=0.01,right=0.99)
    # read colormaps
    maps=[m for m in cm.datad if not m.endswith("_r")]
    maps.sort()
    l=len(maps)+1
    # plot all the colormaps
    for i, m in enumerate(maps):
      plt.subplot(1,l,i+1)
      plt.axis("off")
      plt.imshow(a,aspect='auto',cmap=plt.get_cmap(m),origin="lower")
      plt.title(m,rotation=90,fontsize=10)

    plt.show()

# ==============================================================================
# plot a separate colorbar
# ==============================================================================

def colorbar(clim,**kwargs):
  '''
Plot colorbar as separate figure.

:arguments:

  **clim** (``<list>``)
    Limits (lower and upper) of the coloraxis.

:options:

  **cmap** ([``None``] | ...)
    Set colormap.

  **ntick** ([``7``] | ``<int>``)
    Set the number of tick labels.

  **fmt** ([``'%f'``] | ``<str>``)
    Print format to create ``ticklabel`` form ``tick``.

  **tick** (``<list>``) *optional*
    Values to show tick-labels along the colorbar.

  **ticklabel** (``<list>``) *optional*
    Labels corresponding to each of the tick points.

  **label** (``<str>``)
    Add label.

  **labelpad** ([``-150``] | ``<float>``)
    Shift of the label.

  **orientation** ([``None``] | ``vertical`` | ``horizontal``)
    Set the orientation of the colorbar.

  **figsize** ([``(10,10)``] | ``<tuple>``)
    Figure size.

  **figure** ([``True``] | ``False``)
    Open a new figure.
  '''

  # keyword-arguments: read / assign default
  # ----------------------------------------

  # set option defaults
  opt = {}
  opt['tick']        = None
  opt['ticklabel']   = None
  opt['fmt']         = '%f'
  opt['ntick']       = 7
  opt['label']       = None
  opt['figure']      = True
  opt['figsize']     = (10,10)
  opt['cmap']        = None
  opt['orientation'] = 'vertical'
  opt['label']       = None
  opt['labelpad']    = -150

  # check options exclusiveness
  if 'ntick' in kwargs and 'tick' in kwargs:
    raise IOError('"ntick" and "tick" are exclusive')
  if 'ticklabel' in kwargs and 'fmt' in kwargs:
    raise IOError('"ticklabel" and "fmt" are exclusive')

  # read option from input
  for name in kwargs:
    if name not in opt:
      raise IOError('Unknown option %s'%name)
    else:
      opt[name] = kwargs[name]
  # convert short-name for orientations
  alias = dict(v='vertical'  ,ver='vertical'  ,vertical='vertical'    ,\
               h='horizontal',hor='horizontal',horizontal='horizontal')
  opt['orientation'] = alias[opt['orientation']]

  # function to extract options
  def extract(opt,name):
    v = opt[name]
    del opt[name]
    return v
  # set options as variables
  ntick    = extract(opt,'ntick')
  figsize  = extract(opt,'figsize')
  fig      = extract(opt,'figure')
  cmap     = extract(opt,'cmap')
  label    = extract(opt,'label')
  labelpad = extract(opt,'labelpad')

  # set default tick positions
  if opt['tick'] is None:
    opt['tick'] = np.linspace(clim[0],clim[1],ntick)
  # convert data
  clim = np.array([clim])

  # open new figure
  if fig:
    if figsize is None:
      figure()
    else:
      figure(figsize=figsize)

  # plot data with a certain colorbar
  if cmap is None:
    h = plt.imshow(clim)
  else:
    h = plt.imshow(clim,cmap=cmap)

  # remove axis
  plt.gca().set_visible(False)
  cbar = coloraxis(h,**opt)

  # add label
  if label is not None:
    cbar.set_label(label,labelpad=labelpad)

  # return handle
  return cbar

# ==============================================================================
# add a colorbar to existing figure
# ==============================================================================

def coloraxis(handle,tick=None,ticklabel=None,fmt='$%f$',label=None,orientation=None):
  '''
Add a colorbar to the figure. Remark: when saving the figure it is advised to
use the following::

  import matplotlib.pylab as plt
  plt.savefig('example.pdf',bbox_inches='tight')

:arguments:

  **handle** (``<matplotlib>``)
    Handle of the color-object.

:options:

  **tick** (``<list>``)
    Values to show tick-labels along the colorbar.

  **ticklabel** (``<list>``)
    Labels corresponding to each of the tick points.

  **fmt** (``<str>``)
    Print format to create ``ticklabel`` form ``tick``.

  **label** (``<str>``)
    Add label.

  **orientation** ([``None``] | ``vertical`` | ``horizontal``)
    Set the orientation of the colorbar.

:returns:

  **cbar** (``<matplotlib>``)
    Colorbar handle.

.. note::

  Modify the label::

    cbar = coloraxis(...)
    cbar.set_label('test',labelpad=-160,y=0.45)

.. seealso::

  * `StackOverflow "matplotlib: colorbars and it's text labels"
    <http://stackoverflow.com/questions/15908371/
    matplotlib-colorbars-and-its-text-labels>`_

  * `StackOverflow "matplotlib colorbar background and label placement"
    <http://stackoverflow.com/questions/18403226/
    matplotlib-colorbar-background-and-label-placement>`_
  '''

  # add colorbar
  cbar = plt.colorbar(handle,shrink=0.95,aspect=10,orientation=orientation)
  cbar.solids.set_edgecolor("face")

  # get the location of the axis, and colorbar
  (la,ba,wa,ha) = plt.gca().get_position().bounds
  (lc,bc,wc,hc) = cbar.ax.get_position().bounds
  # change the position of the colorbar slightly
  cbar.ax.set_position([ lc-.05*wa , ba+0.1*ha , wc , ha*0.8 ])

  # automatically calculate ticklabels
  if ticklabel is None:
    ticklabel = [fmt%i for i in tick]

  # set tick
  if tick is not None:
    cbar.set_ticks(tick)
    cbar.set_ticklabels(ticklabel)

  # add label
  if label is not None:
    cbar.set_label(label)

  return cbar

# ==============================================================================
# annotate the plot
# ==============================================================================

def annotate(x,y,label,**kwargs):
  '''
Add label to a certain position.

:arguments:

  **x,y** (``<float>``)
    x- and y-position of the label.

  **label** (``<str>``)
    The label.

:options:

  **xycoords** ([``'data'``] | ``<str>``)
    Set the units for ``x,y``. See matplotlib documentation.

  **xytext/delta** (``(<float>,<float>)``)
    Position of the label. The units are set by ``textcoords``.

  **textcoords** ([``'data'``] | ``<str>``)
    Set the units for ``xytext``. See matplotlib documentation.

  **fontsize** (``<float>``)
    If not specified the font-size is automatically calculated from the font-
    size set in ``gooseeye.plot.figure``.

  **arrowprops** ([see below] | ``<dict>``)
    Arrow specification, see below.

  **bbox** ([``{}``] | ``<dict>``)
    Bounding box, see below.

:arrowprops:

  **arrowstyle** ([``'-'``] | ``<str>``)
    Line-end style. For example:

    * ``'-'``  line without end marker [default]
    * ``'->'`` line with end arrow

  **connectionstyle** ([``'arc3,rad=.2'``] | ``<str>``)
    Style of the arrow. For example:

    * ``'arc3,rad=.2'``: curved line [default]
    * ``'angle,angleA=0,angleB=90,rad=10'``: double straight line with sharp
      corner

  **color** ([``'k'``] | ``<str>``)
    The color of the arrow.

:bbox:

  **boxstyle** (``<str>``)
    Style or the box around the label. For example:

    * ``'round'`` square box with rounded edges

  **fc** (``<float>``)
    Face-color (index) of the box, e.g. ``fc = '0.8'``.

.. seealso::

  `matplotlib.annotate <http://matplotlib.org/users/annotations_intro.html>`_
  '''

  # set default arrow-properties
  arrowprops = {
    'arrowstyle'      : '-'           ,
    'connectionstyle' : 'arc3,rad=.2' ,
    'color'           : 'k'           ,
  }

  # set default options
  kwargs.setdefault( 'xycoords'   , 'data'                    )
  kwargs.setdefault( 'textcoords' , 'data'                    )
  kwargs.setdefault( 'fontsize'   , mpl.rcParams['font.size'] )
  kwargs.setdefault( 'arrowprops' , arrowprops                )
  kwargs.setdefault( 'bbox'       , {}                        )

  # overwrite alias
  if 'delta' in kwargs:
    if 'xytext' in kwargs:
      raise IOError('"delta" is an alias for "xytext", specify only one')
    kwargs['xytext'] = kwargs.pop('delta')

  # set default position
  if 'xytext' not in kwargs:
    kwargs['xytext'    ] = (50,20)
    kwargs['textcoords'] = 'offset points'

  # plot
  return plt.annotate(label,xy=(x,y),**kwargs)

# ==============================================================================
# convert mesh to patch, plot patch
# ==============================================================================

# ------------------------------------------------------------------------------
# convert a mesh to a patch object
# ------------------------------------------------------------------------------

def mesh2patch(coor,conn):
  '''
Convert a mesh to patch objects. The mesh is thereby given by

* ``coor``: matrix with nodal coordinates,
* ``conn``: matrix with connectivity.

The result can be included in a plot by::

  ax.add_patch(
    gplot.mesh2patch(coor,coon)
  )

whereby ``ax`` the axis in which to include the plot.

.. note::

  Use :py:meth:`gooseeye.plot.mesh2polygon` for color plots.

:arguments:

  **coor** (``<numpy.ndarray>``)
    Matrix with coordinates (positions) of the nodes.

  **conn** (``<numpy.ndarray>``)
    Matrix with the connectivity of the nodes (rows of ``coor``).

:returns:

  **patches** (``<list>``)
    List with patch objects.

:see also:

  :py:meth:`gooseeye.plot.mesh2polygon`
  '''

  # load extra modules
  from matplotlib.path import Path
  from matplotlib.patches import PathPatch

  # find the number of vertices; each patch consists of 5 vertices (with x- and
  # y-coordinate): lower-left, lower-right, upper-right,u
  nverts = np.shape(conn)[0]*(1+3+1)
  verts  = np.zeros((nverts,2),dtype=coor.dtype)
  # convert connectivity/coordinates to vertices
  for i in range(np.shape(conn)[1]):
    verts[i::5,:] = coor[conn[:,i],:]
  # set plot codes
  codes = np.ones(nverts,int)*Path.LINETO
  codes[0::5] = Path.MOVETO
  codes[4::5] = Path.CLOSEPOLY

  # convert to path
  mypath = Path(verts,codes)
  return PathPatch(mypath)

# ------------------------------------------------------------------------------
# convert a mesh to a polygon
# ------------------------------------------------------------------------------

def mesh2polygon(coor,conn):
  '''
Convert a mesh to patch objects. The mesh is thereby given by

* ``coor``: matrix with nodal coordinates,
* ``conn``: matrix with connectivity.

The result can be included in a plot by::

  gplot.patch(
    gplot.mesh2polygon(coor,conn),
    cindex
  )

:arguments:

  **coor** (``<numpy.ndarray>``)
    Matrix with coordinates (positions) of the nodes.

  **conn** (``<numpy.ndarray>``)
    Matrix with the connectivity of the nodes (rows of ``coor``).

:returns:

  **patches** (``<list>``)
    List with patch objects.
  '''

  # load extra modules
  from matplotlib.patches import Polygon

  # convert each element to a polygon
  poly = []
  for iconn in conn:
    poly.append(Polygon(coor[iconn,:]))

  return poly

# ------------------------------------------------------------------------------
# plot patch collections
# ------------------------------------------------------------------------------

def patch(*args,**kwargs):
  '''
Add patches to plot. The color of the patches is indexed according to the
color-index.

:arguments:

  **patches** (``<list>``)
    List with patch objects (see: ``gplot.mesh2polygon``).

:options:

  **cindex** (``<numpy.ndarray>``)
    Array with color indices.

  **cmap** (``<str>`` | ``<matplotlib>``)
    Define the colormap. To use the default colormaps, input the name.

  **linewidth** ([``1.0``] | ``<float>``)
    Set the width of the edges.

  **edgecolor** ([``'k'``] | ``<str>`` | ``<list>``)
    Set the color of the edges.

  **clim** ([``False``] | ``(<float>,<float>)``)
    Set the lower and upper limit of the color-axis.

  **axis** ([``False``] | ``<matplotlib>``)
    Specify an axis to include to plot in. By default the current axis is used.

:returns:

  **handle** (``<matplotlib>``)
    Handle of the patch objects.
  '''

  from matplotlib.collections import PatchCollection
  import matplotlib.pylab as plt
  import matplotlib

  # check number of input arguments
  if len(args)!=1:
    raise IOError('Only one non-named argument allowed')

  # set defaults for local options
  axis   = kwargs.pop( 'axis'   , plt.gca() )
  cindex = kwargs.pop( 'cindex' , None      )

  # optional: convert colormap to matplotlib.cm object
  if 'cmap' in kwargs:
    if type(kwargs['cmap'])==str:
      kwargs['cmap'] = eval('matplotlib.cm.'+kwargs['cmap'])

  # convert patches to matplotlib objects
  p = PatchCollection(*args,**kwargs)
  # optional: add colors to patches
  if cindex is not None:
    p.set_array(plt.array(cindex))
  # add patches to axis
  axis.add_collection(p)

  # return matplotlib patches
  return p

# ==============================================================================
# get the filename of the executing script
# ==============================================================================

def filename():
  '''
Get the file-name of the executing python script.

For example::

  python test.py

results in "test".

:returns:

  **filename** (``<str>``)
    The name of the executing script.
  '''

  import os,sys

  return os.path.splitext(os.path.basename(sys.argv[0]))[0]

# ==============================================================================
# save figure
# ==============================================================================

def savefig(*args,**kwargs):
  '''
Save the figure with custom options. This command runs the following command
with certain defaults specified::

  import matplotlib.pylab as plt
  plt.savefig(fname,transparent=True,bbox_inches='tight',pad_inches=0)

If the file-name "fname" is only an extension, the file-name of the executing
script is used as name. E.g.::

  gplot.savefig('.svg')

:options:

  **background** ([``False``] | ``True``)
    Remove the background from the SVG image, by removing::

      <g id="patch_1"> ... </g>

.. seealso::

  `matplotlib.pyplot.savefig
  <http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.savefig>`_
  '''

  import re,os

  # if the filename only consists of an extension: pre-apply filename
  if len(filter(None,os.path.splitext(args[0])))==1:
    args    = list(args)
    args[0] = filename()+args[0]
    args    = tuple(args)
  elif len(args[0].split('%s'))>1:
    args    = list(args)
    args[0] = args[0]%filename()
    args    = tuple(args)

  # set default: remove white space
  if 'pad_inches' not in kwargs:
    kwargs['pad_inches'] = 0
  # set default: make BBOX as thight as possible
  if 'bbox_inches' not in kwargs:
    kwargs['bbox_inches'] = 'tight'
  # set default: background transparent
  if 'transparent' not in kwargs:
    kwargs['transparent'] = True

  # check if the image is an SVG-image
  svg = False
  if 'format' in kwargs:
    if kwargs['format']=='svg':
      svg = True
  else:
    if re.match('.*svg',args[0]):
      svg = True
  # add svg-format to options
  if 'format' not in kwargs and svg:
    kwargs['format'] = 'svg'

  # option to remove background
  if 'background' in kwargs:
    # copy option from input
    background = kwargs['background']
    # remove from input
    del kwargs['background']
    # check file-type
    if background==False and svg==False:
      raise IOError('Background removal only possible for SVG image')
  else:
    # set default
    background = False

  # execute regular command with custom options
  plt.savefig(*args,**kwargs)

  #TODO: convert to regular expression, however this seems to be a bitch
  # function to remove the background of the SVG image
  def removebackground(fname):
    # loop until all '<g id="patch_*">*</g>' have been removed
    for i in [1]:
      text = open(fname,'r').read()
      (h,e) = text.split('<g id="patch_%d">'%i)
      e     = e.split('</g>',1)[1]
      text  = '\n'.join([h,e]).replace('\n\n','\n')
      open(fname,'w').write(text)


  # remove background from SVG image
  if background==False and svg==True:
    removebackground(args[0])

# ==============================================================================
# rendering switch
# ==============================================================================

def render(*args,**kwargs):
  '''
Render and/or save figure.

:options:

  **name** ([``None``] | ``<str>``)
    Filename to save to.

  **render** ([``True``] | ``False``)
    Switch to show/hide the figure.

  **save** ([``False``] | ``True``)
    Switch to save figure.
  '''

  # check input
  if   len(args)==0:
    name = kwargs.pop('name',None)
    if name is not None:
      args = (name,)+args
  elif len(args)==1:
    name = args[0]
    if 'name' in kwargs:
      raise IOError('Duplicate option: [name]')
  else:
    raise IOError('Only one argument allowed: [name]')


  # set defaults
  render = kwargs.pop('render', True                               )
  save   = kwargs.pop('save'  , True if name is not None else False)

  # optionally save
  if save:
    savefig(*args,**kwargs)

  # optionally show
  if render:
    plt.show()

  # close
  plt.close()

# ==============================================================================
# show figure
# ==============================================================================

def show(*args,**kwargs):
  '''
Show figure to the screen.
  '''

  kwargs.setdefault('render',True )
  kwargs.setdefault('save'  ,False)
  render(*args,**kwargs)

# ==============================================================================
# read an image (TIFF)
# ==============================================================================

def imread(fname,crop=True,dtype=None,info=False,mode='auto'):
  '''
Read an image from file.

:arguments:

  **filename** (``<str>``)
    The file-name, including the path to it.

:options:

  **crop** ([``True``] | ``False``)
    Crop the output image (matrix) to contain the image only, additional
    information is removed, by applying the EXIF-data.

  **dtype** ([``None``] | ...)
    Change the data-type of the output image. The output has the following range:

    * float: 0.0 <= I <= 1.0
    * uint8: 0   <= I <= 255

  **info** ([``False``] | ``<list>``)
    Read additional EXIF-data from the image. Common names::

      PixelWidth
      PixelHeight

    NB: the output is a dictionary of string. It has to be first convert for
    further usage.

  **mode** ([``'auto'``] | ``'BW'``)
    Optionally reduce the image to black and white.

:returns:

  **img** (``<numpy.ndarray>``)
    The image.

  **info** (``<dict>``) *optional*
    The EXIF-information, as specified in the input.
  '''

  import re

  # read the EXIF-data
  # ------------------

  # support function to automatically find the tag
  def read(tags,name):

    for tag in [34682]+[i for i in tags]:
      try:
        if len(tags[tag].split(name))>1:
          return tags[tag].split(name)[1].split('\n')[0].replace('=','')
      except:
        pass

    raise IOError('Tag "%s" not found'%name)

  # read the EXIF-data
  if crop or info:

    from PIL import Image

    tags = Image.open(fname).tag

  # extract the data
  if crop:
    px = int(read(tags,'ResolutionX'))
    py = int(read(tags,'ResolutionY'))

  # read more data
  if info:
    out = {}
    for i in info:
      out[i] = read(tags,i)

  # read the image (and crop)
  # -------------------------

  # read the image
  im = plt.imread(fname)

  # convert to black and white
  if mode.lower()=='bw' and len(im.shape)==3:
    im = im[:,:,0]

  # check dimensions
  if len(im.shape) not in [2,3]:
    raise IOError('Unknown input')

  # optional crop the image, using the info from above
  if crop:
    im = im[:py,:px]

  # optional convert the data type of the image
  if dtype is not None:
    if im.dtype!=dtype:
      norm = float(np.iinfo(im.dtype).max)
      im   = im.astype(np.float)/norm
      if dtype not in ['float']:
        im = (im*float(np.iinfo(dtype).max)).astype(dtype)

  # return output
  if not info:
    return im
  else:
    return (im,out)

# ==============================================================================
# probability density functions
# ==============================================================================

# ------------------------------------------------------------------------------
# probability density
# ------------------------------------------------------------------------------

def pdf(hist,bin_edges=None,bar_plot=False,**kwargs):
  r'''
Plot the probability density plot.

:arguments:

  **hist** (``<numpy.ndarray>``)
    The data for which to plot the probability density.

  **density** ([``True``] | ``False``)
    If set to ``True`` the area under the curve is normalized to one.

  **area** (``<float>``) *optional*
    Normalize to a specific area, different than one.

  **bins** ([``10``] | ``<int>``)
    The number of bins.

  **range** (``<list>``) *optional*
    The lower and upper range of the bins. If not provided, range is simply
    ``(hist.min(),hist.max())``. Values outside the range are ignored.

  **weights** (``<numpy.ndarray>``) *optional*
    An array of weights, of the same shape as ``hist``. Each value in a only
    contributes its associated weight towards the bin count (instead of 1).

:options:

  **bin_edges** ([``None``] | ``<numpy.ndarray>``)
    Specify the bin-edges. If this is specified the histogram is not calculated,
    this function is then only used from plotting.

  **bar_plot** ([``False``] | ``True``)
    If set to ``True`` a normal plot is made, optionally with the area under the
    curve as face (see "plot options"). If set to ``False``, a bar plot is made
    (see "barstyle").

  **return_data** ([``False``] | ``True``)
    If set to ``True`` the output is a dictionary contain the plot data.

  **plot** ([``True``] | ``False``)
    If set to ``False`` only the data is outputted.

:plot options:

  **linestyle** ([``'-'``] | ``'--'`` | ``'-.'`` | ``None``)
    Set the linestyle. If set to ``None`` not line is plotted.

  **alpha** (``<float>``)
    The alpha of the area under the curve. If not specified the area under the
    area under the curve is not plotted.

:barstyle:

  **facecolor** (``<str>``)
    Face color of the patch-objects.

  **edgecolor** (``<str>``)
    Edge color of the patch-objects.

  **linestyle** ([``'solid'``] | ``'dashed'`` | ``'dashdot'`` | ``'dotted'``)
    Set the linestyle.

  **linewidth** ([``1.0``] | ``<float>``)
    Set the linewidth.

:returns:

  **handle** (``<matplotlib>``), *only if ``plot==True``*
    Handle of the plot (``alpha==None``) or the area under the curve
    (``linestyle==None``). If both are not ``None`` the output is::

      (handle_fill,handle_plot)

  **x,y** (``<numpy.ndarray>``), *optional*
    The plot data.


.. seealso::

  `matplotlib.patches.Rectangle
  <http://matplotlib.org/api/patches_api.html#matplotlib.patches.Rectangle>`_.

  '''

  # define histogram
  # ----------------

  # read/default options
  area        = kwargs.pop('area'       ,1.0  )
  return_data = kwargs.pop('return_data',False)
  kwargs.setdefault('plot',True)

  # histogram options: for input or defaults
  histogram            = {}
  histogram['density'] = kwargs.pop('density',True)
  for key in ['bins','range','weights']:
    if key in kwargs:
      histogram[key] = kwargs.pop(key)

  # calculate histogram, normalize the area
  if bin_edges is None:
    hist,bin_edges = np.histogram(hist,**histogram)
    hist          *= area

  # plot as curve
  # -------------

  if not bar_plot:

    # initiate both plot types as False
    line_plot = False
    fill_plot = False
    # initiate options that should be remove
    line_rm = ['alpha']
    fill_rm = ['plot' ]

    # set default linestyle
    kwargs.setdefault('linestyle','-')

    # check to plot line: set options
    if kwargs['linestyle'] in ['-','--','-.',':']:
      line_plot = True
      line_args = {key:val for key,val in kwargs.iteritems() if key not in line_rm}
      fill_rm  += ['linestyle','linewidth','label','dashes']

    # check to plot fill: set options
    if 'alpha' in kwargs:
      fill_plot = True
      fill_args = {key:val for key,val in kwargs.iteritems() if key not in fill_rm}

    # convert the x- and y-axis
    x = np.cumsum(np.diff(bin_edges))+bin_edges[0]-np.diff(bin_edges)[0]/2.
    y = hist

    # plot background
    if fill_plot and kwargs['plot']:

      # extent the axis to correct the fill
      x = np.array(x,copy=True)
      y = np.array(y,copy=True)
      x = np.hstack(( x[0]*np.ones ((1)) , x , x[-1]*np.ones ((1)) ))
      y = np.hstack((      np.zeros((1)) , y ,       np.zeros((1)) ))

      # plot
      hf = plt.fill(x,y,**fill_args)

    # plot line
    if line_plot and kwargs['plot']:
      if 'plot' in line_args:
        del line_args['plot']
      hp = plot(x,y,**line_args)

    # set handles
    if kwargs['plot']:
      if line_plot and fill_plot:
        handle = (hp,hf)
      elif line_plot:
        handle = hp
      elif fill_plot:
        handle = hf
      else:
        raise IOError('Insufficient settings to plot, specify "linestyle" and/or "alpha" ')

    # return handles, etc.
    if not return_data:
      return handle
    elif kwargs['plot']:
      return dict(
        handle = handle,
        x      = x,
        y      = y,
      )
    else:
      return (x,y)


  # plot as bars
  # ------------

  import matplotlib

  kwargs.pop('plot',False)

  for i,(x0,dx,dy) in enumerate(zip(bin_edges[:-1],bin_edges[1:]-bin_edges[:-1],hist)):
    plt.gca().add_patch(matplotlib.patches.Rectangle((x0,0),dx,dy,**kwargs))

  return None

# ------------------------------------------------------------------------------
# cumulative probability density
# ------------------------------------------------------------------------------

def cdf(hist,mode='continuous',**kwargs):
  '''
Plot the cumulative probability density plot.

:arguments:

  **hist** (``<numpy.ndarray>``)
    The data for which to plot the cumulative probability density.

:options:

  **mode** ([``'continuous'``] | ``'line'`` | ``'bar'``)
    Different plot modes. If set to ``continuous`` the actual data is used. In
    the other cases the data is binned.

:linestyle:

  See ``gplot.plot``. Some examples:

  **linestyle** ([``'-'``] | ``'--'`` | ``'-.'`` | ``None``)
    Set the linestyle

  **linewidth** ([``1.0``] | ``<float>``)
    Set the linewidth.

  **marker** (``<str>``)
    Specify a marker.

:barstyle:

  **facecolor** (``<str>``)
    Face color of the patch-objects.

  **edgecolor** (``<str>``)
    Edge color of the patch-objects.

  **linestyle** ([``'solid'``] | ``'dashed'`` | ``'dashdot'`` | ``'dotted'``)
    Set the linestyle.

  **linewidth** ([``1.0``] | ``<float>``)
    Set the linewidth.

:histogram:

  **density** ([``True``] | ``False``)
    If set to ``True`` the area under the curve is normalized to one.

  **bins** ([``10``] | ``<int>``)
    The number of bins.

  **range** (``<list>``) *optional*
    The lower and upper range of the bins. If not provided, range is simply
    ``(data.min(),data.max())``. Values outside the range are ignored.

  **weights** (``<numpy.ndarray>``) *optional*
    An array of weights, of the same shape as ``data``. Each value in a only
    contributes its associated weight towards the bin count (instead of 1).

.. seealso::

  `matplotlib.patches.Rectangle
  <http://matplotlib.org/api/patches_api.html#matplotlib.patches.Rectangle>`_.
  '''

  # plot continuous
  # ---------------

  if mode in ['continuous','c']:

    return plot(np.sort(hist),np.linspace(0.0,1.0,len(hist)),**kwargs)

  # plot in bins
  # ------------

  histogram            = {}
  histogram['density'] = kwargs.pop('density',True)
  for key in ['bins','range','weights']:
    if key in kwargs:
      histogram[key] = kwargs.pop(key)

  # calculate histogram
  hist,bin_edges = np.histogram(hist,**histogram)
  hist[2:]       = np.cumsum(hist[2:])
  hist          /= hist[-1]
  bin_edges     += np.diff(bin_edges)[0]/2.

  # plot as curve
  if mode in ['line','lines','l']:
    return plot(np.cumsum(np.diff(bin_edges))+bin_edges[0]-np.diff(bin_edges)[0]/2.,hist,**kwargs)


  # plot as bars
  if mode in ['bars','bar','b']:

    import matplotlib

    for i,(x0,dx,dy) in enumerate(zip(bin_edges[:-1],bin_edges[1:]-bin_edges[:-1],hist)):
      plt.gca().add_patch(matplotlib.patches.Rectangle((x0,0),dx,dy,**kwargs))

    return None

# ------------------------------------------------------------------------------
# threshold the cumulative probability density
# ------------------------------------------------------------------------------

def cdf_threshold(data,probability):
  '''
Threshold the cumulative probability density. This function returns the value
for which a ratio of ``probability`` entries in ``data`` have a lower value.

:arguments:

  **data** (``<numpy.ndarray>``)
    The data for which to threshold the cumulative probability.

  **probability**
    The threshold probability.

:returns:

  **value** (``<int> | <float>``)
    The value for with a ratio of ``probability`` have a lower value that
    ``value``.
  '''

  upper = lambda index,data: index if index<=len(data)-1 else len(data)-1
  index = upper(int(np.floor(probability*float(len(data)-1))),data)

  return np.sort(data)[index]

# ==============================================================================
# test functions
# ==============================================================================

if __name__=='__main__':
  pass