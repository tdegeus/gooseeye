'''
Miscellaneous support functions

:copyright:

  `T.W.J. de Geus <http://www.geus.me>`_
  (`t.degeus@gmail.com <mailto:t.degeus@gmail.com>`_)
'''

import numpy as np

# ==============================================================================
# progress bar
# ==============================================================================

def progressbar(i,n,length=40,text='Progress'):
  r'''
Print progress-bar to screen.

:argument:

  **i** (``<int>``)
    Iteration counter.

  **n** (``<int>``)
    Number of iterations.

:options:

  **length** ([``40``] | ``<int>``)
    Length of the status-bar.

  **text** ([``'Progress'``] | ``<str>``)
    The text printed in front of the status bar.

:example:

  .. code-block:: python

    for i in range(10):
      gooseeye.misc.progressbar(i,10)

.. seealso::

  `StackOverflow statusbar
  <http://stackoverflow.com/questions/3160699/python-progress-bar>`_
  '''

  import sys

  # calculate progress percentage
  progress = float(i+1)/float(n)

  # set status string
  status = ""
  if i<0:
    status = "Halt...\r\n"
  if (i+1)>=n:
    status = "Done...\r\n"

  # set output text
  block = int(round(length*progress))
  text = "\r{3}: [{0}] {1:5.1f}% {2}".format( "#"*block+"-"*(length-block), progress*100.0, status, text)

  # write output
  sys.stdout.write(text)
  sys.stdout.flush()