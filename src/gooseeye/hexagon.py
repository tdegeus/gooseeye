'''

:copyright:

  `T.W.J. de Geus <http://www.geus.me>`_
  (`t.degeus@gmail.com <mailto:t.degeus@gmail.com>`_)
'''


import numpy as np


# ==============================================================================
# show grid of hexagons
# ==============================================================================

def imshow(im,**kwargs):
  '''
Plot a matrix as hexagons.

.. seealso::

  :py:meth:`gooseeye.plot.patch`
  '''

  # create "nodes"
  # --------------

  # generate x- and y-positions and the spacing
  xpos  = np.linspace(0.0,1.0,im.shape[0]/2*6+1)
  delta = xpos[2]-xpos[0]
  ymax  = delta*np.sin(np.pi/3.)*float(2*im.shape[1]+1)
  ypos  = np.linspace(0.0,ymax,im.shape[1]*4+3)
  # convert to grid
  (xpos,ypos) = np.meshgrid(xpos,ypos)
  # shift x-positions, to form hexagons
  xpos[ ::4,:] += delta/2.
  # convert to positions
  coor  = np.hstack((np.reshape(xpos,(-1,1)),np.reshape(ypos,(-1,1))))
  nnode = coor.shape[0]

  # create "elements"
  # -----------------

  # grid with node numbers
  node = np.reshape(np.arange(nnode),xpos.shape)

  # initiate the connectivity
  conn = np.zeros((im.size*3,4),dtype='int32')
  # fill connectivity:
  # odd, element 0
  conn[0::6,0] = np.reshape(node[ 0:-5:4 , 0:-6:6 ],(-1))
  conn[0::6,1] = np.reshape(node[ 0:-5:4 , 2:-4:6 ],(-1))
  conn[0::6,2] = np.reshape(node[ 2:-3:4 , 4:-2:6 ],(-1))
  conn[0::6,3] = np.reshape(node[ 2:-3:4 , 2:-4:6 ],(-1))
  # odd, element 1
  conn[1::6,0] = np.reshape(node[ 2:-3:4 , 2:-4:6 ],(-1))
  conn[1::6,1] = np.reshape(node[ 2:-3:4 , 4:-2:6 ],(-1))
  conn[1::6,2] = np.reshape(node[ 4:-1:4 , 2:-4:6 ],(-1))
  conn[1::6,3] = np.reshape(node[ 4:-1:4 , 0:-6:6 ],(-1))
  # odd, element 2
  conn[2::6,0] = np.reshape(node[ 0:-5:4 , 0:-6:6 ],(-1))
  conn[2::6,1] = np.reshape(node[ 2:-3:4 , 2:-4:6 ],(-1))
  conn[2::6,2] = np.reshape(node[ 4:-1:4 , 0:-6:6 ],(-1))
  conn[2::6,3] = np.reshape(node[ 2:-3:4 , 0:-6:6 ],(-1))
  # even, element 1
  conn[3::6,0] = np.reshape(node[ 2:-3:4 , 4:-2:6 ],(-1))
  conn[3::6,1] = np.reshape(node[ 2:-3:4 , 6:  :6 ],(-1))
  conn[3::6,2] = np.reshape(node[ 4:-1:4 , 6:  :6 ],(-1))
  conn[3::6,3] = np.reshape(node[ 4:-1:4 , 4:-2:6 ],(-1))
  # even, element 2
  conn[4::6,0] = np.reshape(node[ 4:-2:4 , 4:-2:6 ],(-1))
  conn[4::6,1] = np.reshape(node[ 4:-2:4 , 6:  :6 ],(-1))
  conn[4::6,2] = np.reshape(node[ 6:  :4 , 6:  :6 ],(-1))
  conn[4::6,3] = np.reshape(node[ 6:  :4 , 4:-2:6 ],(-1))
  # even, element 3
  conn[5::6,0] = np.reshape(node[ 2:-3:4 , 4:-2:6 ],(-1))
  conn[5::6,1] = np.reshape(node[ 4:-1:4 , 4:-2:6 ],(-1))
  conn[5::6,2] = np.reshape(node[ 6:  :4 , 4:-2:6 ],(-1))
  conn[5::6,3] = np.reshape(node[ 4:-1:4 , 2:-4:6 ],(-1))

  # set the connectivity of the hexagons
  connhex = np.zeros((im.size,6),dtype='int32')
  connhex[:,0] = conn[0::3,0]
  connhex[:,1] = conn[0::3,1]
  connhex[:,2] = conn[0::3,2]
  connhex[:,3] = conn[1::3,2]
  connhex[:,4] = conn[1::3,3]
  connhex[:,5] = conn[2::3,3]

  # plot
  # ----

  import gooseeye.plot as gplot

  return gplot.patch(
    gplot.mesh2polygon(coor,connhex),
    cindex = np.flipud(im).reshape(-1),
    **kwargs
  )