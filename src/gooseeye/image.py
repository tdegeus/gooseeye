'''

:copyright:

  `T.W.J. de Geus <http://www.geus.me>`_
  (`t.degeus@gmail.com <mailto:t.degeus@gmail.com>`_)
'''


import numpy as np
import core

# ==============================================================================
# create an artificial images of circles
# ==============================================================================

def dummy_circles(shape,N=(3,3),random=False,periodic=True,radius=None,shapes=None,return_shapes=False):
  r'''
Create a (periodic) 2D-image of circles. The circles are equi-sized and are
aligned on a regular grid. Optionally both the position and the radius can be
perturbed.

:arguments:

  **shape** (``(<int>,<int>)``)
    Number of pixels in both directions.

:options:

  **N** ([``(3,3)``] | ``(<int>,<int>)``)
    The number of circles in both directions.

  **random** ([``False``] | ``True``)
    If set to ``True`` the positions and dimensions of the different islands
    are randomized.

  **periodic** ([``True``] | ``False``)
    Signal if the created image is periodic or not.

  **radius** ([``None``] | ``<float>``)
    Radius of the circles. If not specified::

      radius = 1/(4*max(N))

  **shapes** ([``None``] | ``<numpy.ndarray[*,3]>``)
    Specify the shapes manually::

      shapes = np.array([
        [ x1 , y1 , r1 ],
        [ x2 , y2 , r2 ],
        ...
        [ xn , yn , rn ],
      ])

    If this option is used the ``N``, ``radius``, and ``random`` options are
    ignored. For example::

      shapes = np.array([
        [ 0.0 , 0.0 , 0.095 ],
        [ 0.5 , 0.0 , 0.095 ],
        [ 0.0 , 0.5 , 0.095 ],
        [ 0.5 , 0.5 , 0.095 ],
      ],dtype='float')

  **return_shapes** ([``False``] | ``True``)
    Return the shapes used to generate the image.

:returns:

  **image** (``<numpy.ndarray.bool>``)
    The generated 2D-image.

  **shapes** (``<numpy.ndarray>``), *optional*
    The shape definitions used to generate this image.
  '''

  # create
  # ------

  if shapes is None:

    # total number of circles, initiate positions/size
    n      = np.prod(N)
    shapes = np.zeros((n,3),dtype='float')

    # set positions
    x      = np.linspace(0.0,1.0,N[0])
    y      = np.linspace(0.0,1.0,N[1])
    (x,y)  = np.meshgrid(x,y)
    x      = x.reshape(-1)
    y      = y.reshape(-1)

    # randomize positions
    if random:
      x += np.random.normal(0.0,0.5/float(N[0]),n)
      y += np.random.normal(0.0,0.5/float(N[1]),n)

    # store
    shapes[:,0] = x
    shapes[:,1] = y

    # set radius
    if radius is None:
      shapes[:,2] = 1.0/(4.*float(max(N)))
    else:
      shapes[:,2] = radius

    # randomize radii
    if random:
      shapes[:,2] += (np.random.random(n)-.5)*max(shapes[:,2])

  # generate
  # --------

  if not return_shapes:
    return  core.image_dummy_circles(np.zeros(shape,dtype='bool'),shapes,periodic)
  else:
    return (core.image_dummy_circles(np.zeros(shape,dtype='bool'),shapes,periodic),shapes)

# ==============================================================================
# path between two voxels
# ==============================================================================

def path(i0,i1,algorithm='bresenham'):
  r'''
Calculate the path between two points/pixels/voxels.

:arguments:

  **i0,i1** (``<numpy.ndarray>``)
    The indices of the beginning and end points.

  **algorithm** (``"bresenham"`` | ``"actual"`` | ``"full"``)
    The algorithm to use. The basic properties are:

    * ``"bresenham"``: constant number of voxels per path
    * ``"actual"``: all voxels that fully contain the line
    * ``"full"``: a path that only crosses voxel faces (never points)

:returns:

  **path** (``<numpy.ndarray>``)
    The indices of the pixel/voxel path.
  '''

  return core.image_path(i0,i1,algorithm)

# ==============================================================================
# dilate an image
# ==============================================================================

def dilate(image,iterations,dtype='bool'):
  r'''
Dilate the image.

:arguments:

  **image** (``<numpy.ndarray>``)
    The image.

:options:

  **dtype** ([``'bool'``] | ``<str>``)
    The type of dilation to apply.

  **iterations** ([``25``] | ``<int>``)
    Number of iterations.
  '''

  import scipy.ndimage

  if dtype=='bool':
    return scipy.ndimage.binary_dilation(
      image.astype(np.bool),
      scipy.ndimage.generate_binary_structure(2,1),
      iterations=iterations
    )

# ==============================================================================
# dilate clusters (based on size)
# ==============================================================================

def clusters_dilate(labels,num_features,iterations=1,kernel=None,periodic=False):
  r'''
Dilate clusters, with a different number of iterations per cluster.

.. note::

  The dilation stops when another cluster is encountered. **The clusters are not
  merged**.

:arguments:

  **labels** (``<numpy.ndarray.int32>``)
    Clusters, labeled with non-zero index.

  **num_features** (``<int>``)
    The number of clusters in the image.

  **iterations** (``<numpy.ndarray.int32[num_features+1]> | <int>``)
    The number of iterations for each cluster-index. ``iterations[0]`` is
    ignored, as the background is not dilated.

  **kernel** ([``None``] | ``<numpy.ndarray.bool>``)
    The kernel to dilate with. By default a cross is used::

      kernel = np.array([
        [ False , True  , False ],
        [ True  , True  , True  ],
        [ False , True  , False ],
      ],dtype='bool')

  **periodic** ([``False``] | ``True``)
    Signal periodicity. If the image is periodic, the dilation is continued
    across the periodic edges of the image.

:returns:

  **labels** (``<numpy.ndarray.int32>``)
    Dilated clusters.
  '''

  # set default kernel
  if kernel is None:
    kernel = np.array([
      [0,1,0],
      [1,1,1],
      [0,1,0],
    ],dtype='bool')

  # calculate the number of feature
  if type(iterations)==int:
    iterations = iterations*np.ones((num_features+1),dtype='int32')

  # dilate the cluster, using input/default settings
  return core.image_clusters_dilate(labels,num_features,iterations,kernel,periodic)

# ==============================================================================
# label clusters (using SciPy for the moment)
# ==============================================================================

def clusters(image,periodic=False,return_centers=False,min_size=False,structure=None):
  r'''
Label clusters of pixels/voxels. The output is an integer image, of the same
shape as the input image, whereby each pixel cluster is assigned a unique non-
zero integer. A zero index is assigned to the background.

:arguments:

  **image** (``<numpy.ndarray>``)
    The input image.

:options:

  **return_centers** ([``False``] | ``True``)
    Signal to return an image where only the centers have a non-zero index.

  **periodic** ([``False``] | ``True``)
    This option is used to calculate the clusters, accounting for periodicity.
    In that case, clusters that start on one side and continue on the opposing
    side are given the same label.

  **min_size** ([``False``] | ``<int>``)
    Threshold the clusters to a minimum number of pixels per cluster.

  **structure** (``<numpy.ndarray>``)
    A structuring element that defines feature connections. It must be
    symmetric. If no structuring element is provided, one is automatically
    generated with a squared connectivity equal to one. That is, for a 2-D
    input array, the default structuring element is::

      [[0,1,0],
       [1,1,1],
       [0,1,0]]

:returns:

  **label** (``<numpy.ndarray>``)
    The clusters (with applied periodicity).

  **num_features** (``<int>``)
    The number of clusters in the image.

  **centers** (``<numpy.ndarray>``), *optional*
    The cluster labels at the centers of gravity.

  **size_features** (``<numpy.ndarray[num_features+1]>``), *optional*
    The size of each feature.

.. seealso::

  `scipy.ndimage.measurement.label
  <http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/
  scipy.ndimage.measurements.label.html>`_
  '''

  # initiate variables
  centers = None
  size    = None

  # initiate structure
  if structure is None:
    structure = np.array([
      [0,1,0],
      [1,1,1],
      [0,1,0],
    ],dtype='bool')

  # calculate the clusters
  label,N = core.image_clusters(image,structure=structure,periodic=periodic,min_size=min_size)

  # calculate centers of each cluster
  if return_centers:
    center,size = core.image_clusters_center(image,structure,label,N,periodic)

  # return output
  # -------------

  if not return_centers:
    return (label,N)
  else:
    return (label,N,center,size)


# ==============================================================================

def roimul(f,roi,periodic=False,norm=True,nonneg=False):
  r'''

Perform the following multiplication (also implemented in 1-D and 3-D)::

  RESULT[i,j] = F[i+di,j+dj]*ROI[i+di,j+dj]

:arguments:

  **f** (``<numpy.ndarray>``)
    Input image.

  **roi** (``<numpy.ndarray>``)
    The region-of-interest (kernel of the multiplication).

:options:

  **nonneg** ([``False``] | ``True``)
    Only take into account non-negative results.

  **periodic** ([``False``] | ``True``)
    Signal to consider the images periodic or not. If the images are not
    periodic a band of half the ROI-size is skipped along the edges. This can
    be avoided by using the ``pad`` option.

  **norm** ([``True``] | ``False``)
    Normalize the result. If set to ``False`` the un-normalized result and the
    normalization factor are outputted separately.

:returns:

  **P** (``<numpy.ndarray>``)
    The result. The result is normalized unless ``norm = False``.

  **norm** (``<int>``), *optional*
    The normalization factor, only outputted if ``norm = False``.

  '''

  if nonneg: result = core.image_roimul_nonneg(f,roi,periodic)
  else     : result = core.image_roimul       (f,roi,periodic)

  if norm: return result.astype(np.float)/float(roi.size)
  else   : return (result,roi.size)

# ==============================================================================
# 2-point probability, 2-point cluster, convolution, etc.
# ==============================================================================

def correlate(f,g,roi,periodic=False,norm=True,pad=False,fmask=None,gmask=None):
  r'''
Calculate the correlation of two images (may be the same image). Depending on
the data-type of the images:

* both boolean: 2-point probability;

* both floating-point: correlation function;

* both integer: 2-point cluster function, thereby each cluster is labeled using
  a unique integer index (see ``gooseeye.image.clusters``)

:arguments:

  **f,g** (``<numpy.ndarray>``)
    Input images (may be the same). The calculated measure depends on the data-
    type of this image.

  **roi** (``<list>``)
    The size of the region-of-interest for which the result is calculated.

:options:

  **periodic** ([``False``] | ``True``)
    Signal to consider the images periodic or not. If the images are not
    periodic a band of half the ROI-size is skipped along the edges. This can
    be avoided by using the ``pad`` option.

  **norm** ([``True``] | ``False``)
    Normalize the result. If set to ``False`` the un-normalized result and the
    normalization factor are outputted separately.

  **pad** ([``False``] | ``True``)
    Pad the input images with a region of half the ROI width. This way, all
    voxels along the edges can be taken into account without the need to assume
    periodicity.

  **fmask,gmask** ([``None``] | ``<numpy.ndarray.bool>``)
    Specify voxels to mask (``==True``). These voxels are not taken into account
    in the statistics, the normalization is modified for this.

:returns:

  **P** (``<numpy.ndarray[roi]>``)
    The result (2-point probability, correlation, 2-point cluster function,
    etc.). The result is normalized unless ``norm = False``.

  **norm** (``<int>`` | ``<numpy.ndarray>``), *optional*
    The normalization factor, only outputted if ``norm = False``. There are
    two possibilities:

    1.  If ``pad``, ``fmask``, or ``gmask`` are specified: the output is an
        array of the size of the region-of-interest. To normalize::

          P.astype(np.float)/norm.astype(np.float)

    2.  Otherwise ("normal" mode): the output is an integer. To normalize::

          P.astype(np.float)/float(norm)

.. seealso::

  | :py:meth:`gooseeye.image.clusters`
  | :py:meth:`gooseeye.image.clusters_dilate`
  '''

  # masked correlation
  # ------------------

  if pad or fmask is not None or gmask is not None:

    # initiate
    if fmask is None:
      fmask = np.zeros(f.shape,dtype='bool')
    if gmask is None:
      gmask = np.zeros(g.shape,dtype='bool')

    # apply pad
    if pad:
      half  = tuple((np.array(roi)-1)/2)
      f     = np.pad(f    ,(half,half),mode='constant',constant_values=0)
      g     = np.pad(g    ,(half,half),mode='constant',constant_values=0)
      fmask = np.pad(fmask,(half,half),mode='constant',constant_values=1)
      gmask = np.pad(gmask,(half,half),mode='constant',constant_values=1)

    # calculate correlation
    (roi,N) = core.image_correlate_mask(f,g,fmask,gmask,roi,periodic)

    # apply normalization
    if norm:
      return roi.astype(np.float)/N.astype(np.float)
    else:
      return (roi,N)


  # default correlation functions
  # -----------------------------

  else:

    # calculate correlation
    (roi,N) = core.image_correlate(f,g,roi,periodic)

    # apply normalization
    if norm:
      return roi.astype(np.float)/float(N)
    else:
      return (roi,N)

# ==============================================================================
# calculate the conditional average
# ==============================================================================

def correlate_weight(weights,image,roi,periodic=False,norm=True,pad=False,mask=None):
  r'''
Calculate the weighted average intensity of the input image. This corresponds to
the following convolution:

.. math::

  R(\delta \vec{x}) = \mathcal{W} \star \mathcal{I}

If the weight factor :math:`W` (``weights``) is boolean, this may be interpreted
as the conditional average of the input image.

:arguments:

  **weights,image** (``<numpy.ndarray>``)
    The weight factors (boolean or floating-point) and the image (boolean or
    floating-point).

  **roi** (``<list>``)
    The size of the region-of-interest for which the result is calculated.

:options:

  **periodic** ([``False``] | ``True``)
    Signal to consider the images periodic or not. If the images are not
    periodic a band of half the ROI-size is skipped along the edges. This can
    be avoided by using the ``pad`` option.

  **norm** ([``True``] | ``False``)
    Normalize the result. If set to ``False`` the un-normalized result and the
    normalization factor are outputted separately.

  **pad** ([``False``] | ``True``)
    Pad the input images with a region of half the ROI width. This way, all
    voxels along the edges can be taken into account without the need to assume
    periodicity.

  **mask** ([``None``] | ``<numpy.ndarray.bool>``)
    Specify voxels to mask (``==True``). This mask is **only applied to the
    image**. These voxels are not taken into account in the statistics, the
    normalization is modified for this.

:returns:

  **P** (``<numpy.ndarray[roi]>``)
    The result, normalized unless ``norm = False``.

  **norm** (``<float>`` | ``<numpy.ndarray>``), *optional*
    The normalization factor, only outputted if ``norm = False``. There are
    two possibilities:

    1.  If ``pad`` or ``mask`` are specified: the output is an
        array of the size of the region-of-interest. To normalize::

          P.astype(np.float)/norm.astype(np.float)

    2.  Otherwise ("normal" mode): the output is a float. To normalize::

          P.astype(np.float)/float(norm)
  '''

  # masked correlation
  # ------------------

  if pad or mask is not None:

    # initiate
    if mask is None:
      mask = np.zeros(image.shape,dtype='bool')

    # apply pad
    if pad:
      half    = tuple((np.array(roi)-1)/2)
      weights = np.pad(weights,(half,half),mode='constant',constant_values=0)
      image   = np.pad(image  ,(half,half),mode='constant',constant_values=0)
      mask    = np.pad(mask   ,(half,half),mode='constant',constant_values=1)

    # calculate correlation
    (roi,N) = core.image_correlate_weight_mask(weights,image,mask,roi,periodic)

    # apply normalization
    if norm:
      return roi.astype(np.float)/N.astype(np.float)
    else:
      return (roi,N)

  # default correlation
  # -------------------

  else:

    # calculate correlation
    (roi,N) = core.image_correlate_weight(weights,image,roi,periodic)

    # apply normalization
    if norm:
      return roi.astype(np.float)/float(N)
    else:
      return (roi,N)

# ==============================================================================
# lineal path function
# ==============================================================================

def correlate_weight_collapse(image,labels,centers,roi,periodic=True,norm=True,pad=False,mask=None,algorithm='bresenham',mode='all'):
  r'''
Calculate the average intensity around clusters, whereby the result is collapsed
to the center of gravity of the clusters.

:arguments:

  **image** (``<numpy.ndarray.float>``)
    Input image.

  **labels** (``<numpy.ndarray.int32>``)
    The cluster-labels, whereby each cluster is labeled with a unique non-zero
    index.

  **centers** (``<numpy.ndarray.int32>``)
    The centers of the clusters, with the label index of the cluster at the
    position of the center. The rest of the image is zero.

  **roi** (``<list>``)
    The size of the region-of-interest for which the result is calculated.

:options:

  **periodic** ([``False``] | ``True``)
    Signal to consider the images periodic or not. If the images are not
    periodic a band of half the ROI-size is skipped along the edges. This can
    be avoided by using the ``pad`` option.

  **norm** ([``True``] | ``False``)
    Normalize the result. If set to ``False`` the un-normalized result and the
    normalization factor are outputted separately.

  **pad** ([``False``] | ``True``)
    Pad the input images with a region of half the ROI width. This way, all
    voxels along the edges can be taken into account without the need to assume
    periodicity.

  **mask** ([``None``] | ``<numpy.ndarray.bool>``)
    Specify voxels to mask (``==True``). This mask is **only applied to the
    image**. These voxels are not taken into account in the statistics, the
    normalization is modified for this.

  **algorithm** (``"bresenham"`` | ``"actual"`` | ``"full"``)
    The algorithm to use to calculate the voxel paths:

    * ``"bresenham"``: constant number of voxels per path
    * ``"actual"``: all voxels that fully contain the line between the two
      generating voxels.
    * ``"full"``: a path that only crosses voxel faces (never single points).

  **mode** (``'perpendicular'`` | ``'cross'`` | ``all`` | ``'lineal'``)
    Mode to generate the end points:

    * ``'perpendicular'``: paths along the axes,
    * ``'cross'``: paths along the axes and the diagonal,
    * ``'all'``: paths to all possible end points,
    * ``'lineal'``: paths to half possible end points.

:returns:

  **P** (``<numpy.ndarray[roi]>``)
    The result, normalized unless ``norm = False``.

  **norm** (``<int>`` | ``<numpy.ndarray>``), *optional*
    The normalization factor. To normalize::

          P.astype(np.float)/float(norm)

  '''

  # calculate the voxel stamp
  stamp = core.image_linealpath_stamp(roi,algorithm,mode)

  # initiate
  if mask is None:
    mask = np.zeros(image.shape,dtype='bool')

  # apply pad
  if pad:
    half     = tuple((np.array(roi)-1)/2)
    image    = np.pad(image   ,(half,half),mode='constant',constant_values=0)
    labels   = np.pad(labels  ,(half,half),mode='constant',constant_values=0)
    centers  = np.pad(centers ,(half,half),mode='constant',constant_values=0)
    mask     = np.pad(mask    ,(half,half),mode='constant',constant_values=1)

  # calculate the lineal path function
  P,N = core.image_correlate_weight_collapse(image,labels,centers,mask,stamp,roi,periodic)

  # apply normalization
  if norm:
    return P.astype(np.float)/N.astype(np.float)
  else:
    return (P,N)

# ==============================================================================
# lineal path function
# ==============================================================================

def linealpath(image,roi,periodic=False,norm=True,algorithm='bresenham',mode='lineal'):
  r'''
Calculate the lineal path function (of a boolean image).

:arguments:

  **image** (``<numpy.ndarray.bool>``)
    Input image.

  **roi** (``<list>``)
    The size of the region-of-interest for which the result is calculated.

:options:

  **periodic** ([``False``] | ``True``)
    Signal to consider the images periodic or not. If the images are not
    periodic a band of half the ROI-size is skipped along the edges. This can
    be avoided by using the ``pad`` option.

  **norm** ([``True``] | ``False``)
    Normalize the result. If set to ``False`` the un-normalized result and the
    normalization factor are outputted separately.

  **algorithm** (``"bresenham"`` | ``"actual"`` | ``"full"``)
    The algorithm to use to calculate the voxel paths:

    * ``"bresenham"``: constant number of voxels per path
    * ``"actual"``: all voxels that fully contain the line between the two
      generating voxels.
    * ``"full"``: a path that only crosses voxel faces (never single points).

  **mode** (``'perpendicular'`` | ``'cross'`` | ``all`` | ``'lineal'``)
    Mode to generate the end points:

    * ``'perpendicular'``: paths along the axes,
    * ``'cross'``: paths along the axes and the diagonal,
    * ``'all'``: paths to all possible end points,
    * ``'lineal'``: paths to half possible end points: this will account for
      the symmetry in the lineal path function.

    NB: the symmetry of the lineal path function is applied to the result, no
    matter if the calculation was symmetric or not.

:returns:

  **P** (``<numpy.ndarray[roi]>``)
    The result, normalized unless ``norm = False``.

  **norm** (``<int>`` | ``<numpy.ndarray>``), *optional*
    The normalization factor. To normalize::

          P.astype(np.float)/float(norm)

  '''

  # check data-type
  if image.dtype!='bool':
    conv = np.array(image,copy=True).astype(np.bool).astype(image.dtype)
    if np.sum(np.abs(image-conv))!=0: raise IOError('Only defined for binary images')
    else                            : image = image.astype(np.bool)

  # calculate the voxel stamp
  stamp = core.image_linealpath_stamp(roi,algorithm,mode)

  # calculate the lineal path function
  L,norm = core.image_linealpath(image,stamp,roi,periodic)

  # apply normalization
  if norm:
    return L.astype(np.float)/float(norm)
  else:
    return (L,norm)

# ==============================================================================
# histogram, taking mask into account
# ==============================================================================

def histogram(image,mask=None,**kwargs):
  '''
Calculate the image's histogram. This function can ignore masked pixels.

:arguments:

  **image** (``<numpy.ndarray>``)
    The input image.

:options:

  **mask** ([``None``] | ``<numpy.ndarray.bool>``)
    Specify voxels to mask (``==True``). These voxels are not taken into account
    in the histogram.

  **density** ([``True``] | ``False``)
    If set to ``True`` the area under the curve is normalized to one.

  **bins** ([``256``] | ``<int>``)
    The number of bins.

  **range** (``<list>``) *optional*
    The lower and upper range of the bins. If not provided, range is simply
    ``(image.min(),image.max())``. Values outside the range are ignored.

:returns:

  **hist** (``<numpy.ndarray>``)
    The values of the histogram.

  **bin_edges** (``<numpy.ndarray.float[len(hist)+1]>``)
    The bin edges.
  '''

  kwargs.setdefault( 'density' , True )
  kwargs.setdefault( 'bins'    , 256  )

  image = image.reshape(-1)[np.where(mask.astype(np.bool).reshape(-1)==False)[0]]

  return np.histogram(image,**kwargs)

