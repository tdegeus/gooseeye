﻿'''
Module to interact with the GooseEYE-core functions.

:copyright:

  | T.W.J. de Geus
  | http://www.geus.me
  | http://www.geus.me/gooseeye
  | gooseeye@geus.me

'''

import numpy as np
import ctypes
import re,os,sys

# import shared library
try:
  if sys.platform=='win32': lib = ctypes.CDLL('libgooseeye.dll')
  else                    : lib = ctypes.CDLL('libgooseeye.so')
except:
  import warnings
  if sys.platform=='win32': warnings.warn('Failed to import "libgooseeye.dll"')
  else                    : warnings.warn('Failed to import "libgooseeye.so"' )

# ==============================================================================

def np_ptr(arg):
  r'''
Convert the argument to a C-pointer. For ``None`` input the function returns a
``NULL``-pointer

:argument:

  **arg** (``<numpy.ndarray>`` | ``None``)

:returns:

  **arg** (``<numpy.ctypeslib.as_cytpes>`` | ``None``)
  '''

  if arg is not None:
    return np.ctypeslib.as_ctypes( arg )
  else:
    return arg

# ==============================================================================

def asnumpy(arg,dtype=None):
  r'''
Convert to NumPy-array of a specific data-type.

:argument:

  **arg** (``<int>`` | ``<float>`` | ``<list>`` | ``<numpy.ndarray>``)

:option:

  **dtype** = ``<str>``

:returns:

  **arg**  (``<numpy.ndarray.dtype>``)
  '''

  # do not convert ``None`` items
  if arg is None:
    return None

  # already a NumPy-array: check/convert type and quit
  if type(arg)==np.ndarray:
    if dtype is None:
      return arg
    elif arg.dtype==dtype:
      return arg
    else:
      return arg.astype(dtype)

  # convert a single value to a list
  if type(arg)==int or type(arg)==float or type(arg)==bool:
    arg = [arg]
  # convert to list to NumPy-array of the correct type
  if dtype is None:
    return np.array(arg)
  else:
    return np.array(arg,dtype=dtype)

# ==============================================================================
# core.h
# ==============================================================================

def error(*args):
  '''
Acts on ``ctypes.restype``. If the error-code is non-zero an exception is
raised, whereby the message is read from the compiled library (using the
function ``get_error``).
  '''

  # define interface to the function that returns the error string
  cfun          = lib.get_error
  cfun.restype  = ctypes.c_char_p
  cfun.argtypes = []

  # check the error code, and return the error string
  if args[0]:
    raise RuntimeError(cfun())
  else:
    return args[0]

# ------------------------------------------------------------------------------

class Matrix(object):
  r'''
Store a matrix as 1-D array to be able to interface with the C-routines. To
recover the matrix in it's original shape::

  >>> Matrix.asnumpy()

:arguments:

  **matrix** (``<numpy.ndarray>``)
    The matrix.

.. note::

  This function includes methods to create a pointer to the
  :ref:`Matrix <src/core/Matrix>` structure from the GooseEYE core.
  '''

  # ............................................................................

  def __init__(self,matrix):

    # save matrix information: dimension, size, and shape
    self.nd    = len(matrix.shape)
    self.size  = matrix.size
    self.shape = np.array(matrix.shape,dtype='int32')

    # initiate data fields
    self.data_b = None
    self.data_i = None
    self.data_d = None

    # store matrix, based on data-type
    if re.match('float.*',str(matrix.dtype)):
      self.data_d = matrix.astype(np.float64).reshape(-1)
    elif re.match('bool.*',str(matrix.dtype)):
      self.data_b = matrix.astype(np.int32).reshape(-1)
    else:
      self.data_i = matrix.astype(np.int32).reshape(-1)

  # ............................................................................

  class typedef(ctypes.Structure):
    '''
Create typedef from C-structure.
    '''

    _fields_ = [
      ('data_b', ctypes.POINTER(ctypes.c_int   )) ,
      ('data_i', ctypes.POINTER(ctypes.c_int   )) ,
      ('data_d', ctypes.POINTER(ctypes.c_double)) ,
      ('shape' , ctypes.POINTER(ctypes.c_int   )) ,
      ('size'  , ctypes.c_int                   ) ,
      ('nd'    , ctypes.c_int                   ) ,
    ]

  # ............................................................................

  def ptr(self):
    '''
Return pointer to the structure
    '''

    return self.typedef(\
      np_ptr( self.data_b   ) ,
      np_ptr( self.data_i   ) ,
      np_ptr( self.data_d   ) ,
      np_ptr( self.shape    ) ,
      int   ( self.size     ) ,
      int   ( self.nd       ) ,
    )

  # ............................................................................

  def __getitem__(self,key):
    return Matrix(self.asnumpy().__getitem__(key))

  # ............................................................................

  def asnumpy(self):
    if   self.data_b is not None:
      return self.data_b.reshape(self.shape[-self.nd:]).astype(np.bool)
    elif self.data_i is not None:
      return self.data_i.reshape(self.shape[-self.nd:])
    elif self.data_d is not None:
      return self.data_d.reshape(self.shape[-self.nd:])
    else:
      raise RuntimeError('Insufficient data fields')

# ==============================================================================
# image_tools.h
# ==============================================================================

def image_path(i0,i1,algorithm):
  r'''
See: :ref:`src/core/image_path`

:arguments:

  **i0,i1** (``<numpy.ndarray>``)

  **algorithm** (``"bresenham"`` | ``"actual"`` | ``"full"``)

:returns:

  **path** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_path
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.c_int,
    ctypes.c_int,
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.POINTER(ctypes.c_int),
  )

  # convert input type, set the dimensions, initiate the length
  i0 = np.array(i0,dtype='int32')
  i1 = np.array(i1,dtype='int32')
  nd = len(i0)
  N  = np.empty((1),dtype='int32')

  # convert voxel path algorithm
  algorithm = {'bresenham':0,'actual':1,'full':2}[algorithm]

  # check
  if len(i1)!=nd:
    raise IOError('"i0" and "i1" must have the same shape')

  # calculate the length
  cfun(i0,i1,algorithm,nd,N,None)

  # initiate the voxel path
  path = np.empty((int(N)*nd),dtype='int32')

  # calculate the path
  cfun(i0,i1,algorithm,nd,N,np.ctypeslib.as_ctypes(path))

  return path.reshape(int(N),nd)

# ..............................................................................

def image_dummy_circles(self,circles,periodic):
  r'''
See: :ref:`src/core/image_dummy_circles`

:arguments:

  **self** (``<numpy.ndarray>``), *must be 2-D*

  **circles** (``<numpy.ndarray.int[*,3]``)

  **periodic** (``True`` | ``False``)

:returns:

  **self** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_dummy_circles
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to "Matrix"
  self    = Matrix(self)
  circles = Matrix(circles)

  # run function
  cfun(self.ptr(),circles.ptr(),periodic)

  # return in original shape
  return self.asnumpy()

# ..............................................................................

def image_clusters(image,structure,periodic=False,min_size=-1):
  r'''
See: :ref:`src/core/image_clusters`

:arguments:

  **image** (``<numpy.ndarray.bool>``)

  **structure** (``<numpy.ndarray.bool>``)

:options:

  **periodic** ([``False``] | ``True``)

  **min_size** ([``-1``] | ``>0``)

:returns:

  **label** (``<numpy.ndarray.int32>``)

  **num_features** (``<int>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_clusters
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.POINTER(Matrix.typedef),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
  )

  # initiate
  label        = Matrix(np.empty(image.shape,dtype='int32'))
  num_features = np.zeros((1)        ,dtype='int32')
  image        = Matrix(image)
  structure    = Matrix(structure)

  # call function
  cfun(image.ptr(),structure.ptr(),int(periodic),min_size,label.ptr(),num_features)

  return (label.asnumpy(),int(num_features[0]))

# ..............................................................................

def image_clusters_dilate(label,num_features,iterations,kernel,periodic):
  r'''
See: :ref:`src/core/image_clusters_dilate`

:arguments:

  **label** (``<numpy.ndarray.int32>``)

  **num_features** (``<int>``)

  **iterations** (``<numpy.ndarray.int32[num_features+1]>``)

  **kernel** (``<numpy.ndarray.bool>``)

  **periodic** (``True`` | ``False``)

:returns:

  **label** (``<numpy.ndarray.int32>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_clusters_dilate
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert input
  label       = Matrix(label)
  kernel       = Matrix(kernel)
  iterations   = asnumpy(iterations,'int32')

  # call function
  cfun(label.ptr(),num_features,iterations,kernel.ptr(),int(periodic))

  return label.asnumpy()

# ..............................................................................

def image_clusters_center(image,structure,label,num_features,periodic):
  r'''
See: :ref:`src/core/image_clusters_center`

:arguments:

  **image** (``<numpy.ndarray.int32>``)

  **structure** (``<numpy.ndarray.bool>``)

  **label** (``<numpy.ndarray.int32>``)

  **num_features** (``<int>``)

  **periodic** (``True`` | ``False``)

:returns:

  **centers** (``<numpy.ndarray.int32>``)

  **size_features** (``<numpy.ndarray.int32>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_clusters_center
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.POINTER(Matrix.typedef),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
  )

  # convert input arguments
  size_features = np.empty((num_features+1),dtype='int32')
  center        = Matrix(np.empty(image.shape,dtype='int32'))
  image         = Matrix(image)
  structure     = Matrix(structure)
  label         = Matrix(label)

  # run function
  cfun(image.ptr(),structure.ptr(),label.ptr(),num_features,periodic,center.ptr(),size_features)

  return (center.asnumpy(),size_features)

# ==============================================================================
# image_stat.h
# ==============================================================================

class Stamp(object):
  '''
Store a stamp with a number of different voxel-paths.

:arguments:

  **npath** (``<int>``)
    The number of points in the stamp.

  **nd** (``<int>``)
    The number of dimensions for which the stamp is defined.

:fields:

  **nd,path** (``<int>``)
    The number of dimensions and paths (see options).

  **ia** (``<numpy.ndarray.int32[npath+1]>``)
    An index to split ``ja`` per path.

  **ja** (``<numpy.ndarray.int32[N*nd]>``)
    The voxels in the paths stored as list. I.e.::

      [ (h,j,i) , (h,j,i) , ... ]

    NB: ``ia`` splits ``ja`` at the voxel number. To actually split ``ja`` the
    number of dimensions still has to be applied. I.e. to get indices of path
    ``idx``::

      ja[ ia[idx]*nd , : ]

  **stat** (``<numpy.ndarray.int32[N]>``)
    Per voxel signal if it has to be stored in the statistics. This in needed
    for the lineal path function, to avoid duplicate counts.

:usage:

  To get all voxels in the path ``idx``, use::

    stamp[idx]

  This returns the voxels in a matrix of dimensions ``(n,nd)``, with ``n`` the
  number of voxels in path ``idx``.
  '''

  def __init__(self,npath,nd):

    # initiate variables
    self.ia    = np.empty((npath+1),dtype='int32')
    self.ja    = None
    self.stat  = None
    self.npath = npath
    self.nd    = nd

    # create function to update the C-pointer
    self._c_struct = lambda: self._C_Struct(\
      ctypes_ptr( self.ia        ) ,
      ctypes_ptr( self.ja        ) ,
      ctypes_ptr( self.stat      ) ,
      int       ( self.npath     ) ,
      int       ( self.nd        ) ,
    )

  # ............................................................................

  class typedef(ctypes.Structure):
    '''
Create typedef from C-structure.
    '''

    _fields_ = [
      ('ia'        , ctypes.POINTER(ctypes.c_int)) ,
      ('ja'        , ctypes.POINTER(ctypes.c_int)) ,
      ('stat'      , ctypes.POINTER(ctypes.c_int)) ,
      ('npath'     , ctypes.c_int                ) ,
      ('nd'        , ctypes.c_int                ) ,
    ]

  # ............................................................................

  def ptr(self):
    '''
Return pointer to the structure
    '''

    return self.typedef(\
      np_ptr( self.ia        ) ,
      np_ptr( self.ja        ) ,
      np_ptr( self.stat      ) ,
      int   ( self.npath     ) ,
      int   ( self.nd        ) ,
    )

  # ............................................................................

  def __len__(self):
    return self.npath

  # ............................................................................

  def __getitem__(self,i):

    if i==self.npath:
      raise StopIteration
    return self.ja.reshape(-1,self.nd)[self.ia[i]:self.ia[i+1],:]

# ..............................................................................

def image_roimul(f,roi,periodic):
  r'''
See: :ref:`src/core/image_roimul`

:arguments:

  **f** (``<numpy.ndarray>``)

  **roi** (``<numpy.ndarray>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_roimul
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to Image-class
  N   = f.shape
  f   = Matrix(f  )
  roi = Matrix(roi)

  # initiate output
  if f.data_i is not None and roi.data_i is not None: result = Matrix(np.empty(N,dtype='int32'  ))
  else                                              : result = Matrix(np.empty(N,dtype='float64'))

  # run function, and return result
  cfun(f.ptr(),roi.ptr(),result.ptr(),periodic)

  return result.asnumpy()

# ..............................................................................

def image_roimul_nonneg(f,roi,periodic):
  r'''
See: :ref:`src/core/image_roimul_nonneg`

:arguments:

  **f** (``<numpy.ndarray>``)

  **roi** (``<numpy.ndarray>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_roimul_nonneg
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to Image-class
  N   = f.shape
  f   = Matrix(f  )
  roi = Matrix(roi)

  # initiate output
  if f.data_i is not None and roi.data_i is not None: result = Matrix(np.empty(N,dtype='int32'  ))
  else                                              : result = Matrix(np.empty(N,dtype='float64'))

  # run function, and return result
  cfun(f.ptr(),roi.ptr(),result.ptr(),periodic)

  return result.asnumpy()

# ..............................................................................

def image_correlate(f,g,roi,periodic):
  r'''
See: :ref:`src/core/image_correlate`

:arguments:

  **f,g** (``<numpy.ndarray>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<int>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_correlate
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.c_int,
  )

  # convert to Image-class
  f = Matrix(f)
  g = Matrix(g)

  # initiate normalization factor
  norm = np.empty((1),dtype='int32')
  # initiate output
  if f.data_i is not None and g.data_i is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  elif f.data_b is not None and g.data_b is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  else:
    roi = Matrix(np.empty(roi,dtype='float64'))

  # run function, and return result
  cfun(f.ptr(),g.ptr(),roi.ptr(),norm,periodic)

  return (roi.asnumpy(),int(norm))

# ..............................................................................

def image_correlate_mask(f,g,fmask,gmask,roi,periodic):
  r'''
See: :ref:`src/core/image_correlate_mask`

:arguments:

  **f,g** (``<numpy.ndarray>``)

  **fmask,gmask** (``<numpy.ndarray.bool>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_correlate_mask
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to Matrix-class
  f     = Matrix(f    )
  g     = Matrix(g    )
  fmask = Matrix(fmask)
  gmask = Matrix(gmask)

  # initiate normalization
  norm = Matrix(np.empty(roi,dtype='int32'))
  # initiate output
  if f.data_i is not None and f.data_i is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  elif f.data_b is not None and f.data_b is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  else:
    roi = Matrix(np.empty(roi,dtype='float64'))

  # run function
  cfun(f.ptr(),g.ptr(),fmask.ptr(),gmask.ptr(),roi.ptr(),norm.ptr(),periodic)

  # return result
  return (roi.asnumpy(),norm.asnumpy())

# ..............................................................................

def image_correlate_weight(weights,image,roi,periodic):
  r'''
See: :ref:`src/core/image_correlate_weight`

:arguments:

  **weights,image** (``<numpy.ndarray>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<int>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_correlate_weight
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    np.ctypeslib.ndpointer(ctypes.c_double,flags="C_CONTIGUOUS"),
    ctypes.c_int,
  )

  # convert to Image-class
  image   = Matrix(image)
  weights = Matrix(weights)

  # initiate normalization factor
  norm = np.array((1),dtype='float64')
  # initiate output
  if   image.data_i is not None and weights.data_i is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  elif image.data_b is not None and weights.data_b is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  else:
    roi = Matrix(np.empty(roi,dtype='float64'))

  # run function, and return result
  cfun(weights.ptr(),image.ptr(),roi.ptr(),norm,periodic)

  return (roi.asnumpy(),int(norm))

# ..............................................................................

def image_correlate_weight_mask(weights,image,mask,roi,periodic):
  r'''
See: :ref:`src/core/image_correlate_weight_mask`

:arguments:

  **image,weights** (``<numpy.ndarray>``)

  **mask** (``<numpy.ndarray.bool>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_correlate_weight_mask
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to Image-class
  image   = Matrix(image  )
  weights = Matrix(weights)
  mask    = Matrix(mask   )

  # initiate normalization factor
  if weights.data_b is not None:
    norm = Matrix(np.empty(roi,dtype='int32'  ))
  else:
    norm = Matrix(np.empty(roi,dtype='float64'))
  # initiate output
  if   image.data_i is not None and weights.data_i is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  elif image.data_b is not None and weights.data_b is not None:
    roi = Matrix(np.empty(roi,dtype='int32'  ))
  else:
    roi = Matrix(np.empty(roi,dtype='float64'))

  # run function, and return result
  cfun(weights.ptr(),image.ptr(),mask.ptr(),roi.ptr(),norm.ptr(),periodic)

  return (roi.asnumpy(),norm.asnumpy())

# ..............................................................................

def image_correlate_weight_collapse(image,label,centers,mask,stamp,roi,periodic):
  r'''
See: :ref:`src/core/image_correlate_weight_collapse`

:arguments:

  **image** (``<numpy.ndarray>``)

  **label,centers** (``<numpy.ndarray>``)

  **mask** (``<numpy.ndarray.bool>``)

  **stamp** (``<core.Stamp>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<numpy.ndarray>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_correlate_weight_collapse
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Stamp.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Matrix.typedef),
    ctypes.c_int,
  )

  # convert to Matrix-class
  image   = Matrix(image  )
  label  = Matrix(label )
  centers = Matrix(centers)
  mask    = Matrix(mask   )

  # initiate normalization factor
  norm = Matrix(np.empty(roi,dtype='int32'  ))
  roi  = Matrix(np.empty(roi,dtype='float64'))

  # run function, and return result
  cfun(image.ptr(),label.ptr(),centers.ptr(),mask.ptr(),
       stamp.ptr(),roi.ptr(),norm.ptr(),periodic)

  return (roi.asnumpy(),norm.asnumpy())

# ..............................................................................

def image_linealpath(image,stamp,roi,periodic):
  r'''
See: :ref:`src/core/image_linealpath`

:arguments:

  **image** (``<numpy.ndarray.bool>``)

  **stamp** (``<core.Stamp>``)

  **roi** (``<list>``)

  **periodic** = (``True`` | ``False``)

:returns:

  **result** (``<numpy.ndarray>``)

  **norm** (``<int>``)
  '''

  # define an interface to the compiled function
  cfun          = lib.image_linealpath
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Matrix.typedef),
    ctypes.POINTER(Stamp.typedef),
    ctypes.POINTER(Matrix.typedef),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.c_int,
  )

  # convert to Matrix-class
  image = Matrix(image)

  # initiate normalization factor
  norm =        np.empty((1),dtype='int32')
  roi  = Matrix(np.empty(roi,dtype='int32'))

  # run function, and return result
  cfun(image.ptr(),stamp.ptr(),roi.ptr(),norm,periodic)

  return (roi.asnumpy(),int(norm))

# ..............................................................................

def image_linealpath_stamp(shape,algorithm,mode):
  r'''
See:

* :ref:`src/core/image_linealpath_stamp_points`
* :ref:`src/core/image_linealpath_stamp_paths`

:arguments:

  **shape** (``<numpy.ndarray[nd]``)

  **algorithm** (``"bresenham"`` | ``"actual"`` | ``"full"``)

  **mode** (``'perpendicular'`` | ``'cross'`` | ``all`` | ``'lineal'``)

:returns:

  **stamp** (``<core.Stamp>``)
  '''

  # read the number of dimensions
  nd = len(shape)

  # ------------------
  # set the end points
  # ------------------

  if mode == 'lineal':

    # define an interface to the compiled function
    cfun          = lib.image_linealpath_stamp_points
    cfun.restype  = ctypes.c_int
    cfun.errcheck = error
    cfun.argtypes = (
      ctypes.c_int,
      np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
      np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
      ctypes.POINTER(ctypes.c_int),
    )

    # convert arguments
    shape = np.array(shape,dtype='int32')

    # calculate the number of points
    n = np.empty((1),dtype='int32')
    cfun(nd,shape,n,None)

    # calculate the points
    points = np.empty((nd*int(n)),dtype='int32')
    cfun(nd,shape,n,np.ctypeslib.as_ctypes(points))

    # convert the number of points to integer, reshape the points
    n      = int(n)
    points = points.reshape(n,nd)

    #TODO: make more clever
    # use full symmetry in stamps (generate only 1/2, 1/4, or 1/8 in 1,2,3 dimensions)
    # this will fix the order immediately, and make the stamps fully symmetric
    # logical ordering
    if nd==2:

      # left edge
      irow    = np.where(points[:,1]==-(shape[1]-1)/2)[0]
      isrt    = np.argsort(points[irow,0])
      iledge  = irow[isrt]
      # right edge
      irow    = np.where(points[:,1]==+(shape[1]-1)/2)[0]
      isrt    = np.argsort(points[irow,0])
      iredge  = np.flipud(irow[isrt])
      # bottom edge
      irow    = np.where(points[:,0]==+(shape[0]-1)/2)[0]
      isrt    = np.argsort(points[irow,1])
      ibottom = irow[isrt][1:-1]
      # combine edges
      isort  = np.concatenate((iledge,ibottom,iredge))
      points = points[isort,:]

  elif mode == 'all':

    if not nd==2:
      raise IOError('Only implemented in 2-D')

    # number of points
    n = 2*(shape[0]-2)+2*(shape[1])

    # calculate the points
    points = np.zeros((n,nd),dtype='int32');
    i = 0
    points[i:i+shape[1]  ,1] =  np.arange(shape[1])-(shape[1]-1)/2
    points[i:i+shape[1]  ,0] = -(shape[0]-1)/2
    i += shape[1]
    points[i:i+shape[1]  ,1] =  np.arange(shape[1])-(shape[1]-1)/2
    points[i:i+shape[1]  ,0] = +(shape[0]-1)/2
    i += shape[1]
    points[i:i+shape[0]-2,1] = -(shape[1]-1)/2
    points[i:i+shape[0]-2,0] = (np.arange(shape[0])-(shape[0]-1)/2)[1:-1]
    i += shape[0]-2
    points[i:i+shape[0]-2,1] = +(shape[1]-1)/2
    points[i:i+shape[0]-2,0] = (np.arange(shape[0])-(shape[0]-1)/2)[1:-1]
    i += shape[0]-2

  elif mode == 'perpendicular':

    # initiate the points
    n      = 2*nd
    points = np.zeros((n,nd),dtype='int32')
    # set the points along the axes
    for i in range(nd):
      points[2*i  ,i] = -(shape[i]-1)/2
      points[2*i+1,i] = +(shape[i]-1)/2

  elif mode == 'cross':

    # calculate the number of points
    if nd==1:
      n = 2*nd
    elif nd==2:
      n = 2*nd+4
    else:
      raise IOError('Non implemented')

    # initiate the points
    points = np.zeros((n,nd),dtype='int32')
    # point along the axes
    for i in range(nd):
      points[2*i  ,i] = -(shape[i]-1)/2
      points[2*i+1,i] = +(shape[i]-1)/2
    # diagonal points
    if nd==2:
      points[4,0] = -(shape[0]-1)/2
      points[4,1] = -(shape[1]-1)/2
      points[5,0] = +(shape[0]-1)/2
      points[5,1] = -(shape[1]-1)/2
      points[6,0] = -(shape[0]-1)/2
      points[6,1] = +(shape[1]-1)/2
      points[7,0] = +(shape[0]-1)/2
      points[7,1] = +(shape[1]-1)/2

  else:

    raise IOError('Unknown mode')

  # ----------------
  # create the stamp
  # ----------------

  # define an interface to the compiled function
  cfun          = lib.image_linealpath_stamp_paths
  cfun.restype  = ctypes.c_int
  cfun.errcheck = error
  cfun.argtypes = (
    ctypes.POINTER(Stamp.typedef),
    np.ctypeslib.ndpointer(ctypes.c_int,flags="C_CONTIGUOUS"),
    ctypes.c_int,
  )

  # convert voxel path algorithm, create stamp
  algorithm = {'bresenham':0,'actual':1,'full':2}[algorithm]
  stamp     = Stamp(n,nd)

  # calculate the number of voxels
  cfun(stamp.ptr(),points,algorithm)

  # complete stamp
  stamp.ja   = np.empty((stamp.ia[-1]*stamp.nd),dtype='int32')
  stamp.stat = np.zeros((stamp.ia[-1]         ),dtype='int32')
  cfun(stamp.ptr(),points,algorithm)

  # set storage
  # -----------

  # http://stackoverflow.com/questions/16970982/find-unique-rows-in-numpy-array
  def unique_rows(a):
    b = np.ascontiguousarray(a).view(np.dtype((np.void, a.dtype.itemsize * a.shape[1])))
    _, idx = np.unique(b, return_index=True)
    return idx

  # per voxel, determine to include in the statistics
  stamp.stat[unique_rows(stamp.ja.reshape(-1,nd))] = 1

  return stamp
