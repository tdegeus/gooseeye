
#ifndef CORE_H
#define CORE_H

// include standard libraries
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// export settings on different platforms
#if defined (_MSC_VER)
  #define EXPORT __declspec(dllexport)
#else
  #define EXPORT
#endif

// simple support functions
#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))
#define ABS(a)   (((a)<0) ? -(a) : (a))
#define SIGN(a)  (((a)<0) ? -1 : (a)>0 ? 1 : 0)

// 3-D matrix index -> array index
#define M3(h,i,j,N)  ( (h)*(N)[1]*(N)[2]+(i)*(N)[2]+(j) )

// 3-D periodic matrix index -> array index
#define M3P(h,i,j,N) \
  ( ( ((h)<0) ? (h)+(N)[0] : ((h)>=(N)[0]) ? (h)-(N)[0] : (h) )*(N)[1]*(N)[2]+\
    ( ((i)<0) ? (i)+(N)[1] : ((i)>=(N)[1]) ? (i)-(N)[1] : (i) )*(N)[2]       +\
    ( ((j)<0) ? (j)+(N)[2] : ((j)>=(N)[2]) ? (j)-(N)[2] : (j) )              )

// 2-D matrix index -> array index
#define M2(i,j,N)  ( (i)*(N)[1]+(j) )

// 2-D periodic matrix index -> array index
#define M2P(i,j,N) \
  ( ( ((i)<0) ? (i)+(N)[0] : ((i)>=(N)[0]) ? (i)-(N)[0] : (i) )*(N)[1]       +\
    ( ((j)<0) ? (j)+(N)[1] : ((j)>=(N)[1]) ? (j)-(N)[1] : (j) )              )

/* =============================================================================
 * error handling
 * ========================================================================== */

/*!
\verbatim embed:rst

Return the global error string.

Functions return an exit code, with the following meaning:

* ``0``: process ended without errors;
* ``1``: an error has occurred, raise an exception.

When the exit-code is ``1``, an error message is stored to the global variable
``error_string``.

:arguments:

  ``<void>``

:returns:

  **error** (``char*``)
    The error string.

\endverbatim
*/
extern EXPORT char* get_error();

// define global error string
char *error_string;

/* =============================================================================
 * matrix data-structure
 * ========================================================================== */

/*!
\verbatim embed:rst

Store a matrix (of booleans, integers, or doubles)

:fields:

  **data_b** (``int*``), **data_i** (``int*``), **data_d** (``double*``)
    Pointer to store the matrix as pointer to a 1-D array. These fields are
    usually mutually exclusive: two fields are a ``NULL`` pointer.

  **shape** (``int* = int[nd]``)
    The shape of the matrix.

  **size** (``int``)
    Total size of the matrix (``== prod(shape)``).

  **nd** (``int``)
    Number of dimensions of the matrix.

\endverbatim
*/
typedef struct Matrix {
  int*    data_b  ;
  int*    data_i  ;
  double* data_d  ;
  int*    shape   ;
  int     size    ;
  int     nd      ;
} Matrix;


/* =============================================================================
 * check data-type
 * ========================================================================== */

/*!
\verbatim embed:rst

Check data-type of the matrix.

:arguments:

  **self** (``Matrix*``), *in*
    The matrix.

  **has_bool**   (``int = -1 | 0 | 1``)
    If ``0/1`` the function checks if ``data_b`` is yes/no a ``NULL`` pointer.

  **has_int**    (``int = -1 | 0 | 1``)
    If ``0/1`` the function checks if ``data_i`` is yes/no a ``NULL`` pointer.

  **has_double** (``int = -1 | 0 | 1``)
    If ``0/1`` the function checks if ``data_d`` is yes/no a ``NULL`` pointer.

:returns:

  **error** (``<int> = 0 | 1``)
    See ``core/get_error``.

\endverbatim
*/
int matrix_dtype(Matrix* self, int has_bool, int has_int, int has_double);

/* =============================================================================
 * create quasi N-D shape
 * ========================================================================== */

/*!
\verbatim embed:rst

This function can:

* ``shape == NULL``:
  check the number of dimensions (to match a certain value)

* ``shape != NULL``:
  calculate a quasi-N-D shape array, and check the number of dimensions only
  as an upper limit.

:arguments:

  **self** (``Matrix*``), *in*
    The matrix.

  **N** (``int``)
    Number of dimensions (of the quasi-N-D shape).

  **shape** (``int* = int[N] | NULL``), *out*
    The quasi-N-D shape.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
 */
int matrix_shape(Matrix* self, int N, int* shape);

/* =============================================================================
 * check if 2 matrices are consistent
 * ========================================================================== */

/*!
\verbatim embed:rst

Check if two matrices are consistent in terms of size and data-type.

:arguments:

  **self,other** (``Matrix*``), *in*
    The matrix.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
 */
int matrix_consistent(Matrix* self, Matrix* other);

#endif
