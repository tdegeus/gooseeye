
#include "image.h"

/*  ========================================================================  */

int image_mid(Matrix* self, int* mid)
{

  int i;
  int shape[3];

  // calculate quasi-3D shape
  matrix_shape(self,3,shape);

  // check if image shape is odd
  for ( i=0 ; i<3 ; i++ ) {
    if ( !(shape[i]%2) && shape[i]!=0 ) {
      error_string = "Image shape must be odd";
      return 1;
    }
  }

  // calculate the index of middle of the image
  for ( i=0 ; i<3 ; i++ ) {
    if ( shape[i]>1 )
      mid[i] = (shape[i]-1)/2;
    else
      mid[i] = 0;
  }

  return 0;

}

/*  ========================================================================  */

extern int image_path(
  int* xa, int* xb, int mode, int ndim, int* N, int* path
)
{

  // determine storage logical
  int store;

  if ( path==NULL )
    store = 0;
  else
    store = 1;

  /* ------------------------------------------------------------------------
   * Bresenham algorithm
   *
   * References:
   *   http://www.luberth.com/plotter/line3d.c.txt.html
   * ------------------------------------------------------------------------ */

  if ( mode==0 ) {

    int a[3],s[3],x[3],d[3];
    int in[2],j;
    int i,iin;
    int nnz=0;

    // set defaults
    for ( i=0; i<3; i++ ) {
      a[i] = 1;
      s[i] = 0;
      x[i] = 0;
      d[i] = 0;
    }

    // calculate:
    // absolute distance, set to "1" if the distance in one
    // sign of the distance (can be -1/+1 or 0)
    // current position (temporary value)
    for ( i=0; i<ndim; i++) {
      a[i] = ABS (xb[i]-xa[i]) << 1;
      s[i] = SIGN(xb[i]-xa[i]);
      x[i] = xa[i];
    }

    // determine which direction is dominant
    for ( j=0; j<3; j++ ) {
      // set the remaining directions
      iin = 0;
      for ( i=0; i<3; i++ ) {
        if ( i!=j ) {
          in[iin] = i;
          iin    += 1;
        }
      }
      // determine if the current direction is dominant
      if ( a[j] >= MAX(a[in[0]],a[in[1]]) )
        break;
    }

    // set increment in non-dominant directions
    for ( i=0; i<2; i++)
      d[in[i]] = a[in[i]]-(a[j]>>1);
    // loop until "x" coincides with "xb"
    while ( 1 ) {
      // add current voxel to path
      if ( store )
        for ( i=0; i<ndim; i++)
          path[nnz*ndim+i] = x[i];
      // update voxel counter
      nnz += 1;
      // check convergence
      if ( x[j]==xb[j] ) {
        *N = nnz;
        return 0;
      }
      // check increment in other directions
      for ( i=0; i<2; i++ ) {
        if ( d[in[i]]>=0 ) {
          x[in[i]] += s[in[i]];
          d[in[i]] -= a[j];
        }
      }
      // increment
      x[j] += s[j];
      for ( i=0; i<2; i++ )
        d[in[i]] += a[in[i]];
    }

  }

  /* ------------------------------------------------------------------------
   * Voxel through which the line crosses:
   * - actual:  only those voxel which enclose the line
   * - full:    only cross faces (never cross a single point)
   * ------------------------------------------------------------------------ */

  if ( mode==1 || mode==2 ) {

    // position, slope, (length to) next intersection
    double x[3],v[3],t[3],next[3],sign[3];
    int isign[3];
    // active dimensions (for in-plane paths dimension have to be skipped
    // to avoid dividing by zero)
    int in[3],iin,nin;
    // path of the current voxel
    int cindex[3];
    int nnz = 1;
    // counters
    int i,imin,n;

    // set the direction coefficient in all dimensions; if it is zero this
    // dimension is excluded from further analysis (i.e. in-plane growth)
    nin = 0;
    for ( i=0 ; i<ndim ; i++ ) {
      // set origin, store to output array; initiate the position
      cindex[i] = xa[i];
      if ( store )
        path[i]  = xa[i];
      // initiate position, set slope
      x[i] = (double)(xa[i]);
      v[i] = (double)(xb[i]-xa[i]);
      // non-zero slope: calculate the sign and the next intersection
      // with a voxel's edges, and update the list to include this dimension
      // in the further analysis
      if ( v[i] ) {
        sign[i]  = v[i]/fabs(v[i]);
        isign[i] = (int)sign[i];
        next[i]  = sign[i]*0.5;
        in[nin]  = i;
        nin++;
      }
    }

    // starting from "xa" loop to "xb"
    while ( 1 ) {

      // find translation coefficient "t" for each next intersection
      // (only include dimensions with non-zero slope)
      for ( iin=0 ; iin<nin ; iin++ ) {
        i      = in[iin];
        t[iin] = (next[i]-x[i])/v[i];
      }
      // find the minimum "t": the intersection which is closet along the line
      // from the current position -> proceed in this direction
      imin = 0;
      for ( iin=1 ; iin<nin ; iin++ )
        if ( t[iin]<t[imin] )
          imin = iin;

      // update path: proceed in dimension of minimum "t"
      // note: if dimensions have equal "t" -> proceed in each dimension
      for ( iin=0 ; iin<nin ; iin++ ) {
        if ( fabs(t[iin]-t[imin])<1.e-6 ) {
          cindex[in[iin]] += isign[in[iin]];
          next[in[iin]]   += sign[in[iin]];
          // store all the face crossings ("mode")
          if ( mode==2 ) {
            if ( store )
              for ( i=0 ; i<ndim ; i++ )
                path[nnz*ndim+i] = cindex[i];
            nnz++;
          }
        }
      }
      // store only the next voxel ("actual")
      if ( mode==1 ) {
        if ( store )
          for ( i=0 ; i<ndim ; i++ )
            path[nnz*ndim+i] = cindex[i];
        nnz++;
      }
      // update position, and current path
      for ( i=0 ; i<ndim ; i++ )
        x[i] = xa[i]+v[i]*t[imin];

      // check convergence: stop when "xb" is reached
      n = 0;
      for ( i=0 ; i<ndim; i++ )
        if ( cindex[i]==xb[i] )
          n++;
      if ( n==ndim )
        break;

    }

    // number of voxels in the path
    *N = nnz;
    return 0;
  }

  return 1;
}

/*  ========================================================================  */

extern int image_dummy_circles(Matrix* self, Matrix* shapes, int periodic)
{

  int*   I;
  int    ishape;
  int    i,j,di,dj,Di,Dj,pi,pj;
  double x,y,r,dx,dy;

  // check data-type and check if 2-D
  if ( matrix_dtype(self  ,-1,-1, 0) ) return 1;
  if ( matrix_dtype(shapes, 0, 0, 1) ) return 1;
  if ( matrix_shape(self  , 2,NULL ) ) return 1;
  if ( matrix_shape(shapes, 2,NULL ) ) return 1;

  // create alias for the image
  if      ( self->data_b!=NULL ) {
    I = self->data_b;
  }
  else if ( self->data_i!=NULL ) {
    I = self->data_b;
  }
  else {
    error_string = "Image must have 'data_b' OR 'data_i'";
    return 1;
  }

  // create alias for the shape
  int* N = self->shape;

  // zero-initialize the entire image
  for ( i=0 ; i<self->size ; i++ )
    I[i] = 0;

  // loop over shapes
  for ( ishape=0 ; ishape<(shapes->shape[0]) ; ishape++ )
  {

    // read shape
    x = shapes->data_d[ishape*3+0];
    y = shapes->data_d[ishape*3+1];
    r = shapes->data_d[ishape*3+2];

    // distance in vertical direction
    Di = (int)( (double)N[0]*r );
    pi = (int)( (double)N[0]*y );
    pj = (int)( (double)N[1]*x );

    // loop over vertical distances
    for ( di=-Di ; di<+Di ; di++ ) {

      // calculate horizontal distance
      dy = (double)di / (double)N[0];
      dx = pow(pow(r,2.0)-pow(dy,2.0),0.5);
      Dj = (int)( (double)N[1]*dx );

      // loop over vertical distances
      for ( dj=-Dj ; dj<+Dj ; dj++ ) {

        // find the pixel index
        i = pi+di;
        j = pj+dj;

        // periodic image -> account for periodicity
        if ( periodic )
          I[M2P(i,j,N)] = 1;

        // not periodic image -> skip if outside the boundary
        else
          if ( !( i<0 || i>N[0] || j<0 || j>N[1] ) )
            I[M2(i,j,N)] = 1;

      }
    }
  }

  return 0;
}

/*  ========================================================================  */

extern int image_clusters(
  Matrix* image, Matrix* structure, int periodic, int min_size,
  Matrix* label, int* num_features
)
{

  int* L = label->data_i;
  int  i,j;

  // label the clusters
  if ( image_clusters_label(image,structure,periodic,label,num_features) ) return 1;


  // threshold cluster-size
  if ( min_size >= 0 )
  {
    // - calculate size
    int* num = (int*) malloc( ((*num_features)+1)*sizeof(int) );
    if ( image_clusters_size(label,*num_features,num) ) { free(num); return 1; }

    // - apply threshold: remove clusters with too small size
    for ( i=0 ; i<label->size ; i++ ) {
      if ( num[L[i]]<min_size )
        L[i] = 0;
    }

    // - renumber
    // --  initialize logical list to check if the label is included
    for ( i=0 ; i<(*num_features)+1 ; i++ )
      num[i] = -1;
    // --  fill logical list
    for ( i=0 ; i<label->size ; i++ )
      num[L[i]] = 1;
    // --  new numbering for the included labels
    j = 0;
    for ( i=0 ; i<(*num_features)+1 ; i++ ) {
      if ( num[i] != -1 ) {
        num[i] = j;
        j++;
      }
    }
    // -- renumber the labels
    for ( i=0 ; i<label->size ; i++ )
      L[i] = num[L[i]];

    free(num);

  }

  return 0;
}

/*  ------------------------------------------------------------------------  */

int image_clusters_label(
  Matrix* image, Matrix* structure, int periodic, Matrix* label, int* num_features
)
{

  int  h,i,j,di,dj,dh,idx,jdx,ii,jj,nlab;
  int  NI[3],NS[3],lb[3],ub[3];
  int  mid[] = {0,0,0};
  int  lab   = 0;
  int  N     = image    ->size;
  int* I     = image    ->data_b;
  int* S     = structure->data_b;
  int* L     = label    ->data_i;

  // check input
  if ( matrix_dtype(image    ,1,0,0)   ) return 1;
  if ( matrix_dtype(label    ,0,1,0)   ) return 1;
  if ( matrix_dtype(structure,1,0,0)   ) return 1;
  // calculate quasi-3D shape
  if ( matrix_shape(image    ,3,NI) ) return 1;
  if ( matrix_shape(structure,3,NS) ) return 1;
  // midpoint of the structure
  if ( image_mid(structure,mid)     ) return 1;

  // local arrays
  int* lnk = (int*)malloc(image->size*sizeof(int));
  int* inc = (int*)malloc(image->size*sizeof(int));

  // initiate labels / cluster-links / saved clusters
  for ( i=0 ; i<N ; i++ ) {
    L  [i] = 0;
    lnk[i] = i;
    inc[i] = 0;
  }
  inc[0] = 1;

  // periodic: lower/upper bound of the structure is always the same (full)
  if ( periodic )
  {
    for ( i=0 ; i<3 ; i++ )
    {
      lb[i] = -mid[i];
      ub[i] = +mid[i];
    }
  }

  // loop over voxels (in all directions)
  for ( h=0 ; h<NI[0] ; h++ ) {
    for ( i=0 ; i<NI[1] ; i++ ) {
      for ( j=0 ; j<NI[2] ; j++ ) {

        // array-index of the voxel
        idx = M3(h,i,j,NI);

        // only continue of non-zero voxels
        if ( I[idx] )
        {

          // set lower/upper bound of the structure near edges
          if ( !periodic )
          {
            if ( h <        mid[0] ) lb[0]=0; else lb[0]=-mid[0];
            if ( i <        mid[1] ) lb[1]=0; else lb[1]=-mid[1];
            if ( j <        mid[2] ) lb[2]=0; else lb[2]=-mid[2];
            if ( h >= NI[0]-mid[0] ) ub[0]=0; else ub[0]=+mid[0];
            if ( i >= NI[1]-mid[1] ) ub[1]=0; else ub[1]=+mid[1];
            if ( j >= NI[2]-mid[2] ) ub[2]=0; else ub[2]=+mid[2];
          }

          // cluster not yet assigned: try couple to existing
          if ( L[idx]==0 )
          {
            for ( dh=lb[0] ; dh<=ub[0] ; dh++ ) {
              for ( di=lb[1] ; di<=ub[1] ; di++ ) {
                for ( dj=lb[2] ; dj<=ub[2] ; dj++ ) {
                  // check value of the structure
                  if ( S[M3(dh+mid[0],di+mid[1],dj+mid[2],NS)] )
                  {
                    // array-index of the comparison voxel
                    if ( !periodic ) { jdx = M3 (h+dh,i+di,j+dj,NI); }
                    else             { jdx = M3P(h+dh,i+di,j+dj,NI); }
                    // couple if possible
                    if ( L[jdx] )
                    {
                      L[idx] = L[jdx];
                      goto end;
                    }
                  }
                }
              }
            }
          }
          end: ;

          // cluster not yet assigned: create new cluster
          if ( L[idx]==0 )
          {
            lab++;
            L[idx] = lab;
            inc[lab] = 1;
          }

          // try to couple surrounding
          for ( dh=lb[0] ; dh<=ub[0] ; dh++ ) {
            for ( di=lb[1] ; di<=ub[1] ; di++ ) {
              for ( dj=lb[2] ; dj<=ub[2] ; dj++ ) {
                // check value of the structure
                if ( S[M3(dh+mid[0],di+mid[1],dj+mid[2],NS)] )
                {
                  // array-index of the comparison voxel
                  if ( !periodic ) { jdx = M3 (h+dh,i+di,j+dj,NI); }
                  else             { jdx = M3P(h+dh,i+di,j+dj,NI); }
                  // add other voxel to cluster / create new cluster for it
                  if ( I[jdx] )
                  {
                    if   ( L[jdx]==0 )
                    {
                      L[jdx] = L[idx];
                    }
                    else
                    {
                      if ( image_cluster_link(lnk,L[idx],L[jdx]) ) { free(lnk); free(inc); return 1; }
                    }
                  }
                }
              }
            }
          }

        }

      }
    }
  }

  // renumber clusters
  // - initiate new number
  nlab = 0;
  // - loop over all labels
  for ( i=0 ; i<=lab ; i++ )
  {
    // - loop over all coupled labels
    if ( inc[i] )
    {
      ii = i;
      while ( 1 )
      {
        jj      = lnk[ii];
        lnk[ii] = nlab;
        inc[ii] = 0;
        if ( jj==i ) { break; }
        ii      = jj;
      }
      nlab++;
    }
  }
  // - renumber all labels
  for ( i=0 ; i<N ; i++ )
    L[i] = lnk[L[i]];

  num_features[0] = nlab-1;

  free(lnk); free(inc);
  return 0;
}

/*  ------------------------------------------------------------------------  */

int image_cluster_link(int* linked, int a, int b)
{

  if ( a==b )
    return 0;

  int i,c;

  if ( a>b )
  {
    c = a;
    a = b;
    b = c;
  }

  // both unlinked
  // -------------

  if ( linked[a]==a && linked[b]==b )
  {
    linked[a] = b;
    linked[b] = a;
    return 0;
  }

  // a linked / b unlinked
  // ---------------------

  if ( linked[a]!=a && linked[b]==b )
  {
    linked[b] = linked[a];
    linked[a] = b;
    return 0;
  }

  // a unlinked / b linked
  // ---------------------

  if ( linked[a]==a && linked[b]!=b )
  {
    linked[a] = linked[b];
    linked[b] = a;
    return 0;
  }

  // both linked -> to each other
  // ----------------------------

  i = a;
  while ( 1 )
  {
    if ( linked[i]==b ) { return 0; }
    i = linked[i];
    if ( i==a ) { break; }
  }

  // both linked -> to different groups
  // ----------------------------------

  c = linked[a];
  linked[a] = b;

  i = a;
  while ( 1 )
  {
    i = linked[i];
    if ( linked[i]==b ) { break; }
  }
  linked[i] = c;

  return 0;

}

/*  ------------------------------------------------------------------------  */

int image_clusters_size(Matrix* label, int num_features, int* size_features)
{

  int  i;
  int  n = num_features+1;
  int  N = label->size;
  int* F = label->data_i;
  int* S = size_features;

  // check size
  if ( matrix_dtype(label,0,1,0) ) return 1;

  // initialize
  for ( i=0 ; i<n ; i++ )
    S[i] = 0;

  // calculate size
  for ( i=0 ; i<N ; i++ )
    S[F[i]]++;

  return 0;
}

/*  ------------------------------------------------------------------------  */

extern int image_clusters_center(
  Matrix* image, Matrix* structure, Matrix* label, int num_features, int periodic,
  Matrix* center, int* size_features
)
{

  if ( !periodic )
  {
    if ( image_clusters_center_np(label,num_features,center,size_features) )
      return 1;
  }
  else
  {
    if ( image_clusters_center_p(image,structure,label,num_features,center,size_features) )
      return 1;
  }

  return 0;
}

/*  ------------------------------------------------------------------------  */

int image_clusters_center_np(
  Matrix* label, int num_features, Matrix* center, int* size_features
)
{

  // set the number of labels. NB: "num_features" is the number of clusters,
  // this does not include the zero background
  int nlabel = num_features+1;

  // set local arrays and counters
  int* n = size_features;
  int  idx,h,i,j;
  int  NI[3];

  // check data-type and set quasi-3D shape
  if ( matrix_dtype(label ,0,1,0) ) return 1;
  if ( matrix_dtype(center,0,1,0) ) return 1;
  if ( matrix_shape(label ,3,NI)   ) return 1;

  // local arrays
  int* x = (int*)malloc((nlabel)*sizeof(int));
  int* y = (int*)malloc((nlabel)*sizeof(int));
  int* z = (int*)malloc((nlabel)*sizeof(int));

  // alias header variables
  int* I = label->data_i;
  int* C = center->data_i;

  // zero-initialize average index and number of voxels per cluster
  for ( i=0 ; i<nlabel ; i++ ) {
    z[i] = 0;
    y[i] = 0;
    x[i] = 0;
    n[i] = 0;
  }

  // loop over the image to update the counters for each label
  for ( h=0 ; h<NI[0] ; h++ ) {
    for ( i=0 ; i<NI[1] ; i++ ) {
      for ( j=0 ; j<NI[2] ; j++ ) {
        // find the cluster index
        idx = I[M3(h,i,j,NI)];
        n[idx]++;
        // update the average position of the cluster
        if ( idx ) {
          z[idx] += h;
          y[idx] += i;
          x[idx] += j;
        }
      }
    }
  }

  // empty image
  for ( i=0 ; i<label->size ; i++ )
    C[i] = 0;

  // fill the centers of gravity
  for ( idx=1 ; idx<nlabel ; idx++ ) {
    if ( n[idx]>0 ) {

      h = (int)round( (float)z[idx] / (float)n[idx] );
      i = (int)round( (float)y[idx] / (float)n[idx] );
      j = (int)round( (float)x[idx] / (float)n[idx] );

      if ( h <  0     ) { h = 0    ; }
      if ( i <  0     ) { i = 0    ; }
      if ( j <  0     ) { j = 0    ; }
      if ( h >= NI[0] ) { h = NI[0]; }
      if ( i >= NI[1] ) { i = NI[1]; }
      if ( j >= NI[2] ) { j = NI[2]; }

      C[M3(h,i,j,NI)] = idx;
    }
  }

  free(x); free(y); free(z);
  return 0;

}

/*  ========================================================================  */

int image_clusters_center_p(
  Matrix* image, Matrix* structure, Matrix* label, int num_features,
  Matrix* center, int* size_features
)
{

  // non-periodic labels
  // -------------------

  int* L  = label->data_i;
  int* L_ = (int*) malloc( image->size*sizeof(int) );
  int  num_features_[1];

  Matrix label_;
  label_.data_b = NULL;
  label_.data_i = L_;
  label_.data_d = NULL;
  label_.shape  = image->shape;
  label_.size   = image->size;
  label_.nd     = image->nd;

  if ( image_clusters_label(image,structure,0,&label_,num_features_) ) { free(L_); return 1; }

  // local variables
  // ---------------

  int  h,i,j,idx;
  int  NI[3];
  int  N   = num_features    +1;
  int  N_  = num_features_[0]+1;
  int* C   = center->data_i;
  int* n   = size_features;

  // check data-type and set quasi-3D shape
  if ( matrix_dtype(label ,0,1,0) ) { free(L_); return 1; }
  if ( matrix_dtype(center,0,1,0) ) { free(L_); return 1; }
  if ( matrix_shape(label ,3,NI)  ) { free(L_); return 1; }

  // local arrays
  int* x   = (int*) malloc( N *sizeof(int) );
  int* y   = (int*) malloc( N *sizeof(int) );
  int* z   = (int*) malloc( N *sizeof(int) );
  int* n_  = (int*) malloc( N_*sizeof(int) );
  int* x_  = (int*) malloc( N_*sizeof(int) );
  int* y_  = (int*) malloc( N_*sizeof(int) );
  int* z_  = (int*) malloc( N_*sizeof(int) );
  int* num = (int*) malloc( N_*sizeof(int) );
  int* nc  = (int*) malloc( N *sizeof(int) );

  // numbers: non-periodic -> periodic
  for ( i=0 ; i<label->size ; i++ )
    num[L_[i]] = L[i];

  // count the number of repetitions in the non-periodic image
  // (apply periodicity only to repeated clusters)
  for ( i=0 ; i<N ; i++ )
    nc[i] = 0;
  for ( i=0 ; i<N_ ; i++ )
    nc[num[i]]++;


  // calculate position: non-periodic
  // --------------------------------

  // zero-initialize average index and number of voxels per non-periodic cluster
  for ( i=0 ; i<N_ ; i++ ) {
    z_[i] = 0;
    y_[i] = 0;
    x_[i] = 0;
    n_[i] = 0;
  }

  // loop over the image to update the counters for each label
  for ( h=0 ; h<NI[0] ; h++ ) {
    for ( i=0 ; i<NI[1] ; i++ ) {
      for ( j=0 ; j<NI[2] ; j++ ) {
        // find the cluster index
        idx = L_[M3(h,i,j,NI)];
        n_[idx]++;
        // update the average position of the cluster
        if ( idx ) {
          if ( nc[num[idx]]>1 ) {
            if ( h>NI[0]/2 ) { z_[idx] += (h-NI[0]); } else { z_[idx] += h; }
            if ( i>NI[1]/2 ) { y_[idx] += (i-NI[1]); } else { y_[idx] += i; }
            if ( j>NI[2]/2 ) { x_[idx] += (j-NI[2]); } else { x_[idx] += j; }
          }
          else {
            z_[idx] += h;
            y_[idx] += i;
            x_[idx] += j;
          }
        }
      }
    }
  }

  // convert to periodic
  // -------------------

  // zero-initialize average index and number of voxels per periodic cluster
  for ( i=0 ; i<N ; i++ ) {
    z[i] = 0;
    y[i] = 0;
    x[i] = 0;
    n[i] = 0;
  }

  // read positions and number from non-periodic clusters
  for ( i=0 ; i<N_ ; i++ ) {
    z[num[i]] += z_[i];
    y[num[i]] += y_[i];
    x[num[i]] += x_[i];
    n[num[i]] += n_[i];
  }

  // store centers
  // -------------

  // empty
  for ( i=0 ; i<label->size ; i++ )
    C[i] = 0;

  // fill the centers of gravity
  for ( idx=1 ; idx<N ; idx++ ) {
    if ( n[idx]>0 ) {
      h = (int)round( (float)z[idx] / (float)n[idx] );
      i = (int)round( (float)y[idx] / (float)n[idx] );
      j = (int)round( (float)x[idx] / (float)n[idx] );

      C[M3P(h,i,j,NI)] = idx;
    }
  }

  // de-allocate memory
  free(L_ );
  free(n_ );
  free(x_ );
  free(y_ );
  free(z_ );
  free(x  );
  free(y  );
  free(z  );
  free(num);
  free(nc );

  return 0;

}

/*  ------------------------------------------------------------------------  */

extern int image_clusters_dilate(
  Matrix* label, int num_features, int* iterations, Matrix* kernel, int periodic
)
{

  int h,i,j,dh,di,dj;
  int mid[] = {0,0,0};
  int FN[3],RN[3];

  // alias header variables
  int* F  = label ->data_i;
  int* R  = kernel->data_b;
  int  FS = label ->size ;
  int  ilabel,jdx,iter;
  int  max_iter = 0;

  // quasi-3D shape
  if ( matrix_shape(label ,3,FN) ) return 1;
  if ( matrix_shape(kernel,3,RN) ) return 1;

  // check data-type
  if ( matrix_dtype(label ,0,1,0) ) return 1;
  if ( matrix_dtype(kernel,1,0,0) ) return 1;

  // calculate the midpoint of the kernel
  if ( image_mid(kernel,mid) ) return 1;

  // check the number of features
  for ( i=0 ; i<FS ; i++ ) {
    if ( F[i]>num_features ) {
      error_string = "'num_features' < MAX(label)";
      return 1;
    }
  }

  // find maximum number of iterations
  for ( i=0 ; i<num_features ; i++ )
    max_iter = MAX(max_iter,iterations[i+1]);

  // loop over iterations
  for ( iter=0 ; iter<max_iter ; iter++ ) {

    // loop over all voxel
    for ( h=0 ; h<FN[0] ; h++ ) {
      for ( i=0 ; i<FN[1] ; i++ ) {
        for ( j=0 ; j<FN[2] ; j++ ) {
          // label over the current voxel
          ilabel = F[M3(h,i,j,FN)];
          // proceed:
          // - for non-zero label
          // - if the number of iterations for this label has not been exceeded
          if ( ilabel>0 && iterations[ilabel]>iter ) {
            // loop over the kernel
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // dilate for non-zero kernel value
                  if ( R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] && !(dh==0 && di==0 && dj==0) ) {
                    // periodic image
                    if ( periodic ) {
                      // array-index of comparison voxel
                      jdx = M3P(h+dh,i+di,j+dj,FN);
                      // dilate (only for zero comparison voxel)
                      if ( !F[jdx] ) {
                        F[jdx] = -1*ilabel;
                      }
                    }
                    // non-periodic image
                    else {
                      // check if the comparison voxel in the image
                      if ( h+dh<FN[0] && h+dh>=0 && i+di<FN[1] && i+di>=0 && j+dj<FN[2] && j+dj>=0 ) {
                        // array-index of comparison voxel
                        jdx = M3(h+dh,i+di,j+dj,FN);
                        // dilate (only for zero comparison voxel)
                        if ( !F[jdx] ) {
                          F[jdx] = -1*ilabel;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // copy new image to input image
    for ( i=0 ; i<FS ; i++ )
      F[i] = ABS(F[i]);

  }

  return 0;
}

/*  ========================================================================  */

extern int image_roimul(Matrix* f, Matrix* roi, Matrix* res, int periodic)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx;
  int bnd[] = {0,0,0};  // band to skip along the edges of the images
  int mid[] = {0,0,0};  // mid-point of the 'roi'
  int FN[3],RN[3];

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f  ,3,FN) ) return 1;
  if ( matrix_shape     (roi,3,RN) ) return 1;
  if ( matrix_consistent(f  ,res ) ) return 1;
  if ( image_mid        (roi,mid ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image 'bnd = {0,0,0}', i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------------------
  // 2 floating-point images: convolution
  // ------------------------------------

  if ( f->data_d!=NULL && res->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    double* F = f  ->data_d;
    double* O = res->data_d;
    double* R = roi->data_d;

    // zero-initialize result
    for ( i=0 ; i<(FN[0]*FN[1]*FN[2]) ; i++ )
      O[i] = 0.0;

    // calculate result:
    // (a) loop over the voxels in all directions
    // (b) loop over the neighborhood around (h,i,j)
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // compare to neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison pixel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update correlation
                O[idx] += F[jdx]*R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)];
              }
            }
          }
        }
      }
    }

    return 0;

  }

  // ------------------------------------------------------------------
  // 2 integer images:
  // 2-point probability (boolean) / 2-point cluster function (integer)
  // ------------------------------------------------------------------

  if ( f->data_i!=NULL && res->data_i!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f  ->data_i;
    int* O = res->data_i;
    int* R = roi->data_i;

    // zero-initialize result
    for ( i=0 ; i<(FN[0]*FN[1]*FN[2]) ; i++ )
      O[i] = 0;

    // calculate result:
    // (a) loop over the voxels in all directions
    // (b) loop over the neighborhood around (h,i,j)
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // compare to neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison pixel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update correlation
                O[idx] += F[jdx]*R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)];
              }
            }
          }
        }
      }
    }

    return 0;
  }

  error_string = "Unknown data-type combination of 'f' and 'g'";
  return 1;

}

/*  ========================================================================  */

extern int image_roimul_nonneg(Matrix* f, Matrix* roi, Matrix* res, int periodic)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx;
  int bnd[] = {0,0,0};  // band to skip along the edges of the images
  int mid[] = {0,0,0};  // mid-point of the 'roi'
  int FN[3],RN[3];
  int ti;
  double td;

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f  ,3,FN) ) return 1;
  if ( matrix_shape     (roi,3,RN) ) return 1;
  if ( matrix_consistent(f  ,res ) ) return 1;
  if ( image_mid        (roi,mid ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image 'bnd = {0,0,0}', i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------------------
  // 2 floating-point images: convolution
  // ------------------------------------

  if ( f->data_d!=NULL && res->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    double* F = f  ->data_d;
    double* O = res->data_d;
    double* R = roi->data_d;

    // zero-initialize result
    for ( i=0 ; i<(FN[0]*FN[1]*FN[2]) ; i++ )
      O[i] = 0.0;

    // calculate result:
    // (a) loop over the voxels in all directions
    // (b) loop over the neighborhood around (h,i,j)
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // compare to neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison pixel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update correlation
                td = F[jdx]*R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)];
                if ( td >= 0.0 )
                  O[idx] += td;
              }
            }
          }
        }
      }
    }

    return 0;

  }

  // ------------------------------------------------------------------
  // 2 integer images:
  // 2-point probability (boolean) / 2-point cluster function (integer)
  // ------------------------------------------------------------------

  if ( f->data_i!=NULL && res->data_i!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f  ->data_i;
    int* O = res->data_i;
    int* R = roi->data_i;

    // zero-initialize result
    for ( i=0 ; i<(FN[0]*FN[1]*FN[2]) ; i++ )
      O[i] = 0;

    // calculate result:
    // (a) loop over the voxels in all directions
    // (b) loop over the neighborhood around (h,i,j)
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // compare to neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison pixel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update correlation
                ti = F[jdx]*R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)];
                if ( ti >= 0 )
                  O[idx] += ti;
              }
            }
          }
        }
      }
    }

    return 0;
  }

  error_string = "Unknown data-type combination of 'f' and 'g'";
  return 1;

}

/*  ========================================================================  */

extern int image_correlate(Matrix* f, Matrix* g, Matrix* roi, int* norm, int periodic)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx;
  int bnd[] = {0,0,0};  // band to skip along the edges of the images
  int mid[] = {0,0,0};  // mid-point of the 'roi'
  int FN[3],RN[3],N;

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f  ,3,FN) ) return 1;
  if ( matrix_shape     (roi,3,RN) ) return 1;
  if ( matrix_consistent(f  ,g   ) ) return 1;
  if ( image_mid        (roi,mid ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image 'bnd = {0,0,0}', i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------
  // set normalization factor
  // ------------------------

  // set the normalization: number of considered pixels
  N = 0;
  for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ )
    for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ )
      for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ )
        N++;

  // avoid zero division
  if ( N==0 )
    N = 1;

  // store as pointer (to make 'norm' an output variable)
  *norm = N;

  // ------------------------------------
  // 2 floating-point images: convolution
  // ------------------------------------

  if ( f->data_d!=NULL && g->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    double* F = f  ->data_d;
    double* G = g  ->data_d;
    double* R = roi->data_d;

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0.0;

    // calculate result:
    // (a) loop over the voxels in all directions
    // (b) loop over the neighborhood around (h,i,j)
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // compare to neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison pixel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update correlation
                R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] += F[idx]*G[jdx];
              }
            }
          }
        }
      }
    }

    return 0;

  }

  // ------------------------------------------------------------------
  // 2 integer images:
  // 2-point probability (boolean) / 2-point cluster function (integer)
  // ------------------------------------------------------------------

  if ( ( f->data_i!=NULL && g->data_i!=NULL ) || ( f->data_b!=NULL && g->data_b!=NULL ) ) {

    // check data-type
    if ( matrix_dtype(roi,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f  ->data_b;
    int* G = g  ->data_b;
    int* R = roi->data_i;
    if ( f->data_i!=NULL && g->data_i!=NULL ) {
      F = f->data_i;
      G = g->data_i;
    }

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0;

    // loop over voxels (in all directions)
    for ( h=bnd[0] ; h<FN[0]-bnd[0] ; h++ ) {
      for ( i=bnd[1] ; i<FN[1]-bnd[1] ; i++ ) {
        for ( j=bnd[2] ; j<FN[2]-bnd[2] ; j++ ) {
          // array-index of the pixel
          idx = M3(h,i,j,FN);
          // proceed only for non-zero voxel of 'F'
          // a zero value will yield zero for the entire ROI, therefore skipped
          if ( F[idx] ) {
            // initiate relative distance
            dh = 0;
            di = 0;
            dj = 0;
            // loop over ROI (limit to symmetry in result)
            while ( dh<=mid[0] ) {
              while ( di<=mid[1] ) {
                while ( dj<=mid[2] ) {
                  // array-index of the comparison pixel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // update S_2 or C_2
                  if ( F[idx]==G[jdx] ) {
                    R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)]++;
                  }
                  // update relative distance
                  dj += 1;
                }
                // update/reset relative distance
                di += 1;
                dj  = -mid[2];
              }
              // update/reset relative distance
              dh += 1;
              di  = -mid[1];
            }
          }
        }
      }
    }

    // apply point symmetry to the result
    for ( h=-mid[0] ; h<=mid[0] ; h++ )
      for ( i=-mid[1] ; i<=mid[1] ; i++ )
        for ( j=-mid[2] ; j<=+mid[2] ; j++ )
          R[M3(+h+mid[0],+i+mid[1],+j+mid[2],RN)] = R[M3(-h+mid[0],-i+mid[1],-j+mid[2],RN)];


    return 0;
  }

  error_string = "Unknown data-type combination of 'f' and 'g'";
  return 1;

}

/*  ========================================================================  */

extern int image_correlate_mask(
  Matrix* f, Matrix* g, Matrix* fmask, Matrix* gmask, Matrix* roi, Matrix* norm, int periodic
)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx;
  int bnd[] = {0,0,0};
  int mid[] = {0,0,0};
  int FN[3],RN[3];

  // alias variables to condense the code below
  int* FM = fmask->data_b;
  int* GM = gmask->data_b;
  int* N  = norm ->data_i;

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f    ,3,FN ) ) return 1;
  if ( matrix_shape     (roi  ,3,RN ) ) return 1;
  if ( matrix_dtype     (fmask,1,0,0) ) return 1;
  if ( matrix_dtype     (gmask,1,0,0) ) return 1;
  if ( matrix_consistent(f    ,g    ) ) return 1;
  if ( matrix_consistent(fmask,f    ) ) return 1;
  if ( matrix_consistent(fmask,gmask) ) return 1;
  if ( matrix_consistent(roi  ,norm ) ) return 1;
  if ( image_mid        (roi,mid    ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image "bnd = {0,0,0}", i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------
  // set normalization factor
  // ------------------------

  // initiate the normalization
  for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
    N[i] = 0;

  // calculate normalization factor: account for masks
  for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
    for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
      for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
        // continue only if the voxel is not masked: loop over the neighborhood
        if ( !FM[M3(h,i,j,FN)] ) {
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison voxel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // update normalization,
                // only if the comparison voxel is not masked
                if ( !GM[jdx] ) {
                  N[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)]++;
                }
              }
            }
          }
        }
      }
    }
  }

  // make sure that normalization is at least 1
  // this avoids zero division when applying the normalization
  for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
    if ( N[i]==0 )
      N[i] = 1;

  // ------------------------------------------------
  // 2 floating-point images: conditional convolution
  // ------------------------------------------------

  if ( f->data_d!=NULL && g->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    double* F = f  ->data_d;
    double* G = g  ->data_d;
    double* R = roi->data_d;

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0.0;

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j), only if the mask are zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood:
          // only if the voxel is not masked
          if ( !FM[idx] ) {
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // update the correlation:
                  // only if the comparison voxel is not masked
                  if ( !GM[jdx] ) {
                    R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] += F[idx]*G[jdx];
                  }
                }
              }
            }
          }
        }
      }
    }

    return 0;

  }

  // ------------------------------------------------------------------
  // 2 integer images: conditional
  // 2-point probability (boolean) / 2-point cluster function (integer)
  // ------------------------------------------------------------------

  if ( ( f->data_i!=NULL && g->data_i!=NULL ) || ( f->data_b!=NULL && g->data_b!=NULL ) ) {

    // check data-type
    if ( matrix_dtype(roi,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f  ->data_b;
    int* G = g  ->data_b;
    int* R = roi->data_i;
    if ( f->data_i!=NULL && g->data_i!=NULL ) {
      F = f->data_i;
      G = g->data_i;
    }

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0;

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j), only if the mask are zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood:
          // only if the voxel is not masked, and non-zero
          if ( F[idx] && !FM[idx] ) {
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // update the correlation:
                  // only if the comparison voxel is not masked
                  if ( !GM[jdx] ) {
                    if ( F[idx]==G[jdx] ) {
                      R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)]++;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return 0;

  }

  error_string = "Unknown data-type combination of 'f' and 'g'";
  return 1;

}

/*  ========================================================================  */

extern int image_correlate_weight(
  Matrix* weight, Matrix* f, Matrix* roi, double* norm, int periodic
)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx;
  int bnd[] = {0,0,0};
  int mid[] = {0,0,0};
  int FN[3],RN[3];

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f     ,3,FN ) ) return 1;
  if ( matrix_shape     (roi   ,3,RN ) ) return 1;
  if ( matrix_consistent(weight,f    ) ) return 1;
  if ( image_mid        (roi   ,mid  ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image "bnd = {0,0,0}", i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------------
  // boolean weight + boolean image
  // ------------------------------

  if ( weight->data_b!=NULL && f->data_b!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f     ->data_b;
    int* W = weight->data_b;
    int* R = roi   ->data_i;

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0;

    // initialize normalization
    int N = 0;

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood
          if ( W[idx] ) {
            // calculate normalization
            N++;
            // calculate correlation
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // calculate correlation
                  R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] += F[jdx];
                }
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    if ( N==0 )
      N = 1;
    // set normalization as pointer
    *norm = (double)N;

    return 0;
  }

  // -------------------------------------
  // boolean weight + floating-point image
  // -------------------------------------

  if ( weight->data_b!=NULL && f->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    int*    W = weight->data_b;
    double* F = f     ->data_d;
    double* R = roi   ->data_d;

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0.0;

    // initialize normalization
    int N = 0;

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood
          if ( W[idx] ) {
            // calculate normalization
            N++;
            // calculate correlation
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // calculate correlation
                  R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] += F[jdx];
                }
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    if ( N==0 )
      N = 1;
    // set normalization as pointer
    *norm = (double)N;

    return 0;

  }

  // --------------------------------------------
  // floating-point weight + floating-point image
  // --------------------------------------------

  if ( weight->data_d!=NULL && f->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi,0,0,1) ) return 1;

    // alias variables to condense code
    double* W = weight->data_d;
    double* F = f     ->data_d;
    double* R = roi   ->data_d;

    // zero-initialize result
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
      R[i] = 0.0;

    // initialize normalization
    double N = 0.0;

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // calculate normalization
          N += W[idx];
          // calculate correlation
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison voxel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // calculate correlation
                R[M3(dh+mid[0],di+mid[1],dj+mid[2],RN)] += W[idx]*F[jdx];
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    if ( N==0.0 )
      N = 1.0;
    // set normalization as pointer
    *norm = N;

    return 0;

  }

  error_string = "Unknown data-type combination of 'W' and 'I'";
  return 1;

}


/*  ========================================================================  */

extern int image_correlate_weight_mask(
  Matrix* weight, Matrix* f, Matrix* mask, Matrix* roi, Matrix* norm, int periodic
)
{

  // local variables
  int h,i,j,dh,di,dj,idx,jdx,hdx;
  int bnd[] = {0,0,0};
  int mid[] = {0,0,0};
  int FN[3],RN[3];

  // alias variables to condense the code below
  int* FM = mask->data_b;

  // --------------
  // initialization
  // --------------

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape     (f     ,3,FN ) ) return 1;
  if ( matrix_shape     (roi   ,3,RN ) ) return 1;
  if ( matrix_consistent(weight,f    ) ) return 1;
  if ( matrix_consistent(mask  ,f    ) ) return 1;
  if ( image_mid        (roi   ,mid  ) ) return 1;

  // set band-to-skip if the image is not periodic
  // for a periodic image "bnd = {0,0,0}", i.e. not band is skipped
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // ------------------------------
  // boolean weight + boolean image
  // ------------------------------

  if ( weight->data_b!=NULL && f->data_b!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi ,0,1,0) ) return 1;
    if ( matrix_dtype(norm,0,1,0) ) return 1;

    // alias variables to condense code
    int* F = f     ->data_b;
    int* W = weight->data_b;
    int* R = roi   ->data_i;
    int* N = norm  ->data_i;

    // zero-initialize result and normalization
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      R[i] = 0;
      N[i] = 0;
    }

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood
          // only if the weight is non-zero
          if ( W[idx] ) {
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // calculate correlation
                  // only if the comparison voxel is zero
                  if ( !FM[jdx] ) {
                    hdx     = M3(dh+mid[0],di+mid[1],dj+mid[2],RN);
                    R[hdx] += F[jdx];
                    N[hdx]++;
                  }
                }
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      if ( N[i]==0 ) {
        N[i] = 1;
      }
    }

    return 0;

  }

  // -------------------------------------
  // boolean weight + floating-point image
  // -------------------------------------

  if ( weight->data_b!=NULL && f->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi ,0,0,1) ) return 1;
    if ( matrix_dtype(norm,0,1,0) ) return 1;

    // alias variables to condense code
    double* F = f     ->data_d;
    int*    W = weight->data_b;
    double* R = roi   ->data_d;
    int*    N = norm  ->data_i;

    // zero-initialize result and normalization
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      R[i] = 0.0;
      N[i] = 0;
    }

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood:
          // only for non-zero weight
          if ( W[idx] ) {
            for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
              for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
                for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                  // array-index of the comparison voxel
                  if ( !periodic )
                    jdx = M3 (h+dh,i+di,j+dj,FN);
                  else
                    jdx = M3P(h+dh,i+di,j+dj,FN);
                  // calculate correlation:
                  // only if the comparison voxel is zero
                  if ( !FM[jdx] ) {
                    hdx     = M3(dh+mid[0],di+mid[1],dj+mid[2],RN);
                    R[hdx] += F[jdx];
                    N[hdx]++;
                  }
                }
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      if ( N[i]==0 ) {
        N[i] = 1;
      }
    }

    return 0;

  }

  // --------------------------------------------
  // floating-point weight + floating-point image
  // --------------------------------------------

  if ( weight->data_d!=NULL && f->data_d!=NULL ) {

    // check data-type
    if ( matrix_dtype(roi ,0,0,1) ) return 1;
    if ( matrix_dtype(norm,0,0,1) ) return 1;

    // alias variables to condense code
    double* F = f     ->data_d;
    double* W = weight->data_d;
    double* R = roi   ->data_d;
    double* N = norm  ->data_d;

    // zero-initialize result and normalization
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      R[i] = 0.0;
      N[i] = 0.0;
    }

    // calculate result:
    // (a ) loop over the voxels in all directions
    // (b*) loop over the neighborhood around (h,i,j),
    //      only if the weight is non-zero
    for ( h=bnd[0] ; h<(FN[0]-bnd[0]) ; h++ ) {
      for ( i=bnd[1] ; i<(FN[1]-bnd[1]) ; i++ ) {
        for ( j=bnd[2] ; j<(FN[2]-bnd[2]) ; j++ ) {
          // array-index of the voxel
          idx = M3(h,i,j,FN);
          // loop over the neighborhood
          for ( dh=-mid[0] ; dh<=mid[0] ; dh++ ) {
            for ( di=-mid[1] ; di<=mid[1] ; di++ ) {
              for ( dj=-mid[2] ; dj<=mid[2] ; dj++ ) {
                // array-index of the comparison voxel
                if ( !periodic )
                  jdx = M3 (h+dh,i+di,j+dj,FN);
                else
                  jdx = M3P(h+dh,i+di,j+dj,FN);
                // calculate the correlation:
                // only if the comparison voxel is zero
                if ( !FM[jdx] ) {
                  hdx     = M3(dh+mid[0],di+mid[1],dj+mid[2],RN);
                  N[hdx] += W[idx];
                  R[hdx] += W[idx]*F[jdx];
                }
              }
            }
          }
        }
      }
    }

    // make sure that the normalization is non-zero, to avoid zero-division
    for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
      if ( N[i]==0.0 ) {
        N[i] = 1.0;
      }
    }

    return 0;

  }

  error_string = "Unknown data-type combination of 'W' and 'I'";
  return 1;

}

/*  ========================================================================  */

extern int image_linealpath_stamp_points(
  int nd, int* shape, int* npoints, int* points
)
{

  int* RN     = shape;    // shape of the ROI
  int  mid[]  = {0,0,0};  // index of the mid-point of the ROI
  int  ipoint = 0;
  int  store  = 0;
  int  i,hr,ir,jr,ind;

  // storage switch: proceed with storage if "points" have been allocated
  if ( points!=NULL )
    store = 1;

  // check the shape: must be odd
  for ( i=0 ; i<nd ; i++ ) {
    if ( !(shape[i]%2) && shape[i]!=0 ) {
      error_string = "ROI must be odd sized";
      return 1;
    }
  }

  // 1-D: trivial assign end points
  // ------------------------------

  if ( nd==1 ){
    *npoints = 1;
    if ( store )
      points[0] = +(RN[0]-1)/2;
    return 0;
  }

  // initiate
  // --------

  // calculate the middle of the ROI
  for ( i=0 ; i<nd ; i++ )
    if ( RN[i]>1 ) {
      if ( nd==2 )
        mid[i+1] = (RN[i]-1)/2;
      else if ( nd==3 )
        mid[i]   = (RN[i]-1)/2;
    }

  // xy-plane (only plane in 2-D)
  // ----------------------------

  // columns (max rows)
  for ( jr=-mid[2] ; jr<=mid[2] ; jr++ ) {

    // extract/update index
    ind = ipoint*nd+(nd-2);
    ipoint++;
    // store
    if ( store ) {
      points[ind  ] = mid[1];
      points[ind+1] = jr;
    }

  }

  // rows (min/max columns)
  for ( ir=0 ; ir<mid[1] ; ir++ ) {

    // (a) positive side
    // extract/update index
    ind = ipoint*nd+(nd-2);
    ipoint++;
    // store
    if ( store ) {
      points[ind  ] = ir;
      points[ind+1] = mid[2];
    }

    // (b) negative side (skip for the first row)
    if ( ir ) {
      // extract/update index
      ind = ipoint*nd+(nd-2);
      ipoint++;
      // store
      if ( store ) {
        points[ind  ] = ir;
        points[ind+1] = -mid[2];
      }
    }

  }

  // 2-D: exit
  if ( nd==2 )
    *npoints = ipoint;
    return 0;

  // other directions (3-D only)
  // ---------------------------

  // 3-D add zero z-direction
  if ( store )
    if ( nd==3 )
      for (i=0; i<ipoint; i++ )
        points[ipoint*nd] = 0;

  // quasi 3-D: exit
  if ( RN[0]==1 ) {
    *npoints = ipoint;
    return 0;
  }

  // -- all planes between the middle (xy-) plane and the outer plane --

  // loop over planes
  for ( hr=1 ; hr<mid[0] ; hr++ ) {

    // columns (min/max rows)
    for ( jr=-mid[2] ; jr<=mid[2] ; jr++ ) {
      if ( store ) {
        points[ipoint*3+0]=hr; points[ipoint*3+1]=+mid[1]; points[ipoint*3+2]=jr; ipoint++;
        points[ipoint*3+0]=hr; points[ipoint*3+1]=-mid[1]; points[ipoint*3+2]=jr; ipoint++;
      }
      else {
        ipoint += 2;
      }
    }
    // loop over rows
    for ( ir=-mid[1]+1 ; ir<mid[1] ; ir++ ) {
      if ( store ) {
        points[ipoint*3+0]=hr; points[ipoint*3+1]=ir; points[ipoint*3+2]=+mid[2]; ipoint++;
        points[ipoint*3+0]=hr; points[ipoint*3+1]=ir; points[ipoint*3+2]=-mid[2]; ipoint++;
      }
      else {
        ipoint += 2;
      }
    }
  }

  // -- outer plane --

  // loop over all rows and columns
  for ( ir=-mid[1] ; ir<=mid[1] ; ir++ ) {
    for ( jr=-mid[2] ; jr<=mid[2] ; jr++ ) {
      if ( store ) {
        points[ipoint*3+0]=mid[0]; points[ipoint*3+1]=ir; points[ipoint*3+2]=jr; ipoint++;
      }
      else {
        ipoint++;
      }
    }
  }

  *npoints = ipoint;

  return 0;

}

/*  ========================================================================  */

extern int image_linealpath_stamp_paths(Stamp* st, int* points, int algorithm)
{

  int  nd     = st->nd;
  int  npath  = st->npath;
  int* ia     = st->ia;
  int* ja     = st->ja;
  int  store  = 0;
  int* x0 = (int*)malloc(nd*sizeof(int));
  int* x1 = (int*)malloc(nd*sizeof(int));
  int  i,ipoint;
  int  shape[1];

  // set storage switch
  if ( st->ja!=NULL )
    store = 1;

  // initiate start point -> center of the ROI
  for ( i=0 ; i<nd ; i++ )
    x0[i] = 0;

  // store starting row
  ia[0] = 0;

  // loop over all end-points -> store pixel path for each point
  for ( ipoint=0 ; ipoint<npath; ipoint++ )
  {
    // store the "end-point"
    for ( i=0 ; i<nd ; i++ )
      x1[i] = points[ipoint*nd+i];
    // update the length of the path, by calculating the voxel path
    if ( !store ) {
      image_path(x0,x1,algorithm,nd,shape,NULL);
      ia[ipoint+1] = ia[ipoint]+shape[0];
    }
    else {
      image_path(x0,x1,algorithm,nd,shape,&ja[ia[ipoint]*nd]);
    }
  }

  free(x0);
  free(x1);

  return 0;

}

/*  ========================================================================  */

extern int image_linealpath(
  Matrix* f, Stamp* st, Matrix* roi, int* norm, int periodic
)
{
  // alias header variables
  int* F     = f  ->data_b;
  int* R     = roi->data_i;
  int  nd    = st ->nd;
  int  npath = st ->npath;
  // local variables
  int h,i,j,ii,idx;
  int ipath,jpath;
  int bnd[] = {0,0,0};
  int mid[] = {0,0,0};
  int ind[] = {0,0,0};
  int N     = 0;
  int FN[3],RN[3];

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_dtype(f  ,1,0,0) ) return 1;
  if ( matrix_dtype(roi,0,1,0) ) return 1;
  if ( matrix_shape(f  ,3,FN ) ) return 1;
  if ( matrix_shape(roi,3,RN ) ) return 1;
  if ( image_mid   (roi,mid  ) ) return 1;

  // set band-to-skip if the image is not periodic
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // initialize
  for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ )
    R[i] = 0;

  // loop over voxels (in all directions)
  for ( h=bnd[0] ; h<FN[0]-bnd[0] ; h++ ) {
    for ( i=bnd[1] ; i<FN[1]-bnd[1] ; i++ ) {
      for ( j=bnd[2] ; j<FN[2]-bnd[2] ; j++ ) {
        // update normalization factor
        N++;
        // proceed only for non-zero voxel
        if ( F[M3(h,i,j,FN)] ) {
          // loop over paths
          for ( ipath=0 ; ipath<npath ; ipath++ ) {
            // loop over voxels int the path
            for ( jpath=st->ia[ipath] ; jpath<st->ia[ipath+1] ; jpath++ ) {
            // get the current voxel-shift, by reading "ja"
            for ( ii=0 ; ii<nd ; ii++ )
              ind[ii+(3-nd)] = st->ja[jpath*nd+ii];
            // array-index
            if ( !periodic )
              idx = M3 (h+ind[0],i+ind[1],j+ind[2],FN);
            else
              idx = M3P(h+ind[0],i+ind[1],j+ind[2],FN);
            // break for zero voxel (continue to the next path)
            if ( !F[idx] )
              break;
            // update the lineal path function
            // (only if the print switch is true for this voxel)
            if ( st->stat[jpath] )
              R[M3(ind[0]+mid[0],ind[1]+mid[1],ind[2]+mid[2],RN)]++;
            }
          }
        }
      }
    }
  }

  // fill ROI using point symmetry
  for ( h=-mid[0] ; h<=mid[0] ; h++ ) {
    for ( i=-mid[1] ; i<=mid[1] ; i++ ) {
      for ( j=-mid[2] ; j<=+mid[2] ; j++ ) {
        R[M3(+h+mid[0],+i+mid[1],+j+mid[2],RN)] = R[M3(-h+mid[0],-i+mid[1],-j+mid[2],RN)];
      }
    }
  }

  // store normalization
  *norm = N;

  return 0;
}

/*  ========================================================================  */

extern int image_correlate_weight_collapse(
  Matrix* f, Matrix* cluster, Matrix* center, Matrix* mask, Stamp* st,
  Matrix* roi, Matrix* norm, int periodic
)
{
  // alias header variables
  double* F     = f      ->data_d;
  double* R     = roi    ->data_d;
  int*    N     = norm   ->data_i;
  int*    Lab   = cluster->data_i;
  int*    Cen   = center ->data_i;
  int*    FM    = mask   ->data_b;
  int     nd    = st     ->nd;
  int     npath = st     ->npath;
  // local variables
  int bnd[] = {0,0,0};
  int mid[] = {0,0,0};
  int ind[] = {0,0,0};
  int jnd[] = {0,0,0};
  int h,i,j,ii,idx,rdx;
  int ipath,jpath,kpath;
  int label;
  int FN[3],RN[3];

  // quasi-3D shape / consistency / middle of the ROI
  if ( matrix_shape(f      ,3,FN ) ) return 1;
  if ( matrix_shape(roi    ,3,RN ) ) return 1;
  if ( matrix_dtype(f      ,0,0,1) ) return 1;
  if ( matrix_dtype(roi    ,0,0,1) ) return 1;
  if ( matrix_dtype(norm   ,0,1,0) ) return 1;
  if ( matrix_dtype(mask   ,1,0,0) ) return 1;
  if ( matrix_dtype(center ,0,1,0) ) return 1;
  if ( matrix_dtype(cluster,0,1,0) ) return 1;
  if ( image_mid   (roi    ,mid  ) ) return 1;

  // set band-to-skip if the image is not periodic
  if ( !periodic )
    for ( i=0 ; i<3 ; i++ )
      bnd[i] = mid[i];

  // initialize: result and normalization
  for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
    R[i] = 0.0;
    N[i] = 0;
  }

  // loop over voxels (in all directions)
  for ( h=bnd[0] ; h<FN[0]-bnd[0] ; h++ ) {
    for ( i=bnd[1] ; i<FN[1]-bnd[1] ; i++ ) {
      for ( j=bnd[2] ; j<FN[2]-bnd[2] ; j++ ) {
        // store the label
        label = Cen[M3(h,i,j,FN)];
        // continue at the cluster centers
        if ( label ) {
          // loop over paths
          for ( ipath=0 ; ipath<npath ; ipath++ ) {
            // loop over the voxel path until the end of the cluster
            for ( jpath=st->ia[ipath] ; jpath<st->ia[ipath+1] ; jpath++ ) {
              // read the shift-index
              for ( ii=0 ; ii<nd ; ii++ ) {
                ind[ii+(3-nd)] = st->ja[jpath*nd+ii];
              }
              // relative shift-index -> absolute matrix index -> array-index
              if ( !periodic )
                idx = M3 (h+ind[0],i+ind[1],j+ind[2],FN);
              else
                idx = M3P(h+ind[0],i+ind[1],j+ind[2],FN);
              // stop this loop at the end of the cluster
              if ( Lab[idx]!=label )
                break;
            }
            // initiate a second index to start reading from the beginning
            // if the path is only one pixel: go to the next path
            kpath=st->ia[ipath];
            // continue the loop
            for ( jpath=jpath ; jpath<st->ia[ipath+1] ; jpath++ ) {
              // update parallel index
              kpath++;
              // check index
              if ( kpath >= st->ia[ipath+1] )
                break;
              // read the shift-index
              for ( ii=0 ; ii<nd ; ii++ ) {
                ind[ii+(3-nd)] = st->ja[jpath*nd+ii];
                jnd[ii+(3-nd)] = st->ja[kpath*nd+ii];
              }
              // relative shift-index -> absolute matrix index -> array-index
              if ( !periodic )
                idx = M3 (h+ind[0],i+ind[1],j+ind[2],FN);
              else
                idx = M3P(h+ind[0],i+ind[1],j+ind[2],FN);
              // update correlation
              if ( !FM[idx] ) {
                rdx = M3(jnd[0]+mid[0],jnd[1]+mid[1],jnd[2]+mid[2],RN);
                N[rdx]++;
                R[rdx] += F[idx];
              }
            }
          }
        }
      }
    }
  }

  // make sure that the normalization is non-zero, to avoid zero-division
  for ( i=0 ; i<(RN[0]*RN[1]*RN[2]) ; i++ ) {
    if ( N[i]==0 ) {
      N[i] = 1;
    }
  }

  return 0;
}
