

#include "core.h"

/* =============================================================================
 * return error string
 * ========================================================================== */

extern char* get_error() {
  return error_string;
}

/* =============================================================================
 * check data-type
 * ========================================================================== */

int matrix_dtype(Matrix* self, int has_bool, int has_int, int has_double)
{

  // check the data-type: boolean
  if      (  has_bool>0   && self->data_b==NULL ) {
    error_string = "Specify 'image->data_b'";
    return 1;
  }
  else if ( !has_bool     && self->data_b!=NULL ) {
    error_string = "'image->data_b' not allowed";
    return 1;
  }

  // check the data-type: integer
  if      (  has_int>0    && self->data_i==NULL ) {
    error_string = "Specify 'image->data_i'";
    return 1;
  }
  else if ( !has_int      && self->data_i!=NULL ) {
    error_string = "'image->data_i' not allowed";
    return 1;
  }

  // check the data-type: floating-point
  if      (  has_double>0 && self->data_d==NULL ) {
    error_string = "Specify 'image->data_d'";
    return 1;
  }
  else if ( !has_double   && self->data_d!=NULL ) {
    error_string = "'image->data_d' not allowed";
    return 1;
  }

  return 0;
}

/* =============================================================================
 * create quasi N-D shape
 * ========================================================================== */

int matrix_shape(Matrix* self, int N, int* shape)
{

  // basic check
  // -----------

  if ( self->shape==NULL ) {
    error_string = "Specify 'image->shape'";
    return 1;
  }

  // match "N" exactly
  // -----------------

  if ( shape==NULL ) {

    if ( self->nd != N ) {
      error_string = "Wrong image size";
      //sprintf(error_string,"Image must be %d (not %d)",N,self->nd);
      return 1;
    }
    else {
      return 0;
    }

  }

  // calculate the quasi-N-D shape, match "N" as upper-bound
  // -------------------------------------------------------

  int i,dN;

  // calculate number of dimensions to add
  dN = N-self->nd;

  // check: number of dimensions must be sufficient
  if ( dN<0 ) {
	error_string = "Wrong image size";
    //sprintf(error_string,"Image must be at least %d (not %d)",N,self->nd);
    return 1;
  }

  // set quasi N-D shape
  for ( i=0 ; i<dN ; i++ )
    shape[i] = 1;
  for ( i=0 ; i<self->nd ; i++ )
    shape[dN+i] = self->shape[i];

  return 0;
}

/* =============================================================================
 * check if 2 matrices are consistent
 * ========================================================================== */

int matrix_consistent(Matrix* self, Matrix* other){

  int i;

  // check if the number of dimensions is consistent
  if ( self->nd != other->nd ) {
    error_string = "'self' and 'other' must have the same dimension";
    return 1;
  }

  // check if the shape is consistent
  for ( i=0 ; i<self->nd ; i++ ) {
    if ( self->shape[i]!=other->shape[i] ) {
      error_string = "'self' and 'other' must have the same shape";
      return 1;
    }
  }

  return 0;

}