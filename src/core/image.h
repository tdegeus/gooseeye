
#ifndef IMAGE_H
#define IMAGE_H

#include "core.h"

/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate the index (3-D) of the middle of a shape. This function raises an
error if the image is not odd-shaped.

:arguments:

  **self** (``Matrix*``), *in*
    The matrix.

  **midpoint** (``int = int[3]``), *out*
    The matrix-index of the mid-point.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
 */
int image_mid(
  Matrix* self, int* mid
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate a the voxel-path connecting two voxels

:arguments:

  **i0,i1** (``int* = int[nd]``), *in*
    The indices of the beginning and end points.

  **algorithm** (``int = 0 | 1 | 2``), *in*
    The algorithm to use. The following algorithms are available:

    * ``0``: "bresenham"
    * ``1``: "actual"
    * ``2``: "full"

  **nd** (``int``), *in*
    Number of dimensions.

  **N** (``int*``), *out*
    Number of voxels in the path. NB: this is a point to a single integer. It
    is a pointer to be able to use it as output.

  **path** (``int* = int[N*nd]``), *out*
    The voxel path. If ``path = NULL`` the number of voxels in the path, ``N``,
    is calculated. In this mode nothing is stored to the path.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
 */
extern EXPORT int image_path(
  int* i0, int* i1, int algorithm, int nd, int* N, int* path
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Create black-white image with "black" circles at specified positions in an
otherwise "white" background.

:arguments:

  **im** (``Matrix* = Matrix[*,*]``), *out*
    The image as 2-D matrix.

  **shapes** (``Matrix* = Matrix[*,3]``), *in*
    The locations of the circles::

      [
        [ x1 , y1 , r1 ],
        [ x2 , y2 , r2 ],
        ...
        [ xn , yn , rn ],
      ]

  **periodic** (``int = 0 | 1``)
    Signal to take the image not-periodic or periodic.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/
extern EXPORT int image_dummy_circles(
  Matrix* im, Matrix* shapes, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Label clusters of pixels/voxels. The output is an integer image, of the same
shape as the input image, whereby each pixel cluster is assigned a unique non-
zero integer. A zero index is assigned to the background.

:input argument:

  **image** (``Matrix*; data_b``)
    Black-and-white image.

  **structure** (``Matrix*; data_b``)
    Kernel to use to determine the connections.

  **periodic** (``int = 0 | 1``)
    Signal periodicity.

  **min_size** (``int = -1 | >=0``)
    Only include include clusters large than a certain number of pixels. For a
    value of ``-1`` this option is ignored.

:output arguments:

  **label** (``Matrix*; data_i``)
    The labeled clusters.

  **num_features** (``int``), *pointer to number*
    The number of clusters (non-zero indices).

:returns:

  **error** (``int = 0 | !0``)
    The exit code (non-zero in case of error). See :ref:`src/core/get_error`.

\endverbatim
*/
extern EXPORT int image_clusters(
  Matrix* image, Matrix* structure, int periodic, int min_size,
  Matrix* label, int* num_features
);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Label clusters, back-end of :ref:`src/core/image_clusters`.

:input arguments:

  **image** (``Matrix*; data_b``)

  **structure** (``Matrix*; data_b``)

  **periodic** (``int = 0 | 1``)

:output arguments:

  **label** (``Matrix*; data_i``)

  **num_features** (``int*``), *pointer to number*

:returns:

  **error** (``int = 0 | !0``)

\endverbatim
*/
int image_clusters_label(
  Matrix* image, Matrix* structure, int periodic, Matrix* label, int* num_features
);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Update linked labels stored as linked list. Back-end of
:ref:`src/core/image_clusters`.

In this linked-list a set of equivalent labels can be read by:

.. code-block:: c

  i = linked[begin]
  while ( 1 )
  {
    if ( i==begin ) { break; }
    i = linked[i];
  }

:arguments:

  **linked** (``int* = int[N]``), *inout*
    The linked list.

  **a,b** (``int``), *in*
    The label to link together.

:returns:

  **error** (``int = 0 | !0``)

\endverbatim
*/
int image_cluster_link(int* linked, int a, int b);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Calculate size of clusters. Back-end of :ref:`src/core/image_clusters`.

:input arguments:

  **label** (``Matrix*; data_i``)

  **num_features** (``int``)

:output arguments:

  **size_features** (``int* = int[num_features+1]``)

:returns:

  **error** (``int = 0 | !0``)

\endverbatim
*/

int image_clusters_size(Matrix* label, int num_features, int* size_features);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Calculate the center of gravity of each cluster. This sets a single integer
index (equal to the original label) in the center of gravity of each cluster.

:input arguments:

  **image** (``Matrix*; data_b``)
    Black-and-white image.

  **structure** (``Matrix*; data_b``)
    Kernel to use to determine the connections.

  **label** (``Matrix*; data_i``)
    The labeled clusters for the ``image``.

  **num_features** (``int``)
    The number of clusters (non-zero indices).

  **periodic** (``int = 0 | 1``)
    Signal periodicity.

:output arguments:

  **centers** (``Matrix*; data_i``)
    The labeled cluster-centers (only the centers are non-zero).

  **size_features** (``int* = int[num_features+1]``)
    The size of the clusters.

:returns:

  **error** (``int = 0 | !0``)
    The exit code (non-zero in case of error). See :ref:`src/core/get_error`.

\endverbatim
*/
extern EXPORT int image_clusters_center(
  Matrix* image, Matrix* structure, Matrix* label, int num_features, int periodic,
  Matrix* center, int* size_features
);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Centers of all clusters, non-periodic image. Back-end of:
:ref:`src/core/image_clusters_center`.

:input arguments:

  **label** (``Matrix*; data_i``)

  **num_features** (``int``)

:output arguments:

  **centers** (``Matrix*; data_i``)

  **size_features** (``int* = int[num_features+1]``)

:returns:

  **error** (``int = 0 | !0``)

\endverbatim
*/
int image_clusters_center_np(
  Matrix* label, int num_features, Matrix* center, int* size_features
);

/*!
\verbatim embed:rst

Centers of all clusters, non-periodic image. Back-end of:
:ref:`src/core/image_clusters_center`.

:input arguments:

  **image** (``Matrix*; data_b``)

  **structure** (``Matrix*; data_b``)

  **label** (``Matrix*; data_i``)

  **num_features** (``int``)

:output arguments:

  **centers** (``Matrix*; data_i``)

  **size_features** (``int* = int[num_features+1]``)

:returns:

  **error** (``int = 0 | !0``)

\endverbatim
*/
int image_clusters_center_p(
  Matrix* image, Matrix* structure, Matrix* label, int num_features,
  Matrix* center, int* size_features
);

/*  ------------------------------------------------------------------------  */

/*!
\verbatim embed:rst

Dilate clusters, with a different number of iterations per cluster. The dilation
stops when another cluster is encountered. **The clusters are not merged**.

:arguments:

  **label** (``Matrix*; data_i``), *inout*
    The labeled clusters.

  **num_features** (``int``), *in*
    The number of clusters (non-zero indices).

  **iterations** (``int* = int[num_features+1]``), *in*
    The number of iterations for each cluster. For example ``iterations[3]`` are
    applied from cluster ``3``. The value of ``iterations[0]`` is irrelevant for
    this function.

  **kernel** (``Matrix*; data_b``), *in*
    The kernel to use to dilate.

  **periodic** (``int = 0 | 1``), *in*
    Signal periodicity. If ``1`` the dilation is periodically continued across
    the edges of the image.

:returns:

  **error** (``<int> = 0 | 1``)
    See ``core/get_error``.

\endverbatim
*/

extern EXPORT int image_clusters_dilate(
  Matrix* label, int num_features, int* iterations, Matrix* kernel, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Collection of voxel paths.

:fields:

  **ia** (``int* = int[npath+1]``)
    Split ``ja`` per path.

  **ja**  (``int* = int[npath*N*nd]``)
    The indices (e.g. (h,i,j)) in the path.

  **stat** (``int* = int[npath*N]``)
    Signal to include the voxel in the statistics.

  **npath** (``int``)
    Number of voxel paths. NB: the number of voxels in each path is implicitly
    include in ``ia``. For example, the number of voxels in ``ipath``:

    .. code-block:: c

      ia[ipath+1]-ia[ipath]

  **nd** (``int``)
    Number of dimensions of the original image (``nd==2`` is a regular image,
    and ``nd==3`` is a voxel image).

\endverbatim
 */
typedef struct Stamp {
  int*    ia         ;
  int*    ja         ;
  int*    stat       ;
  int     npath      ;
  int     nd         ;
} Stamp;

/*  ========================================================================  */

/*!
\verbatim embed:rst

Perform the following multiplication (also implemented in 1-D and 3-D)::

  RESULT[i,j] = F[i+di,j+dj]*ROI[i+di,j+dj]

:arguments:

  **f,result** (``Matrix*``)
    The input and the result.

  **roi** (``Matrix*``)
    The region-of-interest (kernel with which to multiply).

  **periodic** (``int = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``0`` a band of
    the size of half the ROI is skipped along all edges of the image ``f``.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_roimul(
  Matrix* f, Matrix* roi, Matrix* result, int periodic
);

/*!
\verbatim embed:rst

Perform the following multiplication (also implemented in 1-D and 3-D)::

  RESULT[i,j] = F[i+di,j+dj]*ROI[i+di,j+dj]

whereby only positive results are taken into account.

:arguments:

  **f,result** (``Matrix*``)
    The input and the result.

  **roi** (``Matrix*``)
    The region-of-interest (kernel with which to multiply).

  **periodic** (``int = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``0`` a band of
    the size of half the ROI is skipped along all edges of the image ``f``.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/
extern EXPORT int image_roimul_nonneg(
  Matrix* f, Matrix* roi, Matrix* result, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Correlate two images. Depending on the type of image, the correlation-function
is different.

* Two floating-point images: convolution

  .. math::

    R (\delta \vec{x}) = f (\vec{x}) \star g (\vec{x})

  For the case that :math:`g = f` this corresponds to the auto-correlation
  function.

* Two boolean images: 2-point probability

  .. math::

    S_2 (\vec{x}_1,\vec{x}_2) = P \{ f(\vec{x}_1)=1 , g(\vec{x}_2)=1 \}

* Two integer images: 2-point cluster function

  .. math::

    C_2 (\vec{x}_1,\vec{x}_2) =
    P \{ f(\vec{x}_1) = g(\vec{x}_2) ; f(\vec{x}_1) \neq 0 \}

  To calculate this, each cluster has to be assigned a unique index. Note that
  the background (which is ignored) has a zero-index.

.. warning::

  The normalization is not applied in this function. It has to be applied
  outside this function.

:arguments:

  **f,g** (``Matrix*``), *in*
    The images to correlate.

  **roi** (``Matrix*``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``int*``), *out(init)*
    The normalization factor. It is a single integer, stored as pointer to be
    an output variable.

  **periodic** (``int = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``0`` a band of
    the size of half the ROI is skipped along all edges of the images ``f`` and
    ``g``. To avoid this without using periodicity use
    ``image_correlate_masked``.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_correlate(
  Matrix* f, Matrix* g, Matrix* roi, int* norm, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Correlate two images. Compared to ``image_correlate`` this function has the
option to mask voxels of the input images ``f`` and ``g``. A masked voxel is
excluded from the result, as follows

1. the correlation is not calculated if the considered voxel is masked;
2. the normalization is adjusted as the relevant location is the
   region-of-interest.

The different modes are the same as in ``image_correlate``.

.. warning::

  The normalization is not applied in this function. It has to be applied
  outside this function.

:arguments:

  **f,g** (``Matrix*``), *in*
    The images to correlate.

  **fmask,gmask** (``Matrix*; data_b``), *in*
    The image masks: always boolean.

  **roi** (``Matrix*``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``Matrix*``), *out(init)*
    The normalization.

  **periodic** (``<int> = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``False`` a
    band of the size of half the ROI is skipped along all edges of the images
    ``f`` and ``g``. To avoid skipping any pixels, the images can be
    padded. A mask then has be set for the padded pixels.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_correlate_mask(
  Matrix* f, Matrix* g, Matrix* fmask, Matrix* gmask, Matrix* roi, Matrix* norm, int periodic
);


/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate the weighted average intensity:

.. math::

  R(\delta \vec{x}) = \mathcal{W} \star \mathcal{I}

The normalization is the sum of :math:`\mathcal{W}`. The result is interpreted
as the average intensity around voxels where :math:`\mathcal{W}` is high. This
function has the following operational modes:

* Boolean weight and boolean image.

* Boolean weight and floating-point image.

* Floating-point weight and floating-point image. This takes into account the
  actual value of ``W``, this makes this mode considerably slower than the above
  two modes.

:arguments:

  **W** (``Matrix*``), *in*
    The weight factors.

  **I** (``Matrix*``), *in*
    The image.

  **roi** (``Matrix*``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``double*``), *out(init)*
    The normalization factor. It is a single float, stored as pointer to be
    an output variable.

  **periodic** (``int = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``False`` a
    band of the size of half the ROI is skipped along all edges. To avoid this
    without using periodicity use ``image_correlate_weight_masked``.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_correlate_weight(
  Matrix* W, Matrix* I, Matrix* roi, double* norm, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate the weighted average intensity, masking certain voxels. The
functionality is the same as ``image_correlate_weight`` with the difference that
certain voxels in ``I`` may be masked. The normalization is modified for the
masked voxels.

.. note::

  The mask does not effect the weight at all.

:arguments:

  **W** (``Matrix*``), *in*
    The weight factors.

  **I** (``Matrix*``), *in*
    The image.

  **mask** (``Matrix*; data_b``), *in*
    The mask for the image ``I``.

  **roi** (``Matrix*``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``Matrix*``), *out(init)*
    The normalization factor.

  **periodic** (``<int> = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``False`` a
    band of the size of half the ROI is skipped along all edges. To avoid this,
    pad the image and set a mask for the padded voxels.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_correlate_weight_mask(
  Matrix* weight, Matrix* f, Matrix* mask, Matrix* roi, Matrix* norm, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate the average intensity around clusters, whereby the result is collapsed
to the center of gravity of the clusters.

:arguments:

  **f** (``Matrix*; data_d``), *in*
    The image.

  **cluster** (``Matrix*; data_i``), *in*
    The clusters, with each cluster labeled with a unique non-zero index.

  **center** (``Matrix*; data_i``), *in*
    The center of each cluster, with only the label-index in the center of each
    cluster, the rest of the image in zero.

  **mask** (``Matrix*; data_b``), *in*
    The mask for the image ``f``.

  **stamp** (``stat.Stamp*``), *in*
    The voxel stamp along which the result is collapsed to the center.

  **roi** (``Matrix*; data_d``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``Matrix*; data_i``), *out(init)*
    The normalization factor for each voxel in the region-of-interest.

  **periodic** (``<int> = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``False`` a
    band of the size of half the ROI is skipped along all edges.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_correlate_weight_collapse(
  Matrix* f, Matrix* cluster, Matrix* center, Matrix* mask, Stamp* st,
  Matrix* roi, Matrix* norm, int periodic
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Set the end-points in that form the pixel-path-stamp for the lineal path
function.

:arguments:

  **nd** (``int``), *in*
    Number of dimensions.

  **shape** (``int* = int[nd]``), *in*
    Shape of the region-of-interest.

  **npoints** (``int*``), *out*
    Number of end-points/pixels/voxels in the stamp. NB: this is a single
    integer stored as pointer to act as output.

  **points** (``int* = int[nd*npoints]``). *out*
    The end-points/pixels/voxels.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_linealpath_stamp_points(
  int nd, int* shape, int* npoints, int* points
);

/*  ========================================================================  */

/*!
\verbatim embed:rst

Convert a set of end-points to a pixel-path-stamp.

:arguments:

  **stamp** (``stat.Stamp*``), *inout*
    The voxel stamp.

  **points** (``int* = int[nd*npoints]``). *out*
    The end-points/pixels/voxels.

  **algorithm** (``int = 0 | 1 | 2``), *in*
    The algorithm to use. The following algorithms are available:

    * ``0``: "bresenham"
    * ``1``: "actual"
    * ``2``: "full"

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_linealpath_stamp_paths(
  Stamp* stamp, int* points, int algorithm
);


/*  ========================================================================  */

/*!
\verbatim embed:rst

Calculate the lineal path function.

:arguments:

  **I** (``Matrix*; data_b``), *in*
    The image.

  **stamp** (``stat.Stamp*``), *inout*
    The voxel stamp.

  **roi** (``Matrix*``), *out(init)*
    The region-of-interest for which the result is calculated.

  **norm** (``int*``), *out(init)*
    The normalization factor. It is a single integer, stored as pointer to be
    an output variable.

  **periodic** (``<int> = 0 | 1``)
    Signal if the image must be taken periodic or not. If set to ``False`` a
    band of the size of half the ROI is skipped along all edges.

:returns:

  **error** (``int = 0 | !0``)
    The exit code. If an error is encountered the function returns a non-zero
    value. See :ref:`src/core/get_error`.

\endverbatim
*/

extern EXPORT int image_linealpath(
  Matrix* f, Stamp* stamp, Matrix* roi, int* norm, int periodic
);

/*  ========================================================================  */

#endif
