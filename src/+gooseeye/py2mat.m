function self = py2mat(self)
%PY2MAT - convert NumPy-array to matrix
%
%   mat = PY2MAT(mat)
%
%     Convert NumPy-array to MATLAB-matrix. The type of this matrix depends on
%     the type of the NumPy-array.

pyshape = cell(self.shape);
n       = length(pyshape);
shape   = zeros(1,n);
for i = 1:n
    shape(i) = pyshape{i};
end

if     self.dtype == py.numpy.float64; data = double(py.array.array('d',py.numpy.nditer(self                       ,pyargs('order','F'))));
elseif self.dtype == py.numpy.float32; data = single(py.array.array('d',py.numpy.nditer(self                       ,pyargs('order','F'))));
elseif self.dtype == py.numpy.int64;   data = int64 (py.array.array('i',py.numpy.nditer(self                       ,pyargs('order','F'))));
elseif self.dtype == py.numpy.int32;   data = int32 (py.array.array('i',py.numpy.nditer(self                       ,pyargs('order','F'))));
else;                                  data = int32 (py.array.array('i',py.numpy.nditer(self.astype(py.numpy.int32),pyargs('order','F'))));
end

self = reshape(data,shape);

end