
function [C,varargout] = linealpath(image,roi,periodic,normalize,algorithm,directions)
%LINEALPATH - lineal path function
%
%   C        = linealpath(image,roi)
%   C        = linealpath(image,roi,periodic,normalize,algorithm,directions)
%   [C,norm] = linealpath(image,roi,periodic,false    ,algorithm,directions)

if nargin < 3; periodic   = py.False           ; else periodic    = py.bool(periodic)  ; end
if nargin < 4; normalize  = py.True            ; else normalize   = py.bool(normalize) ; end
if nargin < 5; algorithm  = py.str('bresenham'); else algorithm   = py.str(algorithm)  ; end
if nargin < 6; directions = py.str('all')      ; else directions  = py.str(directions) ; end

C = py.gooseeye.image.linealpath(gooseeye.mat2py(image),gooseeye.mat2py(roi),periodic,normalize,algorithm,directions);

if normalize == py.False; out = cell(C); C = out{1}; normalize = out{2}; end

C = gooseeye.py2mat(C);

if nargout == 2;
  if normalize == py.False
    varargout{1} = normalize;
  else
    error('Only defined if "normalize == false"');
  end
end

end