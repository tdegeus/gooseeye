
function [C,varargout] = correlate_weight(weights,image,roi,periodic,normalize,pad,mask)
%CORRELATE_WEIGHT - weighted correlation
%
%   C        = correlate_weight(weights,image,roi)
%   C        = correlate_weight(weights,image,roi,periodic,normalize,pad,mask)
%   [C,norm] = correlate_weight(weights,image,roi,periodic,false    ,pad,mask)

if nargin < 4; periodic  = py.False ; else periodic  = py.bool(periodic)     ; end
if nargin < 5; normalize = py.True  ; else normalize = py.bool(normalize)    ; end
if nargin < 6; pad       = py.False ; else pad       = py.bool(pad)          ; end
if nargin < 7; mask      = py.None  ; else mask      = gooseeye.mat2py(mask) ; end

C = py.gooseeye.image.correlate_weight(gooseeye.mat2py(weights),gooseeye.mat2py(image),gooseeye.mat2py(roi),periodic,normalize,pad,mask);

if normalize == py.False; dual = true; out = cell(C); C = out{1}; normalize = out{2}; end

C = gooseeye.py2mat(C);

if nargout == 2;
  if dual
    varargout{1} = normalize;
  else
    error('Only defined if "normalize == false"');
  end
end

end