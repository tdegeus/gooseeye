
function [im,varargout] = dummy_circles(shape,N,random,periodic,return_shapes,radius,shapes)
%DUMMY_CIRCLES - create a dummy images as a grid of circles
%
%   im          = dummy_circles(shape)
%   [im,shapes] = dummy_circles(shape,N,random,periodic,return_shapes,radius,shapes)

if nargin < 2; N             = gooseeye.mat2py([3,3],'int32') ; else N             = gooseeye.mat2py(N,'int32') ; end
if nargin < 3; random        = py.False                       ; else random        = py.bool(random)            ; end
if nargin < 4; periodic      = py.True                        ; else periodic      = py.bool(periodic)          ; end
if nargin < 5; return_shapes = py.True                        ; else return_shapes = py.bool(return_shapes)     ; end
if nargin < 6; radius        = py.None                        ; else radius        = py.float(radius)           ; end
if nargin < 7; shapes        = py.None                        ; else shapes        = gooseeye.mat2py(shapes)    ; end

im = py.gooseeye.image.dummy_circles(gooseeye.mat2py(shape,'int32'),N,random,periodic,radius,shapes,return_shapes);

if return_shapes == py.True; out = cell(im); im = out{1}; shapes = out{2}; end

im = gooseeye.py2mat(im);

if nargout == 2;
  if return_shapes == py.True
    varargout{1} = shapes;
  else
    error('Only defined if "return_shapes == false"');
  end
end

end