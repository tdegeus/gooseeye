
function [C,varargout] = correlate_weight_collapse(image,label,center,roi,periodic,normalize,pad,mask,algorithm,directions)
%CORRELATE_WEIGHT_COLLAPSE - weighted correlation, collapsed to one point
%
%   C        = correlate_weight_collapse(image,label,center,roi)
%   C        = correlate_weight_collapse(image,label,center,roi,periodic,normalize,pad,mask,algorithm,directions)
%   [C,norm] = correlate_weight_collapse(image,label,center,roi,periodic,false    ,pad,mask,algorithm,directions)

if nargin <  5; periodic   = py.False           ; else periodic    = py.bool(periodic)     ; end
if nargin <  6; normalize  = py.True            ; else normalize   = py.bool(normalize)    ; end
if nargin <  7; pad        = py.False           ; else pad         = py.bool(pad)          ; end
if nargin <  8; mask       = py.None            ; else mask        = gooseeye.mat2py(mask) ; end
if nargin <  9; algorithm  = py.str('bresenham'); else algorithm   = py.str(algorithm)     ; end
if nargin < 10; directions = py.str('all')      ; else directions  = py.str(directions)    ; end

C = py.gooseeye.image.correlate_weight_collapse(gooseeye.mat2py(image),gooseeye.mat2py(label),gooseeye.mat2py(center),gooseeye.mat2py(roi),periodic,normalize,pad,mask,algorithm,directions);

if normalize == py.False; dual = true; out = cell(C); C = out{1}; normalize = out{2}; end

C = gooseeye.py2mat(C);

if nargout == 2;
  if dual
    varargout{1} = normalize;
  else
    error('Only defined if "normalize == false"');
  end
end

end