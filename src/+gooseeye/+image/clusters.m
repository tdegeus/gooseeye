
function [label,num_features,varargout] = clusters(image,periodic,return_centers,min_size,structure)
%CLUSTERS - label clusters in an image
%
%   [label,num_features]                      = clusters(image)
%   [label,num_features,center,size_features] = clusters(image,periodic,return_centers,min_size,structure)

if nargin < 2; periodic       = py.False ; else periodic       = py.bool(periodic)                 ; end
if nargin < 3; return_centers = py.False ; else return_centers = py.bool(return_centers)           ; end
if nargin < 4; min_size       = py.False ; else min_size       = py.int(min_size)                  ; end
if nargin < 5; structure      = py.None  ; else structure      = gooseeye.mat2py(structure,'bool') ; end

out = py.gooseeye.image.clusters(gooseeye.mat2py(image,'bool'),periodic,return_centers,min_size,structure);
out = cell(out);

label        = gooseeye.py2mat(out{1});
num_features = out{2};

if nargout == 3;
  if return_centers == py.True
    varargout{1} = gooseeye.py2mat(out{3});
  else
    error('Only defined if "return_centers == true"');
  end
end

if nargout == 4;
  if return_centers == py.True
    varargout{1} = gooseeye.py2mat(out{3});
    varargout{2} = gooseeye.py2mat(out{4});
  else
    error('Only defined if "return_centers == true"');
  end
end

end