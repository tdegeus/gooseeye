
function [C,varargout] = correlate(f,g,roi,periodic,normalize,pad,fmask,gmask)
%CORRELATE - correlate two images
%
%   C        = correlate(f,g,roi)
%   C        = correlate(f,g,roi,periodic,normalize,pad,fmask,gmask)
%   [C,norm] = correlate(f,g,roi,periodic,false    ,pad,fmask,gmask)

if nargin < 4; periodic  = py.False ; else periodic  = py.bool(periodic)     ; end
if nargin < 5; normalize = py.True  ; else normalize = py.bool(normalize)    ; end
if nargin < 6; pad       = py.False ; else pad       = py.bool(pad)          ; end
if nargin < 7; fmask     = py.None  ; else fmask     = gooseeye.mat2py(fmask); end
if nargin < 8; gmask     = py.None  ; else gmask     = gooseeye.mat2py(gmask); end

C = py.gooseeye.image.correlate(gooseeye.mat2py(f),gooseeye.mat2py(g),roi,periodic,normalize,pad,fmask,gmask);

if normalize == py.False; dual = true; out = cell(C); C = out{1}; normalize = out{2}; end

C = gooseeye.py2mat(C);

if nargout == 2;
  if dual
    varargout{1} = normalize;
  else
    error('Only defined if "normalize == false"');
  end
end

end