function self = mat2py(self,dtype)
%MAT2PY - convert matrix to NumPy-array
%
%   mat = MAT2PY(mat)
%
%     Convert to NumPy-array, determine the data-type automatically.
%
%   mat = MAT2PY(mat,dtype)
%
%     Convert to NumPy-array of specific data-type.

if nargin == 1
  if     isa(self,'single'); dtype = 'float32';
  elseif isa(self,'double'); dtype = 'float64';
  elseif isa(self,'int32' ); dtype = 'int32'  ;
  elseif isa(self,'int64' ); dtype = 'int64'  ;
  error('Not implemented');
  end
end

shape = size(self);

if     length(shape)==2 && shape(1)==1; shape = [shape(2)];
elseif length(shape)==2 && shape(2)==1; shape = [shape(1)]; end

shape = py.numpy.array(shape);
self  = py.numpy.array(reshape(self.',1,[])).astype(dtype).reshape(shape);

end