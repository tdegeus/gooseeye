# GooseEYE read-me

## Installation

### Windows

#### Pre-requisites 

* WinPython

##### WinPython

Update the ``PATH`` variable such that Python can be opened/run from the command-line simply as ``python.exe``

1.  Open a command prompt (``Start + cmd``).

2.  Execute

        set PATH=%PATH%;"path\to\python.exe"

    For example:

		set PATH=%PATH%;"C:\Program Files (x86)\WinPython-64bit-2.7.10.3\python-2.7.10.amd64"

3.  Now python the ``python.exe`` can be run from anywhere simply by typing

		python.exe
		

#### Installation

*   Copy the pre-compiled ``libgooseeye.dll`` to place where Windows can find it:

        C:\Windows\System32

*   Install the Python module. In a command prompt run

        path\to\python.exe setup.py install

#### Matlab

*   Register Python to Matlab

        >>> pyversion 'path\to\python.exe'

    For example:

        >>> pyversion 'C:\Program Files (x86)\WinPython-64bit-2.7.10.3\python-2.7.10.amd64\python.exe'

### Linux

#### Compile

To compile the Python code to ``*.pyc`` files, and the C code to the
shared-library ``libgooseeye.so``:

    $ cd /path/to/GooseEYE
    $ cd src/
    $ make release

To debug: replace ``release`` with ``debug``.

#### Set environment variables

Set the Python and library path:

    $ source /path/to/GooseEYE/bashrc

This will execute the following commands:

    $ export PYTHONPATH=/path/to/GooseEYE/src:$PYTHONPATH
    $ export LD_LIBRARY_PATH=/path/to/GooseEYE/src:$LD_LIBRARY_PATH

The path variables are now set for this session. This means that a Python script
can now be run. However in a new session (i.e. terminal) the environment
variables have to be reset. To automatically set the path variables, add the
above lines to the ``~/.bashrc`` file.

#### Compile documentation

    $ cd /path/to/GooseEYE/doc
    $ make all

### Prerequisites

#### Fedora

1.  Install GNU compilers and linear algebra libraries:

         $ yum install gcc cmake

2.  Install Python with modules:

         $ yum install numpy python-pip scipy python-matplotlib* ipython

    The modules ``scipy``, ``python-matplotlib*`` and ``ipython`` are not
    strictly needed, but many things will not work without them.

3.  Install Doxygen to compile the C/Fortran documentation

         $ yum install doxygen

4.  Install Sphinx, with modules, to compile the documentation (including the
    documentation output from Doxygen)

         $ pip install sphinx sphinx_bootstrap_theme breathe



### Mac OSx

#### Compile

To compile the Python code to ``*.pyc`` files, and the C code to the
shared-library ``libgooseeye.so``:

    $ cd /path/to/GooseEYE
    $ cd src/
    $ make release

To debug: replace ``release`` with ``debug``.

#### Set environment variables

Set the Python and library path:

    $ source /path/to/GooseEYE/bashrc

This will execute the following commands:

    $ export PYTHONPATH=/path/to/GooseEYE/src:$PYTHONPATH
    $ export LIBRARY_PATH=/path/to/GooseEYE/src:$LIBRARY_PATH

The path variables are now set for this session. This means that a Python script
can now be run. However in a new session (i.e. terminal) the environment
variables have to be reset. To automatically set the path variables, add the
above lines to the ``~/.bashrc`` file.

#### Compile documentation

    $ cd /path/to/GooseEYE/doc
    $ make all

### Prerequisites

#### OSx Yosemite with homebrew

1.  Install GNU compilers and linear algebra libraries:

         $ brew install gcc49

2.  Install Python with modules:

         $ brew install python
         $ pip install numpy scipy matplotlib ipython

    The modules ``scipy``, ``python-matplotlib*`` and ``ipython`` are not
    strictly needed, but many things will not work without them.

3.  Install Doxygen to compile the C/Fortran documentation

         $ brew install doxygen

4.  Install Sphinx, with modules, to compile the documentation (including the
    documentation output from Doxygen)

         $ pip install sphinx sphinx_bootstrap_theme breathe
