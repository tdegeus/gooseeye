﻿from distutils.core import setup

#import sys

#if sys.platform=='win32': lib = '../libgooseeye.dll'
#else                    : lib = '../libgooseeye.so'

setup(
  name         = 'gooseeye' ,
  version      = '1.0'      ,
  package_dir  = {'gooseeye': 'src/gooseeye'},
  #package_data = {'gooseeye':[lib]          },
  packages     = ['gooseeye'],
)